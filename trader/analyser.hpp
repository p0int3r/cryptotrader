#ifndef ANALYSER_HPP
#define ANALYSER_HPP

#include <QObject>
#include <qqml.h>
#include <deque>
#include "microeconomicanalysis.hpp"
#include "macroeconomicanalysis.hpp"
#include "sentimentanalysis.hpp"
#include "artificialintelligenceanalysis.hpp"
#include "timeseries.hpp"

namespace trader
{
    class Analyser: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(trader::SentimentAnalysis *sentimentAnalysis READ getSentimentAnalysis WRITE setSentimentAnalysis NOTIFY sentimentAnalysisChanged)
        Q_PROPERTY(trader::MacroEconomicAnalysis *macroEconomicAnalysis READ getMacroEconomicAnalysis WRITE setMacroEconomicAnalysis NOTIFY macroEconomicAnalysisChanged)
        Q_PROPERTY(trader::MicroEconomicAnalysis *microEconomicAnalysis READ getMicroEconomicAnalysis WRITE setMicroEconomicAnalysis NOTIFY microEconomicAnalysisChanged)
        Q_PROPERTY(trader::ArtificialIntelligenceAnalysis *artificialIntelligenceAnalysis READ getArtificialIntelligenceAnalysis WRITE setArtificialIntelligenceAnalysis NOTIFY artificialIntelligenceAnalysisChanged)
        Q_PROPERTY(trader::data_structures::TimeSeries<std::deque<double>> getTimeSeries READ getTimeSeries WRITE setTimeSeries NOTIFY timeSeriesChanged)

        private:
            trader::SentimentAnalysis *sentimentAnalysis=nullptr;
            trader::MacroEconomicAnalysis *macroEconomicAnalysis=nullptr;
            trader::MicroEconomicAnalysis *microEconomicAnalysis=nullptr;
            trader::ArtificialIntelligenceAnalysis *artificialIntelligenceAnalysis=nullptr;
            trader::data_structures::TimeSeries<std::deque<double>> timeSeries;

        public:
            explicit Analyser(QObject *parent=nullptr);
            ~Analyser();
            trader::SentimentAnalysis *getSentimentAnalysis() const;
            void setSentimentAnalysis(trader::SentimentAnalysis *value);
            trader::MacroEconomicAnalysis *getMacroEconomicAnalysis() const;
            void setMacroEconomicAnalysis(trader::MacroEconomicAnalysis *value);
            trader::MicroEconomicAnalysis *getMicroEconomicAnalysis() const;
            void setMicroEconomicAnalysis(trader::MicroEconomicAnalysis *value);
            trader::ArtificialIntelligenceAnalysis *getArtificialIntelligenceAnalysis() const;
            void setArtificialIntelligenceAnalysis(trader::ArtificialIntelligenceAnalysis *value);
            trader::data_structures::TimeSeries<std::deque<double>> &getTimeSeries();
            void setTimeSeries(const trader::data_structures::TimeSeries<std::deque<double>> &value);
            void clear();

        signals:
            void sentimentAnalysisChanged(const trader::SentimentAnalysis *sentimentAnalysis);
            void macroEconomicAnalysisChanged(const trader::MacroEconomicAnalysis *macroEconomicAnalysisChanged);
            void microEconomicAnalysisChanged(const trader::MicroEconomicAnalysis *microEconomicAnalysisChanged);
            void artificialIntelligenceAnalysisChanged(const trader::ArtificialIntelligenceAnalysis *artificialIntelligenceAnalysis);
            void timeSeriesChanged(const trader::data_structures::TimeSeries<std::deque<double>> &timeSeries);
    };
}

Q_DECLARE_METATYPE(trader::Analyser *)

#endif // ANALYSER_HPP
