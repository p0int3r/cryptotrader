#ifndef SIZER_HPP
#define SIZER_HPP

#include <QObject>
#include <qqml.h>
#include "analysermanager.hpp"
#include "assetmanager.hpp"
#include "portfoliomanager.hpp"
#include "order.hpp"

namespace trader
{
    class Sizer: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)

        private:
            QString name;
            QString description;

        public:
            explicit Sizer(QObject *parent=nullptr);
            explicit Sizer(QString name,QString description,QObject *parent=nullptr);
            virtual ~Sizer();
            QString getName() const;
            void setName(const QString &value);
            QString getDescription() const;
            void setDescription(const QString &value);

        signals:
            void nameChanged(const QString &name);
            void descriptionChanged(const QString &description);
            void resized(const QString &orderId,const trader::Order *order);

        public slots:
            virtual void size(const QString &market,const QObject *object) = 0;
    };
}

Q_DECLARE_METATYPE(trader::Sizer *)

#endif // SIZER_HPP
