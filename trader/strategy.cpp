#include "strategy.hpp"


trader::Strategy::Strategy(QObject *parent): trader::Strategy::Strategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE,parent) {}

trader::Strategy::Strategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel,QObject *parent): QObject(parent)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
    this->connect(this,&trader::Strategy::preprocessed,this,&trader::Strategy::strategise);
}

trader::Strategy::~Strategy() = default;

QString trader::Strategy::getName() const { return this->name; }

void trader::Strategy::setName(const QString &value)
{
    this->name=value;
    emit this->nameChanged(this->name);
}

QString trader::Strategy::getDescription() const { return this->description; }

void trader::Strategy::setDescription(const QString &value)
{
    this->description=value;
    emit this->descriptionChanged(this->description);
}

trader::Strategy::FrequencyLevel trader::Strategy::getFrequencyLevel() const { return this->frequencyLevel; }

void trader::Strategy::setFrequencyLevel(const trader::Strategy::FrequencyLevel &value)
{
    this->frequencyLevel=value;
    emit this->frequencyLevelChanged(this->frequencyLevel);
}
