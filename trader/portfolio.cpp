#include "portfolio.hpp"

trader::Portfolio::Portfolio(QObject *parent): trader::Portfolio(QString(),QString(),0.0,parent) {}

trader::Portfolio::Portfolio(QString name,QString baseCurrency,double capital,QObject *parent): QObject(parent)
{
    this->setName(name);
    this->setBaseCurrency(baseCurrency);
    this->setInitialCapital(capital);
    this->setEquity(capital);
    this->setCurrentCapital(capital);
    this->setRealisedPnL(0.0);
    this->setUnrealisedPnL(0.0);
    this->setReturnOnInvestment(0.0);
    this->setPortfolioId(QUuid::createUuid().toString());
    this->setOpenPositions(new trader::PositionHash());
    this->setClosedPositions(new trader::PositionMultiHash());
    this->connect(this,&trader::Portfolio::positionUpdated,this,&trader::Portfolio::positionTransacted);
    this->connect(this,&trader::Portfolio::positionAdded,this,&trader::Portfolio::positionTransacted);
    this->connect(this,&trader::Portfolio::positionModified,this,&trader::Portfolio::positionTransacted);
    this->connect(this,&trader::Portfolio::positionTransacted,this,&trader::Portfolio::update);
}

trader::Portfolio::~Portfolio()
{
    this->clear();
    delete this->openPositions;
    delete this->closedPositions;
}


QString trader::Portfolio::getPortfolioId() const { return this->portfolioId; }

void trader::Portfolio::setPortfolioId(const QString &value)
{
    this->portfolioId=value;
    emit this->portfolioIdChanged(this->portfolioId);
}

QString trader::Portfolio::getBaseCurrency() const { return this->baseCurrency; }

void trader::Portfolio::setBaseCurrency(const QString &value)
{
    this->baseCurrency=value;
    emit this->baseCurrencyChanged(this->baseCurrency);
}

double trader::Portfolio::getInitialCapital() const { return this->initialCapital; }

void trader::Portfolio::setInitialCapital(double value)
{
    this->initialCapital=value;
    emit this->initialCapitalChanged(this->initialCapital);
}

double trader::Portfolio::getEquity() const { return this->equity; }

void trader::Portfolio::setEquity(double value)
{
    this->equity=value;
    emit this->equityChanged(this->equity);
}


double trader::Portfolio::getCurrentCapital() const { return this->currentCapital; }


void trader::Portfolio::setCurrentCapital(double value)
{
    this->currentCapital=value;
    emit this->currentCapitalChanged(this->currentCapital);
}

double trader::Portfolio::getUnrealisedPnL() const { return this->unrealisedPnL; }

void trader::Portfolio::setUnrealisedPnL(double value)
{
    this->unrealisedPnL=value;
    emit this->unrealisedPnLChanged(this->unrealisedPnL);
}

double trader::Portfolio::getRealisedPnL() const { return this->realisedPnL; }

void trader::Portfolio::setRealisedPnL(double value)
{
    this->realisedPnL=value;
    emit this->realisedPnLChanged(this->realisedPnL);
}

double trader::Portfolio::getReturnOnInvestment() const { return this->returnOnInvestment; }

void trader::Portfolio::setReturnOnInvestment(double value)
{
    this->returnOnInvestment=value;
    emit this->returnOnInvestmentChanged(this->returnOnInvestment);
}

trader::PositionHash *trader::Portfolio::getOpenPositions() const { return this->openPositions; }

void trader::Portfolio::setOpenPositions(trader::PositionHash *value)
{
    this->openPositions=value;
    emit this->openPositionsChanged(this->openPositions);
}

void trader::Portfolio::clearOpenPositions()
{
    trader::PositionHash::iterator iterator;
    for (iterator=this->getOpenPositions()->begin();iterator!=this->getOpenPositions()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getOpenPositions()->clear();
    emit this->openPositionsChanged(this->openPositions);
}


trader::PositionMultiHash *trader::Portfolio::getClosedPositions() const { return this->closedPositions; }

void trader::Portfolio::setClosedPositions(trader::PositionMultiHash *value)
{
    this->closedPositions=value;
    emit this->closedPositionsChanged(this->closedPositions);
}

void trader::Portfolio::clearClosedPositions()
{
    trader::PositionMultiHash::iterator iterator;
    for (iterator=this->getClosedPositions()->begin();iterator!=this->getClosedPositions()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getClosedPositions();
    emit this->closedPositionsChanged(this->closedPositions);
}


QString trader::Portfolio::getName() const { return this->name; }

void trader::Portfolio::setName(const QString &value)
{
    this->name=value;
    emit this->nameChanged(this->name);
}


void trader::Portfolio::clear()
{
    this->setInitialCapital(0.0);
    this->setEquity(0.0);
    this->setCurrentCapital(0.0);
    this->setUnrealisedPnL(0.0);
    this->setRealisedPnL(0.0);
    this->clearOpenPositions();
    this->clearClosedPositions();
}


void trader::Portfolio::update()
{
    this->setUnrealisedPnL(0.0);
    this->setEquity(this->getRealisedPnL());
    this->setEquity(this->getEquity()+this->getInitialCapital());

    double total_equity=0.0;
    double total_unrealised_pnl=0.0;
    trader::PositionHash::iterator iterator;
    for (iterator=this->getOpenPositions()->begin();iterator!=this->getOpenPositions()->end();iterator++)
    {
        trader::Position *current_position=iterator.value();
        total_unrealised_pnl+=current_position->getUnrealisedPnL();
        total_equity+=current_position->getMarketValue()-current_position->getCostBasis()+current_position->getRealisedPnL();
    }

    this->setUnrealisedPnL(total_unrealised_pnl);
    this->setEquity(this->getEquity()+total_equity);
    emit this->updated();
}


bool trader::Portfolio::updatePosition(QString market,double bid,double ask)
{
    bool contains_position=this->getOpenPositions()->contains(market);
    if (contains_position==false) { return false; }
    trader::Position *position=this->getOpenPositions()->value(market);
    position->update(bid,ask);
    emit this->positionUpdated(market,position);
    return true;
}


bool trader::Portfolio::modifyPosition(QString market,trader::Position::Action action,double quantity,double price,double commission,double bid,double ask)
{
    bool contains_position=this->getOpenPositions()->contains(market);
    if (contains_position==false) { return false; }
    trader::Position *position=this->getOpenPositions()->value(market);
    position->transact(action,quantity,price,commission);
    position->update(bid,ask);
    emit this->positionModified(market,position);

    if (position->getQuantity()==0.0)
    {
        double realised_pnl=this->getRealisedPnL()+position->getRealisedPnL();
        this->setRealisedPnL(realised_pnl);
        double initial_capital=this->getInitialCapital();
        double return_on_investment=realised_pnl/initial_capital;
        this->setReturnOnInvestment(return_on_investment);
        this->getOpenPositions()->remove(market);
        emit this->openPositionsChanged(this->openPositions);
        this->getClosedPositions()->insert(market,position);
        emit this->closedPositionsChanged(this->closedPositions);
        emit this->positionClosed(market,position);
    }

    return true;
}


bool trader::Portfolio::addPosition(QString market,trader::Position::Action action,double quantity,double price,double commission,double bid,double ask)
{
    bool contains_position=this->getOpenPositions()->contains(market);
    if (contains_position==true) { return false; }
    trader::Position *position=new trader::Position(market,action,quantity,price,commission,bid,ask);
    this->getOpenPositions()->insert(market,position);
    emit this->openPositionsChanged(this->openPositions);
    emit this->positionAdded(market,position);
    emit this->positionOpened(market,position);
    return true;
}


bool trader::Portfolio::transactPosition(QString market,trader::Position::Action action,double quantity,double price,double commission,double bid,double ask)
{
    if (action==trader::Position::Action::BOUGHT)
    {
        double current_capital=this->getCurrentCapital()-((quantity*price)+commission);
        this->setCurrentCapital(current_capital);
    }
    else if (action==trader::Position::Action::SOLD)
    {
        double current_capital=this->getCurrentCapital()+((quantity*price)-commission);
        this->setCurrentCapital(current_capital);
    }

    bool contains_position=this->getOpenPositions()->contains(market);
    if (contains_position==false) { this->addPosition(market,action,quantity,price,commission,bid,ask); }
    else { this->modifyPosition(market,action,quantity,price,commission,bid,ask); }
    return true;
}
