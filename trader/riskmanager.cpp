#include "riskmanager.hpp"

trader::RiskManager::RiskManager(QObject *parent): QObject(parent)
{
    this->setRisks(new trader::RiskHash());
    this->setData(QVariantMap());
}

trader::RiskManager::~RiskManager()
{
    this->clearRisks();
    delete this->risks;
}

QVariantMap trader::RiskManager::getData() const { return this->data; }

void trader::RiskManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::RiskHash *trader::RiskManager::getRisks() const { return this->risks; }

void trader::RiskManager::setRisks(trader::RiskHash *value)
{
    this->risks=value;
    emit this->risksChanged(this->risks);
}

void trader::RiskManager::clearRisks()
{
    trader::RiskHash::iterator iterator;
    for (iterator=this->getRisks()->begin();iterator!=this->getRisks()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getRisks()->clear();
}

bool trader::RiskManager::addRisk(QString name,trader::Risk *risk)
{
    bool contains_risk=this->getRisks()->contains(name);
    if (contains_risk==true) { return false; }
    if (risk==nullptr) { return false; }
    this->getRisks()->insert(name,risk);
    emit this->riskAdded(name,risk);
    return true;
}

bool trader::RiskManager::removeRisk(QString name)
{
    bool contains_risk=this->getRisks()->contains(name);
    if (contains_risk==false) { return false; }
    trader::Risk *risk=this->getRisks()->value(name);
    this->getRisks()->remove(name);
    emit this->riskRemoved(name,risk);
    risk->deleteLater();
    return true;
}


trader::Risk *trader::RiskManager::getRisk(QString name)
{
    bool contains_risk=this->getRisks()->contains(name);
    if (contains_risk==false) { return nullptr; }
    trader::Risk *risk=this->getRisks()->value(name);
    return risk;
}

void trader::RiskManager::clear()
{
    this->setData(QVariantMap());
    this->clearRisks();
}
