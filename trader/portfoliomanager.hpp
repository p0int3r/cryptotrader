#ifndef PORTFOLIOMANAGER_HPP
#define PORTFOLIOMANAGER_HPP

#include <QObject>
#include <QVariantMap>
#include <QHash>
#include <QString>
#include <qqml.h>
#include "portfolio.hpp"


namespace trader { typedef QHash<QString,trader::Portfolio *> PortfolioHash; }
Q_DECLARE_METATYPE(trader::PortfolioHash *)


namespace trader
{
    class PortfolioManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::PortfolioHash *portfolios READ getPortfolios WRITE setPortfolios NOTIFY portfoliosChanged)

        private:
            QVariantMap data;
            trader::PortfolioHash *portfolios=nullptr;

        public:
            explicit PortfolioManager(QObject *parent=nullptr);
            ~PortfolioManager();
            trader::PortfolioHash *getPortfolios() const;
            void setPortfolios(trader::PortfolioHash *value);
            void clearPortfolios();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void portfoliosChanged(const trader::PortfolioHash *portfolios);
            void portfolioAdded(const QString &name,const trader::Portfolio *portfolio);
            void portfolioRemoved(const QString &name,const trader::Portfolio *portfolio);


        public slots:
            bool addPortfolio(QString name,trader::Portfolio *portfolio);
            bool removePortfolio(QString name);
            trader::Portfolio *getPortfolio(QString name);
    };
}

Q_DECLARE_METATYPE(trader::PortfolioManager *)

#endif // PORTFOLIOMANAGER_HPP
