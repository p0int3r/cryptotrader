#ifndef MACROECONOMICANALYSIS_HPP
#define MACROECONOMICANALYSIS_HPP

#include <QObject>
#include <qqml.h>

// TODO: implement when needed.

namespace trader
{
    class MacroEconomicAnalysis: public QObject
    {
        Q_OBJECT
        QML_ELEMENT

        private:

        public:
            explicit MacroEconomicAnalysis(QObject *parent=nullptr);
            ~MacroEconomicAnalysis();
            void clear();

        signals:

        public slots:
    };
}

Q_DECLARE_METATYPE(trader::MacroEconomicAnalysis *)

#endif // MACROECONOMICANALYSIS_HPP
