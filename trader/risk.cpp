#include "risk.hpp"


trader::Risk::Risk(QObject *parent): trader::Risk::Risk(QString(),QString(),trader::Risk::RiskIndicator::NONE,parent) {}

trader::Risk::Risk(QString name,QString description,trader::Risk::RiskIndicator riskIndicator,QObject *parent): QObject(parent)
{
    this->setName(name);
    this->setDescription(description);
    this->setRiskIndicator(riskIndicator);
    this->connect(this,&trader::Risk::preprocessed,this,&trader::Risk::assess);
}

trader::Risk::~Risk() = default;

QString trader::Risk::getName() const { return this->name; }

void trader::Risk::setName(const QString &value)
{
    this->name=value;
    emit this->nameChanged(this->name);
}

QString trader::Risk::getDescription() const { return this->description; }

void trader::Risk::setDescription(const QString &value)
{
    this->description=value;
    emit this->descriptionChanged(this->description);
}

trader::Risk::RiskIndicator trader::Risk::getRiskIndicator() const { return this->riskIndicator; }

void trader::Risk::setRiskIndicator(const trader::Risk::RiskIndicator &value)
{
    this->riskIndicator=value;
    emit this->riskIndicatorChanged(this->riskIndicator);
}
