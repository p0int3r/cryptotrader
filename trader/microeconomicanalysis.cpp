#include "microeconomicanalysis.hpp"


trader::MicroEconomicAnalysis::MicroEconomicAnalysis(QObject *parent): QObject(parent)
{
    this->setBid(0.0);
    this->setAsk(0.0);
    this->setMiddlePrice(0.0);
    this->setHigh(0.0);
    this->setLow(0.0);
    this->setLast(0.0);
    this->setPreviousDay(0.0);
    this->setVolume(0.0);
    this->setBaseVolume(0.0);
    this->setDepthImbalance(0.0);
    this->setVolumeImbalance(0.0);
    this->setSpread(0.0);
    this->setSpreadPercentage(0.0);
    this->setHistoricalVolatility(0.0);
    this->setHistoricalVolatilityPercentage(0.0);
    this->setMomentum(0.0);
    this->setMomentumPercentage(0.0);
    this->setBidVolume(0.0);
    this->setAskVolume(0.0);
    this->setAverageBidVolume(0.0);
    this->setAverageAskVolume(0.0);
    this->setBaseBidVolume(0.0);
    this->setBaseAskVolume(0.0);
    this->setAverageBaseBidVolume(0.0);
    this->setAverageBaseAskVolume(0.0);
    this->setBuyVolume(0.0);
    this->setSellVolume(0.0);
    this->setAverageBuyVolume(0.0);
    this->setAverageSellVolume(0.0);
    this->setBaseBuyVolume(0.0);
    this->setBaseSellVolume(0.0);
    this->setAverageBaseBuyVolume(0.0);
    this->setAverageBaseSellVolume(0.0);
}

trader::MicroEconomicAnalysis::~MicroEconomicAnalysis() = default;

double trader::MicroEconomicAnalysis::getHigh() const { return this->high; }

void trader::MicroEconomicAnalysis::setHigh(double value)
{
    this->high=value;
    emit this->highChanged(this->high);
}

double trader::MicroEconomicAnalysis::getLow() const { return this->low; }

void trader::MicroEconomicAnalysis::setLow(double value)
{
    this->low=value;
    emit this->lowChanged(this->low);
}

double trader::MicroEconomicAnalysis::getLast() const { return this->last; }

void trader::MicroEconomicAnalysis::setLast(double value)
{
    this->last=value;
    emit this->lastChanged(this->last);
}

double trader::MicroEconomicAnalysis::getPreviousDay() const { return this->previousDay; }

void trader::MicroEconomicAnalysis::setPreviousDay(double value)
{
    this->previousDay=value;
    emit this->previousDayChanged(this->previousDay);
}

double trader::MicroEconomicAnalysis::getBid() const { return this->bid; }

void trader::MicroEconomicAnalysis::setBid(double value)
{
    this->bid=value;
    emit this->bidChanged(this->bid);
}

double trader::MicroEconomicAnalysis::getAsk() const { return this->ask; }

void trader::MicroEconomicAnalysis::setAsk(double value)
{
    this->ask=value;
    emit this->askChanged(this->ask);
}

double trader::MicroEconomicAnalysis::getMiddlePrice() const { return this->middlePrice; }

void trader::MicroEconomicAnalysis::setMiddlePrice(double value)
{
    this->middlePrice=value;
    emit this->middlePriceChanged(this->middlePrice);
}


double trader::MicroEconomicAnalysis::getVolume() const { return this->volume; }

void trader::MicroEconomicAnalysis::setVolume(double value)
{
    this->volume=value;
    emit this->volumeChanged(this->volume);
}

double trader::MicroEconomicAnalysis::getBaseVolume() const { return this->baseVolume; }

void trader::MicroEconomicAnalysis::setBaseVolume(double value)
{
    this->baseVolume=value;
    emit this->baseVolumeChanged(this->baseVolume);
}

double trader::MicroEconomicAnalysis::getDepthImbalance() const { return this->depthImbalance; }

void trader::MicroEconomicAnalysis::setDepthImbalance(double value)
{
    this->depthImbalance=value;
    emit this->depthImbalanceChanged(this->depthImbalance);
}

double trader::MicroEconomicAnalysis::getVolumeImbalance() const { return this->volumeImbalance; }

void trader::MicroEconomicAnalysis::setVolumeImbalance(double value)
{
    this->volumeImbalance=value;
    emit this->volumeImbalanceChanged(this->volumeImbalance);
}

double trader::MicroEconomicAnalysis::getSpread() const { return this->spread; }

void trader::MicroEconomicAnalysis::setSpread(double value)
{
    this->spread=value;
    emit this->spreadChanged(this->spread);
}

double trader::MicroEconomicAnalysis::getSpreadPercentage() const { return this->spreadPercentage; }

void trader::MicroEconomicAnalysis::setSpreadPercentage(double value)
{
    this->spreadPercentage=value;
    emit this->spreadPercentageChanged(this->spreadPercentage);
}

double trader::MicroEconomicAnalysis::getHistoricalVolatility() const { return this->historicalVolatility; }

void trader::MicroEconomicAnalysis::setHistoricalVolatility(double value)
{
    this->historicalVolatility=value;
    emit this->historicalVolatilityChanged(this->historicalVolatility);
}

double trader::MicroEconomicAnalysis::getHistoricalVolatilityPercentage() const { return this->historicalVolatilityPercentage; }

void trader::MicroEconomicAnalysis::setHistoricalVolatilityPercentage(double value)
{
    this->historicalVolatilityPercentage=value;
    emit this->historicalVolatilityPercentageChanged(this->historicalVolatilityPercentage);
}

double trader::MicroEconomicAnalysis::getMomentum() const { return this->momentum; }

void trader::MicroEconomicAnalysis::setMomentum(double value)
{
    this->momentum=value;
    emit this->momentumChanged(this->momentum);
}

double trader::MicroEconomicAnalysis::getMomentumPercentage() const { return this->momentumPercentage; }

void trader::MicroEconomicAnalysis::setMomentumPercentage(double value)
{
    this->momentumPercentage=value;
    emit this->momentumPercentageChanged(this->momentumPercentage);
}

double trader::MicroEconomicAnalysis::getBidVolume() const { return this->bidVolume; }

void trader::MicroEconomicAnalysis::setBidVolume(double value)
{
    this->bidVolume=value;
    emit this->bidVolumeChanged(this->bidVolume);
}

double trader::MicroEconomicAnalysis::getAskVolume() const { return this->askVolume; }

void trader::MicroEconomicAnalysis::setAskVolume(double value)
{
    this->askVolume=value;
    emit this->askVolumeChanged(this->askVolume);
}

double trader::MicroEconomicAnalysis::getAverageBidVolume() const { return this->averageBidVolume; }

void trader::MicroEconomicAnalysis::setAverageBidVolume(double value)
{
    this->averageBidVolume=value;
    emit this->averageBidVolumeChanged(this->averageBidVolume);
}

double trader::MicroEconomicAnalysis::getAverageAskVolume() const { return this->averageAskVolume; }

void trader::MicroEconomicAnalysis::setAverageAskVolume(double value)
{
    this->averageAskVolume=value;
    emit this->averageAskVolumeChanged(this->averageAskVolume);
}

double trader::MicroEconomicAnalysis::getBaseBidVolume() const { return this->baseBidVolume; }

void trader::MicroEconomicAnalysis::setBaseBidVolume(double value)
{
    this->baseBidVolume=value;
    emit this->baseBidVolumeChanged(this->baseBidVolume);
}

double trader::MicroEconomicAnalysis::getBaseAskVolume() const { return this->baseAskVolume; }

void trader::MicroEconomicAnalysis::setBaseAskVolume(double value)
{
    this->baseAskVolume=value;
    emit this->baseAskVolumeChanged(this->baseAskVolume);
}

double trader::MicroEconomicAnalysis::getAverageBaseBidVolume() const { return this->averageBaseBidVolume; }

void trader::MicroEconomicAnalysis::setAverageBaseBidVolume(double value)
{
    this->averageBaseBidVolume=value;
    emit this->averageBaseBidVolumeChanged(this->averageBaseBidVolume);
}

double trader::MicroEconomicAnalysis::getAverageBaseAskVolume() const { return this->averageBaseAskVolume; }

void trader::MicroEconomicAnalysis::setAverageBaseAskVolume(double value)
{
    this->averageBaseAskVolume=value;
    emit this->averageBaseAskVolumeChanged(this->averageBaseAskVolume);
}

double trader::MicroEconomicAnalysis::getBuyVolume() const { return this->buyVolume; }

void trader::MicroEconomicAnalysis::setBuyVolume(double value)
{
    this->buyVolume=value;
    emit this->buyVolumeChanged(this->buyVolume);
}

double trader::MicroEconomicAnalysis::getSellVolume() const { return this->sellVolume; }

void trader::MicroEconomicAnalysis::setSellVolume(double value)
{
    this->sellVolume=value;
    emit this->sellVolumeChanged(this->sellVolume);
}

double trader::MicroEconomicAnalysis::getAverageBuyVolume() const { return this->averageBuyVolume; }

void trader::MicroEconomicAnalysis::setAverageBuyVolume(double value)
{
    this->averageBuyVolume=value;
    emit this->averageBuyVolumeChanged(this->averageBuyVolume);
}

double trader::MicroEconomicAnalysis::getAverageSellVolume() const { return this->averageSellVolume; }

void trader::MicroEconomicAnalysis::setAverageSellVolume(double value)
{
    this->averageSellVolume=value;
    emit this->averageSellVolumeChanged(this->averageSellVolume);
}

double trader::MicroEconomicAnalysis::getBaseBuyVolume() const { return this->baseBuyVolume; }

void trader::MicroEconomicAnalysis::setBaseBuyVolume(double value)
{
    this->baseBuyVolume=value;
    emit this->baseBuyVolumeChanged(this->baseBuyVolume);
}

double trader::MicroEconomicAnalysis::getBaseSellVolume() const { return this->baseSellVolume; }

void trader::MicroEconomicAnalysis::setBaseSellVolume(double value)
{
    this->baseSellVolume=value;
    emit this->baseSellVolumeChanged(this->baseSellVolume);
}

double trader::MicroEconomicAnalysis::getAverageBaseBuyVolume() const { return this->averageBaseBuyVolume; }

void trader::MicroEconomicAnalysis::setAverageBaseBuyVolume(double value)
{
    this->averageBaseBuyVolume=value;
    emit this->averageBaseBuyVolumeChanged(this->averageBaseBuyVolume);
}

double trader::MicroEconomicAnalysis::getAverageBaseSellVolume() const { return this->averageBaseSellVolume; }

void trader::MicroEconomicAnalysis::setAverageBaseSellVolume(double value)
{
    this->averageBaseSellVolume=value;
    emit this->averageBaseSellVolumeChanged(this->averageBaseSellVolume);
}

void trader::MicroEconomicAnalysis::clear()
{
    this->setBid(0.0);
    this->setAsk(0.0);
    this->setMiddlePrice(0.0);
    this->setHigh(0.0);
    this->setLow(0.0);
    this->setLast(0.0);
    this->setPreviousDay(0.0);
    this->setVolume(0.0);
    this->setBaseVolume(0.0);
    this->setDepthImbalance(0.0);
    this->setVolumeImbalance(0.0);
    this->setSpread(0.0);
    this->setSpreadPercentage(0.0);
    this->setHistoricalVolatility(0.0);
    this->setHistoricalVolatilityPercentage(0.0);
    this->setMomentum(0.0);
    this->setMomentumPercentage(0.0);
    this->setBidVolume(0.0);
    this->setAskVolume(0.0);
    this->setAverageBidVolume(0.0);
    this->setAverageAskVolume(0.0);
    this->setBaseBidVolume(0.0);
    this->setBaseAskVolume(0.0);
    this->setAverageBaseBidVolume(0.0);
    this->setAverageBaseAskVolume(0.0);
    this->setBuyVolume(0.0);
    this->setSellVolume(0.0);
    this->setAverageBuyVolume(0.0);
    this->setAverageSellVolume(0.0);
    this->setBaseBuyVolume(0.0);
    this->setBaseSellVolume(0.0);
    this->setAverageBaseBuyVolume(0.0);
    this->setAverageBaseSellVolume(0.0);
}
