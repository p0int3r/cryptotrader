#ifndef TRADER_HPP
#define TRADER_HPP

#include <QObject>
#include <qqml.h>
#include "assetmanager.hpp"
#include "analysermanager.hpp"
#include "executionmanager.hpp"
#include "ordermanager.hpp"
#include "strategymanager.hpp"
#include "riskmanager.hpp"
#include "portfoliomanager.hpp"
#include "filterermanager.hpp"
#include "sizermanager.hpp"

namespace trader
{
    class Trader: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(trader::AnalyserManager *analyserManager READ getAnalyserManager WRITE setAnalyserManager NOTIFY analyserManagerChanged)
        Q_PROPERTY(trader::PortfolioManager *portfolioManager READ getPortfolioManager WRITE setPortfolioManager NOTIFY portfolioManagerChanged)
        Q_PROPERTY(trader::StrategyManager *strategyManager READ getStrategyManager WRITE setStrategyManager NOTIFY strategyManagerChanged)
        Q_PROPERTY(trader::RiskManager *riskManager READ getRiskManager WRITE setRiskManager NOTIFY riskManagerChanged)
        Q_PROPERTY(trader::OrderManager *orderManager READ getOrderManager WRITE setOrderManager NOTIFY orderManagerChanged)
        Q_PROPERTY(trader::ExecutionManager *executionManager READ getExecutionManager WRITE setExecutionManager NOTIFY executionManagerChanged)
        Q_PROPERTY(trader::FiltererManager *filtererManager READ getFiltererManager WRITE setFiltererManager NOTIFY filtererManagerChanged)
        Q_PROPERTY(trader::SizerManager *sizerManager READ getSizerManager WRITE setSizerManager NOTIFY sizerManagerChanged)

        private:
            trader::AssetManager *assetManager=nullptr;
            trader::AnalyserManager *analyserManager=nullptr;
            trader::PortfolioManager *portfolioManager=nullptr;
            trader::StrategyManager *strategyManager=nullptr;
            trader::RiskManager *riskManager=nullptr;
            trader::OrderManager *orderManager=nullptr;
            trader::ExecutionManager *executionManager=nullptr;
            trader::FiltererManager *filtererManager=nullptr;
            trader::SizerManager *sizerManager=nullptr;

        public:
            explicit Trader(QObject *parent=nullptr);
            ~Trader();
            trader::AssetManager *getAssetManager() const;
            void setAssetManager(trader::AssetManager *value);
            trader::AnalyserManager *getAnalyserManager() const;
            void setAnalyserManager(trader::AnalyserManager *value);
            trader::PortfolioManager *getPortfolioManager() const;
            void setPortfolioManager(trader::PortfolioManager *value);
            trader::StrategyManager *getStrategyManager() const;
            void setStrategyManager(trader::StrategyManager *value);
            trader::RiskManager *getRiskManager() const;
            void setRiskManager(trader::RiskManager *value);
            trader::OrderManager *getOrderManager() const;
            void setOrderManager(trader::OrderManager *value);
            trader::ExecutionManager *getExecutionManager() const;
            void setExecutionManager(trader::ExecutionManager *value);
            trader::FiltererManager *getFiltererManager() const;
            void setFiltererManager(trader::FiltererManager *value);
            trader::SizerManager *getSizerManager() const;
            void setSizerManager(trader::SizerManager *value);

        signals:
            void assetManagerChanged(const trader::AssetManager *assetManager);
            void analyserManagerChanged(const trader::AnalyserManager *analyserManager);
            void portfolioManagerChanged(const trader::PortfolioManager *portfolioManager);
            void strategyManagerChanged(const trader::StrategyManager *strategyManager);
            void riskManagerChanged(const trader::RiskManager *riskManager);
            void orderManagerChanged(const trader::OrderManager *orderManager);
            void executionManagerChanged(const trader::ExecutionManager *executionManager);
            void filtererManagerChanged(const trader::FiltererManager *filtererManager);
            void sizerManagerChanged(const trader::SizerManager *sizerManager);

        public slots:
    };
}

Q_DECLARE_METATYPE(trader::Trader *)

#endif // TRADER_HPP
