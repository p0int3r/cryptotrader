#include "strategymanager.hpp"

trader::StrategyManager::StrategyManager(QObject *parent): QObject(parent)
{
    this->setStrategies(new trader::StrategyHash());
    this->setData(QVariantMap());
}

trader::StrategyManager::~StrategyManager()
{
    this->clearStrategies();
    delete this->strategies;
}

QVariantMap trader::StrategyManager::getData() const { return this->data; }

void trader::StrategyManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::StrategyHash *trader::StrategyManager::getStrategies() const { return this->strategies; }

void trader::StrategyManager::setStrategies(trader::StrategyHash *value)
{
    this->strategies=value;
    emit this->strategiesChanged(this->strategies);
}

void trader::StrategyManager::clearStrategies()
{
    trader::StrategyHash::iterator iterator;
    for (iterator=this->getStrategies()->begin();iterator!=this->getStrategies()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getStrategies()->clear();
}


bool trader::StrategyManager::addStrategy(QString name,trader::Strategy *strategy)
{
    bool contains_strategy=this->getStrategies()->contains(name);
    if (contains_strategy==true) { return false; }
    if (strategy==nullptr) { return false; }
    this->getStrategies()->insert(name,strategy);
    emit this->strategyAdded(name,strategy);
    return true;
}

bool trader::StrategyManager::removeStrategy(QString name)
{
    bool contains_strategy=this->getStrategies()->contains(name);
    if (contains_strategy==false) { return false; }
    trader::Strategy *strategy=this->getStrategies()->value(name);
    this->getStrategies()->remove(name);
    emit this->strategyRemoved(name,strategy);
    strategy->deleteLater();
    return true;
}

trader::Strategy *trader::StrategyManager::getStrategy(QString name)
{
    bool contains_strategy=this->getStrategies()->contains(name);
    if (contains_strategy==false) { return nullptr; }
    trader::Strategy *strategy=this->getStrategies()->value(name);
    return strategy;
}


void trader::StrategyManager::clear()
{
    this->setData(QVariantMap());
    this->clearStrategies();
}
