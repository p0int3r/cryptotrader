#ifndef ASSET_HPP
#define ASSET_HPP

#include <QObject>
#include <QString>
#include <QTimer>
#include <qqml.h>

namespace trader
{
    class Asset: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString market READ getMarket WRITE setMarket NOTIFY marketChanged)
        Q_PROPERTY(double allocatedCapital READ getAllocatedCapital WRITE setAllocatedCapital NOTIFY allocatedCapitalChanged)
        Q_PROPERTY(double capitalPercentage READ getCapitalPercentage WRITE setCapitalPercentage NOTIFY capitalPercentageChanged)
        Q_PROPERTY(int tradingRounds READ getTradingRounds WRITE setTradingRounds NOTIFY tradingRoundsChanged)
        Q_PROPERTY(int currentRound READ getCurrentRound WRITE setCurrentRound NOTIFY currentRoundChanged)
        Q_PROPERTY(QTimer *timer READ getTimer WRITE setTimer NOTIFY timerChanged)
        Q_PROPERTY(int frequencyMilliseconds READ getFrequencyMilliseconds WRITE setFrequencyMilliseconds NOTIFY frequencyMillisecondsChanged)

        private:
            QString market;
            double allocatedCapital;
            double capitalPercentage;
            int tradingRounds;
            int currentRound;
            QTimer *timer=nullptr;
            int frequencyMilliseconds;

        public:
            explicit Asset(QObject *parent=nullptr);
            virtual ~Asset();
            QString getMarket() const;
            void setMarket(const QString &value);
            double getAllocatedCapital() const;
            void setAllocatedCapital(double value);
            double getCapitalPercentage() const;
            void setCapitalPercentage(double value);
            int getTradingRounds() const;
            void setTradingRounds(int value);
            int getCurrentRound() const;
            void setCurrentRound(int value);
            QTimer *getTimer() const;
            void setTimer(QTimer *value);
            int getFrequencyMilliseconds() const;
            void setFrequencyMilliseconds(int value);

        signals:
            void marketChanged(const QString &market);
            void allocatedCapitalChanged(const double &allocatedCapital);
            void capitalPercentageChanged(const double &capitalPercentage);
            void tradingRoundsChanged(const int &tradingRounds);
            void currentRoundChanged(const int &currentRound);
            void timerChanged(const QTimer *timer);
            void frequencyMillisecondsChanged(const int &frequencyMilliseconds);
            void started();
            void updated();
            void stopped();

        public slots:
            virtual void start() = 0;
            virtual void update() = 0;
            virtual void stop() = 0;
    };
}

Q_DECLARE_METATYPE(trader::Asset *)

#endif // ASSET_HPP
