#ifndef PORTFOLIO_HPP
#define PORTFOLIO_HPP

#include <QObject>
#include <QUuid>
#include <QString>
#include <QMultiHash>
#include <QHash>
#include <qqml.h>
#include "position.hpp"


namespace trader { typedef QHash<QString,trader::Position *> PositionHash; }
Q_DECLARE_METATYPE(trader::PositionHash *)

namespace trader { typedef  QMultiHash<QString,trader::Position *> PositionMultiHash; }
Q_DECLARE_METATYPE(trader::PositionMultiHash *)


namespace trader
{
    class Portfolio: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(QString portfolioId READ getPortfolioId WRITE setPortfolioId NOTIFY portfolioIdChanged)
        Q_PROPERTY(QString baseCurrency READ getBaseCurrency WRITE setBaseCurrency NOTIFY baseCurrencyChanged)
        Q_PROPERTY(double initialCapital READ getInitialCapital WRITE setInitialCapital NOTIFY initialCapitalChanged)
        Q_PROPERTY(double equity READ getEquity WRITE setEquity NOTIFY equityChanged)
        Q_PROPERTY(double currentCapital READ getCurrentCapital WRITE setCurrentCapital NOTIFY currentCapitalChanged)
        Q_PROPERTY(double unrealisedPnL READ getUnrealisedPnL WRITE setUnrealisedPnL NOTIFY unrealisedPnLChanged)
        Q_PROPERTY(double realisedPnL READ getRealisedPnL WRITE setRealisedPnL NOTIFY realisedPnLChanged)
        Q_PROPERTY(double returnOnInvestment READ getReturnOnInvestment WRITE setReturnOnInvestment NOTIFY returnOnInvestmentChanged)
        Q_PROPERTY(trader::PositionHash *openPositions READ getOpenPositions WRITE setOpenPositions NOTIFY openPositionsChanged)
        Q_PROPERTY(trader::PositionMultiHash *closedPositions READ getClosedPositions WRITE setClosedPositions NOTIFY closedPositionsChanged)

        private:
            QString name;
            QString portfolioId;
            QString baseCurrency;
            double initialCapital;
            double equity;
            double currentCapital;
            double unrealisedPnL;
            double realisedPnL;
            double returnOnInvestment;
            trader::PositionHash *openPositions=nullptr;
            trader::PositionMultiHash *closedPositions=nullptr;

        public:
            explicit Portfolio(QObject *parent=nullptr);
            explicit Portfolio(QString name,QString baseCurrency,double capital,QObject *parent=nullptr);
            ~Portfolio();
            QString getName() const;
            void setName(const QString &value);
            QString getPortfolioId() const;
            void setPortfolioId(const QString &value);
            QString getBaseCurrency() const;
            void setBaseCurrency(const QString &value);
            double getInitialCapital() const;
            void setInitialCapital(double value);
            double getEquity() const;
            void setEquity(double value);
            double getCurrentCapital() const;
            void setCurrentCapital(double value);
            double getUnrealisedPnL() const;
            void setUnrealisedPnL(double value);
            double getRealisedPnL() const;
            void setRealisedPnL(double value);
            double getReturnOnInvestment() const;
            void setReturnOnInvestment(double value);
            trader::PositionHash *getOpenPositions() const;
            void setOpenPositions(trader::PositionHash *value);
            void clearOpenPositions();
            trader::PositionMultiHash *getClosedPositions() const;
            void setClosedPositions(trader::PositionMultiHash *value);
            void clearClosedPositions();
            void clear();

        signals:
            void nameChanged(const QString &name);
            void portfolioIdChanged(const QString &portfolioId);
            void baseCurrencyChanged(const QString &baseCurrency);
            void initialCapitalChanged(const double &initialCapital);
            void equityChanged(const double &equity);
            void currentCapitalChanged(const double &currentCapital);
            void unrealisedPnLChanged(const double &unrealisedPnL);
            void realisedPnLChanged(const double &realisedPnL);
            void returnOnInvestmentChanged(const double &returnOnInvestment);
            void openPositionsChanged(const trader::PositionHash *openPositions);
            void closedPositionsChanged(const trader::PositionMultiHash *closedPositions);
            void positionUpdated(const QString &market,const trader::Position *position);
            void positionAdded(const QString &market,const trader::Position *position);
            void positionModified(const QString &market,const trader::Position *position);
            void positionTransacted(const QString &market,const trader::Position *position);
            void positionOpened(const QString &market,const trader::Position *position);
            void positionClosed(const QString &market,const trader::Position *position);
            void updated();

        public slots:
            void update();
            bool updatePosition(QString market,double bid,double ask);
            bool addPosition(QString market,trader::Position::Action action,double quantity,double price,double commission,double bid,double ask);
            bool modifyPosition(QString market,trader::Position::Action action,double quantity,double price,double commission,double bid,double ask);
            bool transactPosition(QString market,trader::Position::Action action,double quantity,double price,double commission,double bid,double ask);
    };
}

Q_DECLARE_METATYPE(trader::Portfolio *)

#endif // PORTFOLIO_HPP
