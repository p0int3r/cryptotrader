#ifndef ASSETMANAGER_HPP
#define ASSETMANAGER_HPP

#include <QObject>
#include <QString>
#include <QVariantMap>
#include "asset.hpp"
#include <qqml.h>

namespace trader { typedef QHash<QString,trader::Asset *> AssetHash; }
Q_DECLARE_METATYPE(trader::AssetHash *)


namespace trader
{
    class AssetManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::AssetHash *assets READ getAssets WRITE setAssets NOTIFY assetsChanged)

        private:
            QVariantMap data;
            trader::AssetHash *assets=nullptr;

        public:
            explicit AssetManager(QObject *parent=nullptr);
            ~AssetManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::AssetHash *getAssets() const;
            void setAssets(trader::AssetHash *value);
            void clearAssets();

        signals:
            void dataChanged(const QVariantMap &data);
            void assetsChanged(const trader::AssetHash *assets);
            void assetAdded(const QString &market,const trader::Asset *asset);
            void assetRemoved(const QString &market,const trader::Asset *asset);
            void assetStarted(const QString &market,const trader::Asset *asset);
            void assetUpdated(const QString &market,const trader::Asset *asset);
            void assetStopped(const QString &market,const trader::Asset *asset);

        public slots:
            bool addAsset(QString market,trader::Asset *asset);
            bool removeAsset(QString market);
            trader::Asset *getAsset(QString market);
    };
}

Q_DECLARE_METATYPE(trader::AssetManager *)

#endif // ASSETMANAGER_HPP
