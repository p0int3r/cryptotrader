#ifndef FILTERERMANAGER_HPP
#define FILTERERMANAGER_HPP

#include <QObject>
#include <QString>
#include <QVariantMap>
#include <QHash>
#include <qqml.h>
#include "filterer.hpp"

namespace trader { typedef QHash<QString,trader::Filterer *> FiltererHash; }
Q_DECLARE_METATYPE(trader::FiltererHash *)

namespace trader { typedef QList<trader::Filterer *> SortedFiltererList; }
Q_DECLARE_METATYPE(trader::SortedFiltererList *)

namespace trader
{
    class FiltererManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(trader::FiltererHash *filterers READ getFilterers WRITE setFilterers NOTIFY filterersChanged)
        Q_PROPERTY(trader::SortedFiltererList *sortedFilterers READ getSortedFilterers WRITE setSortedFilterers NOTIFY sortedFilterersChanged)
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)

        private:
            QVariantMap data;
            trader::FiltererHash *filterers=nullptr;
            trader::SortedFiltererList *sortedFilterers=nullptr;

        public:
            explicit FiltererManager(QObject *parent=nullptr);
            ~FiltererManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::FiltererHash *getFilterers() const;
            void setFilterers(trader::FiltererHash *value);
            void clearFilterers();
            trader::SortedFiltererList *getSortedFilterers() const;
            void setSortedFilterers(trader::SortedFiltererList *value);
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void filterersChanged(const trader::FiltererHash *filterers);
            void sortedFilterersChanged(const trader::SortedFiltererList *sortedFilterers);
            void filtererAdded(const QString &name,const trader::Filterer *filterer);
            void filtererRemoved(const QString &name,const trader::Filterer *filterer);

        public slots:
            bool addFilterer(QString name,trader::Filterer *filterer);
            bool removeFilterer(QString name);
            trader::Filterer *getFilterer(QString name);
            void sortByPrecedenceOrder();
    };
}

Q_DECLARE_METATYPE(trader::FiltererManager *)

#endif // FILTERERMANAGER_HPP
