#ifndef RISKMANAGER_HPP
#define RISKMANAGER_HPP

#include <QObject>
#include <QVariantMap>
#include <QHash>
#include <QString>
#include <qqml.h>
#include "risk.hpp"

namespace trader { typedef QHash<QString,trader::Risk *> RiskHash; }
Q_DECLARE_METATYPE(trader::RiskHash *)

namespace trader
{
    class RiskManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::RiskHash *risks READ getRisks WRITE setRisks NOTIFY risksChanged)

        private:
            QVariantMap data;
            trader::RiskHash *risks=nullptr;

        public:
            explicit RiskManager(QObject *parent=nullptr);
            ~RiskManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::RiskHash *getRisks() const;
            void setRisks(trader::RiskHash *value);
            void clearRisks();
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void risksChanged(const trader::RiskHash *risks);
            void riskAdded(const QString &name,trader::Risk *risk);
            void riskRemoved(const QString &name,trader::Risk *risk);

        public slots:
            bool addRisk(QString name,trader::Risk *risk);
            bool removeRisk(QString name);
            trader::Risk *getRisk(QString name);
    };

}

Q_DECLARE_METATYPE(trader::RiskManager *)

#endif // RISKMANAGER_HPP
