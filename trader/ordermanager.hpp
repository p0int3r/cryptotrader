#ifndef ORDERMANAGER_HPP
#define ORDERMANAGER_HPP

#include <QObject>
#include <QHash>
#include <QMultiHash>
#include <QVariantMap>
#include <QString>
#include <qqml.h>
#include "order.hpp"


namespace trader { typedef QHash<QString,trader::Order *> OrderHash; }
Q_DECLARE_METATYPE(trader::OrderHash *)

namespace trader { typedef  QMultiHash<QString,trader::Order *> OrderMarketHash; }
Q_DECLARE_METATYPE(trader::OrderMarketHash *)

namespace trader
{
    class OrderManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::OrderHash *suggestedOrders READ getSuggestedOrders WRITE setSuggestedOrders NOTIFY suggestedOrdersChanged)
        Q_PROPERTY(trader::OrderHash *discardedOrders READ getDiscardedOrders WRITE setDiscardedOrders NOTIFY discardedOrdersChanged)
        Q_PROPERTY(trader::OrderHash *openOrders READ getOpenOrders WRITE setOpenOrders NOTIFY openOrdersChanged)
        Q_PROPERTY(trader::OrderHash *closedOrders READ getClosedOrders WRITE setClosedOrders NOTIFY closedOrdersChanged)
        Q_PROPERTY(trader::OrderHash *canceledOrders READ getCanceledOrders WRITE setCanceledOrders NOTIFY canceledOrdersChanged)
        Q_PROPERTY(trader::OrderMarketHash *suggestedMarketOrders READ getSuggestedMarketOrders WRITE setSuggestedMarketOrders NOTIFY suggestedMarketOrdersChanged)
        Q_PROPERTY(trader::OrderMarketHash *openMarketOrders READ getOpenMarketOrders WRITE setOpenMarketOrders NOTIFY openMarketOrdersChanged)

        private:
            QVariantMap data;
            trader::OrderHash *suggestedOrders=nullptr;
            trader::OrderHash *discardedOrders=nullptr;
            trader::OrderHash *openOrders=nullptr;
            trader::OrderHash *closedOrders=nullptr;
            trader::OrderHash *canceledOrders=nullptr;
            trader::OrderMarketHash *suggestedMarketOrders=nullptr;
            trader::OrderMarketHash *openMarketOrders=nullptr;

        public:
            explicit OrderManager(QObject *parent=nullptr);
            ~OrderManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::OrderHash *getSuggestedOrders() const;
            void setSuggestedOrders(trader::OrderHash *value);
            trader::OrderMarketHash *getSuggestedMarketOrders() const;
            void setSuggestedMarketOrders(trader::OrderMarketHash *value);
            void clearSuggestedOrders();
            trader::OrderHash *getDiscardedOrders() const;
            void setDiscardedOrders(trader::OrderHash *value);
            void clearDiscardedOrders();
            trader::OrderHash *getOpenOrders() const;
            void setOpenOrders(trader::OrderHash *value);
            trader::OrderMarketHash *getOpenMarketOrders() const;
            void setOpenMarketOrders(trader::OrderMarketHash *value);
            void clearOpenOrders();
            trader::OrderHash *getClosedOrders() const;
            void setClosedOrders(trader::OrderHash *value);
            void clearClosedOrders();
            trader::OrderHash *getCanceledOrders() const;
            void setCanceledOrders(trader::OrderHash *value);
            void clearCanceledOrders();
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void suggestedOrdersChanged(const trader::OrderHash *suggestedOrders);
            void discardedOrdersChanged(const trader::OrderHash *discardedOrders);
            void openOrdersChanged(const trader::OrderHash *openOrders);
            void closedOrdersChanged(const trader::OrderHash *closedOrders);
            void canceledOrdersChanged(const trader::OrderHash *canceledOrders);
            void suggestedMarketOrdersChanged(const trader::OrderMarketHash *suggestedMarketOrders);
            void openMarketOrdersChanged(const trader::OrderMarketHash *openMarketOrders);
            void suggested(const QString &orderId,const trader::Order *order);
            void discarded(const QString &orderId,const trader::Order *order);
            void opened(const QString &orderId,const trader::Order *order);
            void updated(const QString &orderId,const trader::Order *order);
            void closed(const QString &orderId,const trader::Order *order);
            void canceled(const QString &orderId,const trader::Order *order);

        public slots:
            bool containsSuggestedMarketOrder(QString market);
            bool containsOpenMarketOrder(QString market);
            bool removeSuggestedMarketOrder(QString market);
            bool removeOpenMarketOrder(QString market);
            bool suggest(const QString &orderId,const trader::Order *order);
            bool discard(const QString &orderId,const trader::Order *order);
            bool open(const QString &orderId,const trader::Order *order);
            bool update(const QString &orderId,const trader::Order *order);
            bool close(const QString &orderId,const trader::Order *order);
            bool cancel(const QString &orderId,const trader::Order *order);
    };
}

Q_DECLARE_METATYPE(trader::OrderManager *)

#endif // ORDERMANAGER_HPP
