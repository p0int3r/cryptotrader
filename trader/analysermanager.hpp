#ifndef ANALYSERMANAGER_HPP
#define ANALYSERMANAGER_HPP

#include <QObject>
#include <QHash>
#include <QVariantMap>
#include <qqml.h>
#include "analyser.hpp"

namespace trader { typedef QHash<QString,trader::Analyser *> AnalyserHash; }
Q_DECLARE_METATYPE(trader::AnalyserHash *)

namespace trader
{
    class AnalyserManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::AnalyserHash *analysers READ getAnalysers WRITE setAnalysers NOTIFY analysersChanged)

        private:
            QVariantMap data;
            trader::AnalyserHash *analysers=nullptr;

        public:
            explicit AnalyserManager(QObject *parent=nullptr);
            ~AnalyserManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::AnalyserHash *getAnalysers() const;
            void setAnalysers(trader::AnalyserHash *value);
            void clearAnalysers();
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void analysersChanged(const trader::AnalyserHash *analysers);
            void analyserAdded(const QString &market,const trader::Analyser *analyser);
            void analyserRemoved(const QString &marke,const trader::Analyser *analyser);

        public slots:
            bool addAnalyser(QString market,trader::Analyser *analyser);
            bool removeAnalyser(QString market);
            trader::Analyser *getAnalyser(QString market);
    };
}

Q_DECLARE_METATYPE(trader::AnalyserManager *)

#endif // ANALYSERMANAGER_HPP
