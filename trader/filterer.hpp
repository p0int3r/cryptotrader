#ifndef FILTERER_HPP
#define FILTERER_HPP

#include <QObject>
#include <QString>
#include <qqml.h>

namespace trader
{
    class Filterer: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)
        Q_PROPERTY(int precedenceOrder READ getPrecedenceOrder WRITE setPrecedenceOrder NOTIFY precedenceOrderChanged)

        private:
            QString name;
            QString description;
            int precedenceOrder;

        public:
            explicit Filterer(QObject *parent=nullptr);
            explicit Filterer(QString name,QString description,int precedenceOrder,QObject *parent=nullptr);
            virtual ~Filterer();
            QString getName() const;
            void setName(const QString &value);
            QString getDescription() const;
            void setDescription(const QString &value);
            int getPrecedenceOrder() const;
            void setPrecedenceOrder(int value);

        signals:
            void nameChanged(const QString &name);
            void descriptionChanged(const QString &description);
            void precedenceOrderChanged(const int &precedenceOrder);
            void accepted(const QString &market,const QObject *object);
            void declined(const QString &market,const QObject *object);

        public slots:
            virtual void filter(const QString &market,const QObject *object) = 0;
    };
}

Q_DECLARE_METATYPE(trader::Filterer *)

#endif // FILTERER_HPP
