#include "execution.hpp"
#include <cmath>


trader::Execution::Execution(QObject *parent): QObject(parent)
{
    this->setTimer(new QTimer());
    this->setElapsedTimer(new QElapsedTimer());
    this->connect(this->timer,&QTimer::timeout,this,&trader::Execution::update);
    this->connect(this,&trader::Execution::canceled,this,&trader::Execution::stop);
    this->connect(this,&trader::Execution::closed,this,&trader::Execution::stop);

    this->connect(this->timer,&QTimer::timeout,this,[=]()
    {
        long long elapsed_milliseconds=this->getElapsedTimer()->elapsed();
        int cancel_after_milliseconds=this->getCancelAfterMilliseconds();

        if (elapsed_milliseconds>=cancel_after_milliseconds)
        {
            double quantity_filled=this->getOrder()->getQuantityFilled();
            if (qFuzzyIsNull(quantity_filled)==true) { this->cancel(); }
            else { this->getElapsedTimer()->restart(); }
        }
    });

    this->connect(this,&trader::Execution::placed,this,[=]()
    {
        this->getTimer()->start(this->getFrequencyMilliseconds());
        this->getElapsedTimer()->start();
    });
}

trader::Execution::~Execution()
{
    this->order=nullptr;
    delete this->timer;
    delete this->elapsedTimer;
}


QTimer *trader::Execution::getTimer() const { return this->timer; }

void trader::Execution::setTimer(QTimer *value)
{
    this->timer=value;
    emit this->timerChanged(this->timer);
}

trader::Order *trader::Execution::getOrder() const { return this->order; }

void trader::Execution::setOrder(trader::Order *value)
{
    this->order=value;
    emit this->orderChanged(this->order);
}

QElapsedTimer *trader::Execution::getElapsedTimer() const { return this->elapsedTimer; }

void trader::Execution::setElapsedTimer(QElapsedTimer *value)
{
    this->elapsedTimer=value;
    emit this->elapsedTimerChanged(this->elapsedTimer);
}


int trader::Execution::getCancelAfterMilliseconds() const
{
    return this->cancelAfterMilliseconds;
}

void trader::Execution::setCancelAfterMilliseconds(int value)
{
    cancelAfterMilliseconds=value;
    emit this->cancelAfterMillisecondsChanged(this->cancelAfterMilliseconds);
}

int trader::Execution::getFrequencyMilliseconds() const { return this->frequencyMilliseconds; }

void trader::Execution::setFrequencyMilliseconds(int value)
{
    this->frequencyMilliseconds=value;
    emit this->frequencyMillisecondsChanged(this->frequencyMilliseconds);
}
