#-------------------------------------------------
#
# Project created by QtCreator 2019-06-29T19:52:46
#
#-------------------------------------------------

QT -= gui
QT += quick

TARGET = trader
TEMPLATE = lib
CONFIG += staticlib c++2a qmltypes

QML_IMPORT_NAME = cryptotrader.trader
QML_IMPORT_MAJOR_VERSION = 1
QML_IMPORT_MINOR_VERSION = 0

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

QMAKE_CXXFLAGS += -Wno-deprecated -Wno-deprecated-copy -Wno-deprecated-declarations

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        analyser.cpp \
        analysermanager.cpp \
        artificialintelligenceanalysis.cpp \
        asset.cpp \
        assetmanager.cpp \
        execution.cpp \
        executionmanager.cpp \
        filterer.cpp \
        filterermanager.cpp \
        macroeconomicanalysis.cpp \
        microeconomicanalysis.cpp \
        order.cpp \
        ordermanager.cpp \
        portfolio.cpp \
        portfoliomanager.cpp \
        position.cpp \
        risk.cpp \
        riskmanager.cpp \
        sentimentanalysis.cpp \
        sizer.cpp \
        sizermanager.cpp \
        strategy.cpp \
        strategymanager.cpp \
        trader.cpp

HEADERS += \
        candlestick.hpp \
        analyser.hpp \
        analysermanager.hpp \
        artificialintelligenceanalysis.hpp \
        asset.hpp \
        assetmanager.hpp \
        execution.hpp \
        executionmanager.hpp \
        filterer.hpp \
        filterermanager.hpp \
        macroeconomicanalysis.hpp \
        microeconomicanalysis.hpp \
        order.hpp \
        ordermanager.hpp \
        portfolio.hpp \
        portfoliomanager.hpp \
        position.hpp \
        risk.hpp \
        riskmanager.hpp \
        sentimentanalysis.hpp \
        sizer.hpp \
        sizermanager.hpp \
        strategy.hpp \
        strategymanager.hpp \
        timeseries.hpp \
        trader.hpp \
        trader_traits.hpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH += $$PWD/../../../../../Packages/Boost/boost_1_74_0_modified-build/include
DEPENDPATH += $$PWD/../../../../../Packages/Boost/boost_1_74_0_modified-build/include
