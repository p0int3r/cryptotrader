#include "executionmanager.hpp"



trader::ExecutionManager::ExecutionManager(QObject *parent): QObject(parent)
{
    this->setData(QVariantMap());
    this->setExecutions(new trader::ExecutionHash());
}

trader::ExecutionManager::~ExecutionManager()
{
    this->clear();
    delete this->executions;
}

QVariantMap trader::ExecutionManager::getData() const { return this->data; }

void trader::ExecutionManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::ExecutionHash *trader::ExecutionManager::getExecutions() const { return this->executions; }

void trader::ExecutionManager::setExecutions(trader::ExecutionHash *value)
{
    this->executions=value;
    emit this->executionsChanged(this->executions);
}


void trader::ExecutionManager::clearExecutions()
{
    trader::ExecutionHash::iterator iterator;
    for (iterator=this->getExecutions()->begin();iterator!=this->getExecutions()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getExecutions()->clear();
}

bool trader::ExecutionManager::addExecution(QString orderId,trader::Execution *execution)
{
    bool contains_execution=this->getExecutions()->contains(orderId);
    if (contains_execution==true) { return false; }
    if (execution==nullptr) { return false; }
    this->getExecutions()->insert(orderId,execution);
    this->connect(execution,&trader::Execution::opened,this,[=]() { emit this->opened(orderId,execution->getOrder()); });
    this->connect(execution,&trader::Execution::closed,this,[=]() { emit this->closed(orderId,execution->getOrder()); });
    this->connect(execution,&trader::Execution::canceled,this,[=]() { emit this->canceled(orderId,execution->getOrder()); });
    this->connect(execution,&trader::Execution::updated,this,[=]() { emit this->updated(orderId,execution->getOrder()); });
    this->connect(execution,&trader::Execution::stopped,this,[=]() { emit this->stopped(orderId,execution->getOrder()); });
    emit this->executionAdded(orderId,execution);
    return true;
}

bool trader::ExecutionManager::removeExecution(QString orderId)
{
    bool contains_execution=this->getExecutions()->contains(orderId);
    if (contains_execution==false) { return false; }
    trader::Execution *execution=this->getExecutions()->value(orderId);
    this->getExecutions()->remove(orderId);
    emit this->executionRemoved(orderId,execution);
    execution->deleteLater();
    return true;
}


trader::Execution *trader::ExecutionManager::getExecution(QString orderId)
{
    bool contains_execution=this->getExecutions()->contains(orderId);
    if (contains_execution==false) { return nullptr; }
    trader::Execution *execution=this->getExecutions()->value(orderId);
    return execution;
}

void trader::ExecutionManager::clear()
{
    this->setData(QVariantMap());
    this->clearExecutions();
}
