#include "filterer.hpp"



trader::Filterer::Filterer(QObject *parent): trader::Filterer(QString(),QString(),0,parent) {}

trader::Filterer::Filterer(QString name,QString description,int precedenceOrder,QObject *parent): QObject(parent)
{
    this->setName(name);
    this->setDescription(description);
    this->setPrecedenceOrder(precedenceOrder);
}

trader::Filterer::~Filterer() = default;

QString trader::Filterer::getName() const { return this->name; }

void trader::Filterer::setName(const QString &value)
{
    this->name=value;
    emit this->nameChanged(this->name);
}

QString trader::Filterer::getDescription() const { return this->description; }

void trader::Filterer::setDescription(const QString &value)
{
    this->description=value;
    emit this->descriptionChanged(this->description);
}

int trader::Filterer::getPrecedenceOrder() const { return this->precedenceOrder; }

void trader::Filterer::setPrecedenceOrder(int value)
{
    this->precedenceOrder=value;
    emit this->precedenceOrderChanged(this->precedenceOrder);
}
