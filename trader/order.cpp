#include "order.hpp"


trader::Order::Order(QObject *parent): trader::Order(QString(),0.0,0.0,0.0,0.0,trader::Order::Type::NONE,trader::Order::Exchange::NONE,trader::Order::Status::NONE,parent) {}

trader::Order::Order(QString market,double quantity,double price,double precision,double minimumTradeSize,
    trader::Order::Type type,trader::Order::Exchange exchange,trader::Order::Status status,QObject *parent): QObject(parent)
{
    this->setMarket(market);
    this->setQuantity(quantity);
    this->setQuantityFilled(0.0);
    this->setPrecision(precision);
    this->setMinimumTradeSize(minimumTradeSize);
    this->setPrice(price);
    this->setType(type);
    this->setExchange(exchange);
    this->setStatus(status);
    this->setOrderId(QUuid::createUuid().toString());
    this->setExchangeOrderId(QString());
    this->setTimeStamp(QDateTime::currentDateTime());
    this->setOpened(QDateTime());
    this->setClosed(QDateTime());
    this->setData(QJsonObject());
}

trader::Order::~Order() = default;

trader::Order::Status trader::Order::getStatus() const { return this->status; }

void trader::Order::setStatus(const trader::Order::Status &value)
{
        this->status=value;
        emit this->statusChanged(this->status);
}

trader::Order::Type trader::Order::getType() const { return this->type; }

void trader::Order::setType(const trader::Order::Type &value)
{
    this->type=value;
    emit this->typeChanged(this->type);
}

trader::Order::Exchange trader::Order::getExchange() const { return this->exchange; }

void trader::Order::setExchange(const trader::Order::Exchange &value)
{
    this->exchange=value;
    emit this->exchangeChanged(this->exchange);
}

QString trader::Order::getMarket() const { return this->market; }

void trader::Order::setMarket(const QString &value)
{
    this->market=value;
    emit this->marketChanged(this->market);
}

double trader::Order::getQuantity() const { return this->quantity; }

void trader::Order::setQuantity(double value)
{
    this->quantity=value;
    emit this->quantityChanged(this->quantity);
}

double trader::Order::getQuantityFilled() const { return this->quantityFilled; }

void trader::Order::setQuantityFilled(double value)
{
    this->quantityFilled=value;
    emit this->quantityFilledChanged(this->quantityFilled);
}

double trader::Order::getPrice() const { return this->price; }

void trader::Order::setPrice(double value)
{
    this->price=value;
    emit this->priceChanged(this->price);
}

double trader::Order::getPrecision() const { return this->precision; }

void trader::Order::setPrecision(double value)
{
    this->precision=value;
    emit this->precisionChanged(this->precision);
}

double trader::Order::getMinimumTradeSize() const { return this->minimumTradeSize; }

void trader::Order::setMinimumTradeSize(double value)
{
    this->minimumTradeSize=value;
    emit this->minimumTradeSizeChanged(this->minimumTradeSize);
}


QString trader::Order::getOrderId() const { return this->orderId; }

void trader::Order::setOrderId(const QString &value)
{
    this->orderId=value;
    emit this->orderIdChanged(this->orderId);
}

QString trader::Order::getExchangeOrderId() const { return this->exchangeOrderId; }

void trader::Order::setExchangeOrderId(const QString &value)
{
    this->exchangeOrderId=value;
    emit this->exchangeOrderIdChanged(this->exchangeOrderId);
}

QJsonObject trader::Order::getData() const { return this->data; }

void trader::Order::setData(const QJsonObject &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

QDateTime trader::Order::getTimeStamp() const { return this->timeStamp; }

void trader::Order::setTimeStamp(const QDateTime &value)
{
    this->timeStamp=value;
    emit this->timeStampChanged(this->timeStamp);
}

QDateTime trader::Order::getOpened() const { return this->opened; }

void trader::Order::setOpened(const QDateTime &value)
{
    this->opened=value;
    emit this->openedChanged(this->opened);
}

QDateTime trader::Order::getClosed() const { return this->closed; }

void trader::Order::setClosed(const QDateTime &value)
{
    this->closed=value;
    emit this->closedChanged(this->closed);
}
