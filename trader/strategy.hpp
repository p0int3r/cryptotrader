#ifndef STRATEGY_HPP
#define STRATEGY_HPP

#include <QObject>
#include <QString>
#include <order.hpp>
#include <qqml.h>

namespace trader
{
    class Strategy: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)
        Q_PROPERTY(trader::Strategy::FrequencyLevel frequencyLevel READ getFrequencyLevel WRITE setFrequencyLevel NOTIFY frequencyLevelChanged)

        public:

            enum class FrequencyLevel
            {
                LOW,
                MEDIUM,
                HIGH,
                NONE
            }; Q_ENUM(FrequencyLevel)

        private:
            QString name;
            QString description;
            trader::Strategy::FrequencyLevel frequencyLevel;

        public:
            explicit Strategy(QObject *parent=nullptr);
            explicit Strategy(QString name,QString description,
                FrequencyLevel frequencyLevel,QObject *parent=nullptr);
            virtual ~Strategy();
            QString getName() const;
            void setName(const QString &value);
            QString getDescription() const;
            void setDescription(const QString &value);
            trader::Strategy::FrequencyLevel getFrequencyLevel() const;
            void setFrequencyLevel(const trader::Strategy::FrequencyLevel &value);

        signals:
            void nameChanged(const QString &name);
            void descriptionChanged(const QString &description);
            void frequencyLevelChanged(const trader::Strategy::FrequencyLevel &frequencyLevel);
            void preprocessed(const QString &market,const QObject *object);
            void strategised(const QString &market,const QObject *object);

        public slots:
            virtual void preprocess(const QString &market,const QObject *object) = 0;
            virtual void strategise(const QString &market,const QObject *object) = 0;
    };
}

Q_DECLARE_METATYPE(trader::Strategy *)

#endif // STRATEGY_HPP
