#include "sizer.hpp"


trader::Sizer::Sizer(QObject *parent): trader::Sizer(QString(),QString(),parent) {}

trader::Sizer::Sizer(QString name,QString description,QObject *parent): QObject(parent)
{
    this->setName(name);
    this->setDescription(description);
}

trader::Sizer::~Sizer() = default;

QString trader::Sizer::getName() const { return this->name; }

void trader::Sizer::setName(const QString &value)
{
    this->name=value;
    emit this->nameChanged(this->name);
}

QString trader::Sizer::getDescription() const { return this->description; }

void trader::Sizer::setDescription(const QString &value)
{
    this->description=value;
    emit this->descriptionChanged(this->description);
}
