#ifndef STRATEGYMANAGER_HPP
#define STRATEGYMANAGER_HPP

#include <QObject>
#include <QVariantMap>
#include <QHash>
#include <QString>
#include <qqml.h>
#include "strategy.hpp"

namespace trader { typedef QHash<QString,trader::Strategy *> StrategyHash; }
Q_DECLARE_METATYPE(trader::StrategyHash *)

namespace trader
{
    class StrategyManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::StrategyHash *strategies READ getStrategies WRITE setStrategies NOTIFY strategiesChanged)

        private:
            QVariantMap data;
            trader::StrategyHash *strategies=nullptr;

        public:
            explicit StrategyManager(QObject *parent=nullptr);
            ~StrategyManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::StrategyHash *getStrategies() const;
            void setStrategies(trader::StrategyHash *value);
            void clearStrategies();
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void strategiesChanged(const trader::StrategyHash *strategies);
            void strategyAdded(const QString &name,const trader::Strategy *strategy);
            void strategyRemoved(const QString &name,const trader::Strategy *strategy);

        public slots:
            bool addStrategy(QString name,trader::Strategy *strategy);
            bool removeStrategy(QString name);
            trader::Strategy *getStrategy(QString name);
    };
}

Q_DECLARE_METATYPE(trader::StrategyManager *)

#endif // STRATEGYMANAGER_HPP
