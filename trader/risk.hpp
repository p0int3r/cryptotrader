 #ifndef RISK_HPP
#define RISK_HPP

#include <QObject>
#include <QString>
#include <qqml.h>

namespace trader
{
    class Risk: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)
        Q_PROPERTY(trader::Risk::RiskIndicator riskIndicator READ getRiskIndicator WRITE setRiskIndicator NOTIFY riskIndicatorChanged)

        public:

            enum class RiskIndicator
            {
                LOW,
                MEDIUM,
                HIGH,
                NONE,
            }; Q_ENUM(RiskIndicator)

        private:
            QString         name;
            QString         description;
            RiskIndicator   riskIndicator;

        public:
            explicit Risk(QObject *parent=nullptr);
            explicit Risk(QString name,QString description,trader::Risk::RiskIndicator riskIndicator,QObject *parent=nullptr);
            virtual ~Risk();
            QString getName() const;
            void setName(const QString &value);
            QString getDescription() const;
            void setDescription(const QString &value);
            trader::Risk::RiskIndicator getRiskIndicator() const;
            void setRiskIndicator(const trader::Risk::RiskIndicator &value);

        signals:
            void nameChanged(const QString &name);
            void descriptionChanged(const QString &description);
            void riskIndicatorChanged(const trader::Risk::RiskIndicator &riskIndicator);
            void preprocessed(const QString &market,const QObject *object);
            void assessed(const QString &market,const QObject *object);

        public slots:
            virtual void preprocess(const QString &market,const QObject *object) = 0;
            virtual void assess(const QString &market,const QObject *object) = 0;
    };
}

Q_DECLARE_METATYPE(trader::Risk *)

#endif // RISK_HPP
