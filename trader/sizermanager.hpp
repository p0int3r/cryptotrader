#ifndef SIZERMANAGER_HPP
#define SIZERMANAGER_HPP

#include <QObject>
#include <QString>
#include <QVariantMap>
#include <QHash>
#include <qqml.h>
#include "sizer.hpp"

namespace trader { typedef QHash<QString,trader::Sizer *> SizerHash; }
Q_DECLARE_METATYPE(trader::SizerHash *)

namespace trader
{
    class SizerManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(trader::SizerHash *sizers READ getSizers WRITE setSizers NOTIFY sizersChanged)
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)

        private:
            QVariantMap data;
            trader::SizerHash *sizers=nullptr;

        public:
            explicit SizerManager(QObject *parent=nullptr);
            ~SizerManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::SizerHash *getSizers() const;
            void setSizers(trader::SizerHash *value);
            void clearSizers();
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void sizersChanged(const trader::SizerHash *sizers);
            void sizerAdded(const QString &name,const trader::Sizer *sizer);
            void sizerRemoved(const QString &name,const trader::Sizer *sizer);

        public slots:
            bool addSizer(QString name,trader::Sizer *sizer);
            bool removeSizer(QString name);
            trader::Sizer *getSizer(QString name);
    };
}

Q_DECLARE_METATYPE(trader::SizerManager *)

#endif // SIZERMANAGER_HPP
