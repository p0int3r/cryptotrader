#include "portfoliomanager.hpp"


trader::PortfolioManager::PortfolioManager(QObject *parent): QObject(parent)
{
    this->setPortfolios(new trader::PortfolioHash());
    this->setData(QVariantMap());
}

trader::PortfolioManager::~PortfolioManager()
{
    this->clear();
    delete this->portfolios;
}

QVariantMap trader::PortfolioManager::getData() const { return this->data; }

void trader::PortfolioManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::PortfolioHash *trader::PortfolioManager::getPortfolios() const { return this->portfolios; }

void trader::PortfolioManager::setPortfolios(trader::PortfolioHash *value)
{
    this->portfolios=value;
    emit this->portfoliosChanged(this->portfolios);
}

void trader::PortfolioManager::clearPortfolios()
{
    trader::PortfolioHash::iterator iterator;
    for (iterator=this->getPortfolios()->begin();iterator!=this->getPortfolios()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getPortfolios()->clear();
}

bool trader::PortfolioManager::addPortfolio(QString name,trader::Portfolio *portfolio)
{
    bool contains_portfolio=this->getPortfolios()->contains(name);
    if (contains_portfolio==true) { return false; }
    if (portfolio==nullptr) { return false; }
    this->getPortfolios()->insert(name,portfolio);
    emit this->portfolioAdded(name,portfolio);
    return true;
}

bool trader::PortfolioManager::removePortfolio(QString name)
{
    bool contains_portfolio=this->getPortfolios()->contains(name);
    if (contains_portfolio==false) { return false; }
    trader::Portfolio *portfolio=this->getPortfolios()->value(name);
    this->getPortfolios()->remove(name);
    emit this->portfolioRemoved(name,portfolio);
    portfolio->deleteLater();
    return true;
}


trader::Portfolio *trader::PortfolioManager::getPortfolio(QString name)
{
    bool contains_portfolio=this->getPortfolios()->contains(name);
    if (contains_portfolio==false) { return nullptr; }
    trader::Portfolio *portfolio=this->getPortfolios()->value(name);
    return portfolio;
}


void trader::PortfolioManager::clear()
{
    this->setData(QVariantMap());
    this->clearPortfolios();
}
