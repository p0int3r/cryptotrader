#include "position.hpp"



trader::Position::Position(QObject *parent): trader::Position(QString(),trader::Position::Action::NONE,0.0,0.0,0.0,0.0,0.0,parent) {}

trader::Position::Position(QString market,trader::Position::Action action,double quantity,
    double price,double commission,double bid,double ask,QObject *parent): QObject(parent)
{
    this->setMiddlePrice(0.0);
    this->setAsk(0.0);
    this->setBid(0.0);
    this->setRealisedPnL(0.0);
    this->setUnrealisedPnL(0.0);
    this->setBuys(0.0);
    this->setSells(0.0);
    this->setAverageBoughtCost(0.0);
    this->setAverageSoldCost(0.0);
    this->setTotalBoughtCost(0.0);
    this->setTotalSoldCost(0.0);
    this->setTotalBoughtCost(0.0);
    this->setAveragePrice(0.0);
    this->setCostBasis(0.0);
    this->setNet(0.0);
    this->setNetTotal(0.0);
    this->setNetInclusiveCommission(0.0);
    this->setMarketValue(0.0);
    this->setMarket(market);
    this->setAction(action);
    this->setQuantity(quantity);
    this->setPrice(price);
    this->setCommission(commission);
    this->initiate();
    this->update(bid,ask);
}

trader::Position::~Position() = default;

trader::Position::Action trader::Position::getAction() const { return this->action; }

void trader::Position::setAction(const trader::Position::Action &value)
{
    this->action=value;
    emit this->actionChanged(this->action);
}

double trader::Position::getQuantity() const { return this->quantity; }

void trader::Position::setQuantity(double value)
{
    this->quantity=value;
    emit this->quantityChanged(this->quantity);
}

double trader::Position::getPrice() const { return this->price; }

void trader::Position::setPrice(double value)
{
    this->price=value;
    emit this->priceChanged(this->price);
}

double trader::Position::getMiddlePrice() const { return this->middlePrice; }

void trader::Position::setMiddlePrice(double value)
{
    this->middlePrice=value;
    emit this->middlePriceChanged(this->middlePrice);
}

double trader::Position::getBid() const { return this->bid; }

void trader::Position::setBid(double value)
{
    this->bid=value;
    emit this->bidChanged(this->bid);
}

double trader::Position::getAsk() const { return this->ask; }

void trader::Position::setAsk(double value)
{
    this->ask=value;
    emit this->askChanged(this->ask);
}

double trader::Position::getCommission() const { return this->commission; }

void trader::Position::setCommission(double value)
{
    this->commission=value;
    emit this->commissionChanged(this->commission);
}

double trader::Position::getRealisedPnL() const { return this->realisedPnL; }

void trader::Position::setRealisedPnL(double value)
{
    this->realisedPnL=value;
    emit this->realisedPnLChanged(this->realisedPnL);
}

double trader::Position::getUnrealisedPnL() const { return this->unrealisedPnL; }

void trader::Position::setUnrealisedPnL(double value)
{
    this->unrealisedPnL=value;
    emit this->unrealisedPnLChanged(this->unrealisedPnL);
}

double trader::Position::getBuys() const { return this->buys; }

void trader::Position::setBuys(double value)
{
    this->buys=value;
    emit this->buysChanged(this->buys);
}

double trader::Position::getSells() const { return this->sells; }

void trader::Position::setSells(double value)
{
    this->sells=value;
    emit this->sellsChanged(this->sells);
}

double trader::Position::getAverageBoughtCost() const { return this->averageBoughtCost; }

void trader::Position::setAverageBoughtCost(double value)
{
    this->averageBoughtCost=value;
    emit this->averageBoughtCostChanged(this->averageBoughtCost);
}

double trader::Position::getAverageSoldCost() const { return this->averageSoldCost; }

void trader::Position::setAverageSoldCost(double value)
{
    this->averageSoldCost=value;
    emit this->averageSoldCostChanged(this->averageSoldCost);
}

double trader::Position::getTotalBoughtCost() const { return this->totalBoughtCost; }

void trader::Position::setTotalBoughtCost(double value)
{
    this->totalBoughtCost=value;
    emit this->totalBoughtCostChanged(this->totalBoughtCost);
}

double trader::Position::getTotalSoldCost() const { return this->totalSoldCost; }

void trader::Position::setTotalSoldCost(double value)
{
    this->totalSoldCost=value;
    emit this->totalSoldCostChanged(this->totalSoldCost);
}

double trader::Position::getAveragePrice() const { return this->averagePrice; }

void trader::Position::setAveragePrice(double value)
{
    this->averagePrice=value;
    emit this->averagePriceChanged(this->averagePrice);
}

double trader::Position::getCostBasis() const { return this->costBasis; }

void trader::Position::setCostBasis(double value)
{
    this->costBasis=value;
    emit this->costBasisChanged(this->costBasis);
}

double trader::Position::getNet() const { return this->net; }

void trader::Position::setNet(double value)
{
    this->net=value;
    emit this->netChanged(this->net);
}

double trader::Position::getNetTotal() const { return this->netTotal; }

void trader::Position::setNetTotal(double value)
{
    this->netTotal=value;
    emit this->netTotalChanged(this->netTotal);
}

double trader::Position::getNetInclusiveCommission() const { return this->netInclusiveCommission; }

void trader::Position::setNetInclusiveCommission(double value)
{
    this->netInclusiveCommission=value;
    emit this->netInclusiveCommissionChanged(this->netInclusiveCommission);
}

double trader::Position::getMarketValue() const { return this->marketValue; }

void trader::Position::setMarketValue(double value)
{
    this->marketValue=value;
    emit this->marketValueChanged(this->marketValue);
}

QString trader::Position::getMarket() const { return this->market; }

void trader::Position::setMarket(const QString &value)
{
    this->market=value;
    emit this->marketChanged(this->market);
}



void trader::Position::initiate()
{
    if (this->getAction()==trader::Position::Action::BOUGHT)
    {
        this->setBuys(this->getQuantity());
        this->setAverageBoughtCost(this->getPrice());
        this->setTotalBoughtCost(this->getBuys()*this->getAverageBoughtCost());
        double average_price=(this->getPrice()*this->getQuantity()+this->getCommission())/this->getQuantity();
        this->setAveragePrice(average_price);
        this->setCostBasis(this->getQuantity()*this->getAveragePrice());
    }
    else if (this->getAction()==trader::Position::Action::SOLD)
    {
        this->setSells(this->getQuantity());
        this->setAverageSoldCost(this->getPrice());
        this->setTotalSoldCost(this->getSells()*this->getAverageSoldCost());
        double average_price=(this->getPrice()*this->getQuantity()-this->getCommission())/this->getQuantity();
        this->setAveragePrice(average_price);
        this->setCostBasis(-1.0*this->getQuantity()*this->getAveragePrice());
    }

    this->setNet(this->getBuys()-this->getSells());
    this->setNetTotal(this->getTotalSoldCost()-this->getTotalBoughtCost());
    this->setNetInclusiveCommission(this->getNetTotal()-this->getCommission());
}



void trader::Position::update(double bid,double ask)
{
    double sign=0.0;
    double middle_price=(bid+ask)/2.0;
    this->setBid(bid);
    this->setAsk(ask);
    this->setMiddlePrice(middle_price);
    double net=this->getNet();
    if (net==0.0) { sign=0.0; }
    else if (net<0.0) { sign=-1.0; }
    else if (net>0.0) { sign=1.0; }
    this->setMarketValue(this->getQuantity()*middle_price*sign);
    this->setUnrealisedPnL(this->getMarketValue()-this->getCostBasis());
    emit this->updated();
}



void trader::Position::transact(trader::Position::Action action,double quantity,double price,double commission)
{
    this->setCommission(this->getCommission()+commission);

    if (action==trader::Position::Action::BOUGHT)
    {
        double average_bought_cost=(this->getAveragePrice()*this->getBuys()+price*quantity)/(this->getBuys()+quantity);
        this->setAverageBoughtCost(average_bought_cost);

        if (this->getAction()==trader::Position::Action::BOUGHT)
        {
            double average_price=(this->getAveragePrice()*this->getBuys()+price*quantity+commission)/(this->getBuys()+quantity);
            this->setAveragePrice(average_price);
        }
        else if (this->getAction()==trader::Position::Action::SOLD)
        {
            double realised_pnl=this->getRealisedPnL()+quantity*(this->getAveragePrice()-price)-commission;
            this->setRealisedPnL(realised_pnl);
        }

        this->setBuys(this->getBuys()+quantity);
        this->setTotalBoughtCost(this->getBuys()*this->getAverageBoughtCost());
    }
    else if (action==trader::Position::Action::SOLD)
    {
        double average_sold_cost=(this->getAverageSoldCost()*this->getSells()+price*quantity)/(this->getSells()+quantity);
        this->setAverageSoldCost(average_sold_cost);

        if (this->getAction()==trader::Position::Action::SOLD)
        {
            double average_price=(this->getAveragePrice()*this->getSells()+price*quantity-commission)/(this->getSells()+quantity);
            this->setAveragePrice(average_price);
        }
        else if (this->getAction()==trader::Position::Action::BOUGHT)
        {
            double realised_pnl=this->getRealisedPnL()+quantity*(price-this->getAveragePrice())-commission;
            this->setRealisedPnL(realised_pnl);
        }

        this->setSells(this->getSells()+quantity);
        this->setTotalSoldCost(this->getSells()*this->getAverageSoldCost());
    }

    this->setNet(this->getBuys()-this->getSells());
    this->setQuantity(this->getNet());
    this->setNetTotal(this->getTotalSoldCost()-this->getTotalBoughtCost());
    this->setNetInclusiveCommission(this->getNetTotal()-this->getCommission());
    this->setCostBasis(this->getQuantity()*this->getAveragePrice());
    emit this->transacted();
}
