#ifndef ORDER_HPP
#define ORDER_HPP

#include <QJsonObject>
#include <QString>
#include <QObject>
#include <QUuid>
#include <QDateTime>
#include <qqml.h>

namespace trader
{   
    class Order: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString orderId READ getOrderId WRITE setOrderId NOTIFY orderIdChanged)
        Q_PROPERTY(QString exchangeOrderId READ getExchangeOrderId WRITE setExchangeOrderId NOTIFY exchangeOrderIdChanged)
        Q_PROPERTY(trader::Order::Status status READ getStatus WRITE setStatus NOTIFY statusChanged)
        Q_PROPERTY(trader::Order::Type type READ getType WRITE setType NOTIFY typeChanged)
        Q_PROPERTY(trader::Order::Exchange exchange READ getExchange WRITE setExchange NOTIFY exchangeChanged)
        Q_PROPERTY(QString market READ getMarket WRITE setMarket NOTIFY marketChanged)
        Q_PROPERTY(double quantity READ getQuantity WRITE setQuantity NOTIFY quantityChanged)
        Q_PROPERTY(double quantityFilled READ getQuantityFilled WRITE setQuantityFilled NOTIFY quantityFilledChanged)
        Q_PROPERTY(double price READ getPrice WRITE setPrice NOTIFY priceChanged)
        Q_PROPERTY(double precision READ getPrecision WRITE setPrecision NOTIFY precisionChanged)
        Q_PROPERTY(double minimumTradeSize READ getMinimumTradeSize WRITE setMinimumTradeSize NOTIFY minimumTradeSizeChanged)
        Q_PROPERTY(QJsonObject data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(QDateTime timeStamp READ getTimeStamp WRITE setTimeStamp NOTIFY timeStampChanged)
        Q_PROPERTY(QDateTime opened READ getOpened WRITE setOpened NOTIFY openedChanged)
        Q_PROPERTY(QDateTime closed READ getClosed WRITE setClosed NOTIFY closedChanged)

        public:

            enum class Status
            {
                SUGGESTED,
                DISCARDED,
                OPENED,
                CLOSED,
                CANCELED,
                NONE
            }; Q_ENUM(Status)

            enum class Exchange
            {
                BITTREX,
                POLONIEX,
                BINANCE,
                KRAKEN,
                NONE
            }; Q_ENUM(Exchange)

            enum class Type
            {
                LIMIT_BUY,
                LIMIT_SELL,
                NONE
            }; Q_ENUM(Type)

        private:
            QString orderId;
            QString exchangeOrderId;
            QString portfolioId;
            trader::Order::Status status;
            trader::Order::Type type;
            trader::Order::Exchange exchange;
            QString market;
            double quantity;
            double quantityFilled;
            double price;
            double precision;
            double minimumTradeSize;
            QJsonObject data;
            QDateTime timeStamp;
            QDateTime opened;
            QDateTime closed;

        public:
            explicit Order(QObject *parent=nullptr);
            explicit Order(QString market,double quantity,double price,double precision,
                double minimumTradeSize,trader::Order::Type type,trader::Order::Exchange exchange,
                trader::Order::Status status=trader::Order::Status::SUGGESTED, QObject *parent=nullptr);
            ~Order();
            QString getOrderId() const;
            void setOrderId(const QString &value);
            QString getExchangeOrderId() const;
            void setExchangeOrderId(const QString &value);
            trader::Order::Type getType() const;
            void setType(const trader::Order::Type &value);
            trader::Order::Status getStatus() const;
            void setStatus(const trader::Order::Status &value);
            trader::Order::Exchange getExchange() const;
            void setExchange(const trader::Order::Exchange &value);
            QString getMarket() const;
            void setMarket(const QString &value);
            double getQuantity() const;
            void setQuantity(double value);
            double getQuantityRemaining() const;
            void setQuantityRemaining(double value);
            double getQuantityFilled() const;
            void setQuantityFilled(double value);
            double getPrice() const;
            void setPrice(double value);
            double getPrecision() const;
            void setPrecision(double value);
            double getMinimumTradeSize() const;
            void setMinimumTradeSize(double value);
            QJsonObject getData() const;
            void setData(const QJsonObject &value);
            QDateTime getTimeStamp() const;
            void setTimeStamp(const QDateTime &value);
            QDateTime getOpened() const;
            void setOpened(const QDateTime &value);
            QDateTime getClosed() const;
            void setClosed(const QDateTime &value);

        signals:
            void orderIdChanged(const QString &orderId);
            void exchangeOrderIdChanged(const QString &orderId);
            void statusChanged(const trader::Order::Status &status);
            void typeChanged(const trader::Order::Type &type);
            void exchangeChanged(const trader::Order::Exchange &exchange);
            void marketChanged(const QString &market);
            void quantityChanged(const double &quantity);
            void quantityRemainingChanged(const double &quantityRemaining);
            void quantityFilledChanged(const double &quantityFilled);
            void priceChanged(const double &price);
            void precisionChanged(const double &precision);
            void minimumTradeSizeChanged(const double &minimumTradeSize);
            void dataChanged(const QJsonObject &data);
            void timeStampChanged(const QDateTime &timeStamp);
            void openedChanged(const QDateTime &opened);
            void closedChanged(const QDateTime &closed);
    };
}

Q_DECLARE_METATYPE(trader::Order *)

#endif // ORDER_HPP
