#include "artificialintelligenceanalysis.hpp"

trader::ArtificialIntelligenceAnalysis::ArtificialIntelligenceAnalysis(QObject *parent): QObject(parent) {}

trader::ArtificialIntelligenceAnalysis::~ArtificialIntelligenceAnalysis() = default;

void trader::ArtificialIntelligenceAnalysis::clear() {}
