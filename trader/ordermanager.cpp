#include "ordermanager.hpp"


trader::OrderManager::OrderManager(QObject *parent): QObject(parent)
{
    this->setData(QVariantMap());
    this->setSuggestedOrders(new trader::OrderHash());
    this->setDiscardedOrders(new trader::OrderHash());
    this->setOpenOrders(new trader::OrderHash());
    this->setClosedOrders(new trader::OrderHash());
    this->setCanceledOrders(new trader::OrderHash());
    this->setSuggestedMarketOrders(new trader::OrderMarketHash());
    this->setOpenMarketOrders(new trader::OrderMarketHash());
}

trader::OrderManager::~OrderManager()
{
    this->clear();
    delete this->suggestedOrders;
    delete this->discardedOrders;
    delete this->openOrders;
    delete this->closedOrders;
    delete this->canceledOrders;
    delete this->suggestedMarketOrders;
    delete this->openMarketOrders;
}

QVariantMap trader::OrderManager::getData() const { return this->data; }

void trader::OrderManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::OrderHash *trader::OrderManager::getSuggestedOrders() const { return this->suggestedOrders; }

void trader::OrderManager::setSuggestedOrders(trader::OrderHash *value)
{
    this->suggestedOrders=value;
    emit this->suggestedOrdersChanged(this->suggestedOrders);
}

void trader::OrderManager::clearSuggestedOrders()
{
    trader::OrderHash::iterator iterator;
    this->getSuggestedMarketOrders()->clear();
    for (iterator=this->suggestedOrders->begin();iterator!=this->suggestedOrders->end();iterator++) { iterator.value()->deleteLater(); }
    this->getSuggestedOrders()->clear();
}

trader::OrderHash *trader::OrderManager::getOpenOrders() const { return this->openOrders; }

void trader::OrderManager::setOpenOrders(trader::OrderHash *value)
{
    this->openOrders=value;
    emit this->openOrdersChanged(this->openOrders);
}

void trader::OrderManager::clearOpenOrders()
{
    trader::OrderHash::iterator iterator;
    this->getOpenMarketOrders()->clear();
    for (iterator=this->openOrders->begin();iterator!=this->openOrders->end();iterator++) { iterator.value()->deleteLater(); }
    this->getOpenOrders()->clear();
}

trader::OrderHash *trader::OrderManager::getClosedOrders() const { return this->closedOrders; }

void trader::OrderManager::setClosedOrders(trader::OrderHash *value)
{
    this->closedOrders=value;
    emit this->suggestedOrdersChanged(this->closedOrders);
}

void trader::OrderManager::clearClosedOrders()
{
    trader::OrderHash::iterator iterator;
    for (iterator=this->closedOrders->begin();iterator!=this->closedOrders->end();iterator++) { iterator.value()->deleteLater(); }
    this->closedOrders->clear();
    emit this->closedOrdersChanged(this->closedOrders);
}

trader::OrderHash *trader::OrderManager::getCanceledOrders() const { return this->canceledOrders; }

void trader::OrderManager::setCanceledOrders(trader::OrderHash *value)
{
    this->canceledOrders=value;
    emit this->canceledOrdersChanged(this->canceledOrders);
}

void trader::OrderManager::clearCanceledOrders()
{
    trader::OrderHash::iterator iterator;
    for (iterator=this->canceledOrders->begin();iterator!=this->canceledOrders->end();iterator++) { iterator.value()->deleteLater(); }
    this->getCanceledOrders()->clear();
}


trader::OrderHash *trader::OrderManager::getDiscardedOrders() const { return this->discardedOrders; }

void trader::OrderManager::setDiscardedOrders(trader::OrderHash *value)
{
    this->discardedOrders=value;
    emit this->discardedOrdersChanged(this->discardedOrders);
}

void trader::OrderManager::clearDiscardedOrders()
{
    trader::OrderHash::iterator iterator;
    for (iterator=this->discardedOrders->begin();iterator!=this->discardedOrders->end();iterator++) { iterator.value()->deleteLater(); }
    this->getDiscardedOrders()->clear();
}

trader::OrderMarketHash *trader::OrderManager::getOpenMarketOrders() const { return this->openMarketOrders; }

void trader::OrderManager::setOpenMarketOrders(trader::OrderMarketHash *value)
{
    this->openMarketOrders=value;
    emit this->openMarketOrdersChanged(this->openMarketOrders);
}

trader::OrderMarketHash *trader::OrderManager::getSuggestedMarketOrders() const { return this->suggestedMarketOrders; }

void trader::OrderManager::setSuggestedMarketOrders(trader::OrderMarketHash *value)
{
    this->suggestedMarketOrders=value;
    emit this->suggestedMarketOrdersChanged(this->suggestedMarketOrders);
}


bool trader::OrderManager::suggest(const QString &orderId,const trader::Order *order)
{
    trader::Order *current_order=const_cast<trader::Order *>(static_cast<const trader::Order *>(order));
    bool contains_order=this->getSuggestedOrders()->contains(orderId);
    if (contains_order==true) { return false; }
    this->getSuggestedOrders()->insert(orderId,current_order);
    this->getSuggestedMarketOrders()->insert(order->getMarket(),current_order);
    current_order->setStatus(Order::Status::SUGGESTED);
    emit this->suggestedOrdersChanged(this->suggestedOrders);
    emit this->suggestedMarketOrdersChanged(this->suggestedMarketOrders);
    emit this->suggested(orderId,order);
    return true;
}

bool trader::OrderManager::discard(const QString &orderId,const trader::Order *order)
{
    trader::Order *current_order=const_cast<trader::Order *>(static_cast<const trader::Order *>(order));
    bool contains_order=this->getSuggestedOrders()->contains(orderId);
    if (contains_order==false) { return false; }
    this->getSuggestedOrders()->remove(orderId);
    this->getSuggestedMarketOrders()->remove(order->getMarket(),current_order);
    emit this->suggestedOrdersChanged(this->suggestedOrders);
    emit this->suggestedMarketOrdersChanged(this->suggestedMarketOrders);
    contains_order=this->getDiscardedOrders()->contains(orderId);
    if (contains_order==true) { return false; }
    this->getDiscardedOrders()->insert(orderId,current_order);
    current_order->setStatus(Order::Status::DISCARDED);
    emit this->discardedOrdersChanged(this->discardedOrders);
    emit this->discarded(orderId,order);
    return true;
}


bool trader::OrderManager::open(const QString &orderId,const trader::Order *order)
{
    trader::Order *current_order=const_cast<trader::Order *>(static_cast<const trader::Order *>(order));
    bool contains_order=this->getSuggestedOrders()->contains(orderId);
    if (contains_order==false) { return false; }
    this->getSuggestedOrders()->remove(orderId);
    this->getSuggestedMarketOrders()->remove(order->getMarket(),current_order);
    emit this->suggestedOrdersChanged(this->suggestedOrders);
    emit this->suggestedMarketOrdersChanged(this->suggestedMarketOrders);
    contains_order=this->getOpenOrders()->contains(orderId);
    if (contains_order==true) { return false; }
    this->getOpenOrders()->insert(orderId,current_order);
    this->getOpenMarketOrders()->insert(order->getMarket(),current_order);
    current_order->setStatus(Order::Status::OPENED);
    emit this->openOrdersChanged(this->openOrders);
    emit this->openMarketOrdersChanged(this->openMarketOrders);
    emit this->opened(orderId,order);
    return true;
}

bool trader::OrderManager::update(const QString &orderId,const trader::Order *order)
{
    emit this->updated(orderId,order);
    return true;
}

bool trader::OrderManager::close(const QString &orderId,const trader::Order *order)
{
    trader::Order *current_order=const_cast<trader::Order *>(static_cast<const trader::Order *>(order));
    bool contains_order=this->getOpenOrders()->contains(orderId);
    if (contains_order==false) { return false; }
    this->getOpenOrders()->remove(orderId);
    this->getOpenMarketOrders()->remove(order->getMarket(),current_order);
    emit this->openOrdersChanged(this->openOrders);
    emit this->openMarketOrdersChanged(this->openMarketOrders);
    contains_order=this->getClosedOrders()->contains(orderId);
    if (contains_order==true) { return  false; }
    this->getClosedOrders()->insert(orderId,current_order);
    current_order->setStatus(Order::Status::CLOSED);
    emit this->closedOrdersChanged(this->closedOrders);
    emit this->closed(orderId,order);
    return true;
}


bool trader::OrderManager::cancel(const QString &orderId,const trader::Order *order)
{
    trader::Order *current_order=const_cast<trader::Order *>(static_cast<const trader::Order *>(order));
    bool contains_order=this->getOpenOrders()->contains(orderId);
    if (contains_order==false) { return false; }
    this->getOpenOrders()->remove(orderId);
    this->getOpenMarketOrders()->remove(order->getMarket(),current_order);
    emit this->openOrdersChanged(this->openOrders);
    emit this->openMarketOrdersChanged(this->openMarketOrders);
    contains_order=this->getClosedOrders()->contains(orderId);
    if (contains_order==true) { return false; }
    this->getCanceledOrders()->insert(orderId,current_order);
    current_order->setStatus(Order::Status::CANCELED);
    emit this->canceledOrdersChanged(this->canceledOrders);
    emit this->canceled(orderId,order);
    return true;
}


bool trader::OrderManager::containsSuggestedMarketOrder(QString market)
{
    bool contains_suggested_market_order=this->suggestedMarketOrders->contains(market);
    return contains_suggested_market_order;
}

bool trader::OrderManager::containsOpenMarketOrder(QString market)
{
    bool contains_open_market_order=this->openMarketOrders->contains(market);
    return contains_open_market_order;
}

bool trader::OrderManager::removeSuggestedMarketOrder(QString market)
{
    QList<trader::Order *>::iterator iterator;
    bool contains_market=this->containsSuggestedMarketOrder(market);
    if (contains_market==false) { return false; }
    QList<trader::Order *> orders=this->getSuggestedMarketOrders()->values(market);

    for (iterator=orders.begin();iterator!=orders.end();iterator++)
    {
        trader::Order *current_order=iterator.i->t();
        this->discard(current_order->getOrderId(),current_order);
    }

    this->getSuggestedMarketOrders()->remove(market);
    this->clearDiscardedOrders();
    return true;
}

bool trader::OrderManager::removeOpenMarketOrder(QString market)
{
    QList<trader::Order *>::Iterator iterator;
    bool contains_market=this->containsOpenMarketOrder(market);
    if (contains_market==false) { return false; }
    QList<trader::Order *> orders=this->getOpenMarketOrders()->values(market);

    for (iterator=orders.begin();iterator!=orders.end();iterator++)
    {
        trader::Order *current_order=iterator.i->t();
        this->cancel(current_order->getOrderId(),current_order);
    }

    this->getOpenMarketOrders()->remove(market);
    this->clearCanceledOrders();
    return true;
}


void trader::OrderManager::clear()
{
    this->setData(QVariantMap());
    this->clearSuggestedOrders();
    this->clearDiscardedOrders();
    this->clearOpenOrders();
    this->clearClosedOrders();
    this->clearCanceledOrders();
}
