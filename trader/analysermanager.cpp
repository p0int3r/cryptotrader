#include "analysermanager.hpp"



trader::AnalyserManager::AnalyserManager(QObject *parent): QObject(parent)
{
    this->setData(QVariantMap());
    this->setAnalysers(new trader::AnalyserHash());
}

trader::AnalyserManager::~AnalyserManager()
{
    this->clear();
    delete this->analysers;
}


QVariantMap trader::AnalyserManager::getData() const { return this->data; }

void trader::AnalyserManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::AnalyserHash *trader::AnalyserManager::getAnalysers() const { return this->analysers; }

void trader::AnalyserManager::setAnalysers(trader::AnalyserHash *value)
{
    this->analysers=value;
    emit this->analysersChanged(this->analysers);
}

void trader::AnalyserManager::clearAnalysers()
{
    trader::AnalyserHash::iterator iter;
    for (iter=this->getAnalysers()->begin();iter!=this->getAnalysers()->end();iter++) { iter.value()->deleteLater(); }
    this->getAnalysers()->clear();
}


bool trader::AnalyserManager::addAnalyser(QString market,trader::Analyser *analyser)
{
    bool containsAnalyser=this->getAnalysers()->contains(market);
    if (containsAnalyser==true) { return false;}
    if (analyser==nullptr) { return false; }
    this->getAnalysers()->insert(market,analyser);
    emit this->analyserAdded(market,analyser);
    return true;
}

bool trader::AnalyserManager::removeAnalyser(QString market)
{
    bool containsPortfolio=this->getAnalysers()->contains(market);
    if (containsPortfolio==false) { return false; }
    trader::Analyser *analyser=this->getAnalysers()->value(market);
    this->getAnalysers()->remove(market);
    emit this->analyserRemoved(market,analyser);
    analyser->deleteLater();
    return true;
}


trader::Analyser *trader::AnalyserManager::getAnalyser(QString market)
{
    bool containsAnalyser=this->getAnalysers()->contains(market);
    if (containsAnalyser==false) { return nullptr; }
    trader::Analyser *analyser=this->getAnalysers()->value(market);
    return analyser;
}

void trader::AnalyserManager::clear()
{
    this->setData(QVariantMap());
    this->clearAnalysers();
}
