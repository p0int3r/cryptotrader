#include "sizermanager.hpp"

trader::SizerManager::SizerManager(QObject *parent): QObject(parent)
{
    this->setSizers(new trader::SizerHash());
    this->setData(QVariantMap());
}

trader::SizerManager::~SizerManager()
{
    this->clear();
    delete this->sizers;
}

QVariantMap trader::SizerManager::getData() const { return this->data; }

void trader::SizerManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::SizerHash *trader::SizerManager::getSizers() const { return this->sizers; }

void trader::SizerManager::setSizers(trader::SizerHash *value)
{
    this->sizers=value;
    emit this->sizersChanged(this->sizers);
}

void trader::SizerManager::clearSizers()
{
    trader::SizerHash::iterator iterator;
    for (iterator=this->getSizers()->begin();iterator!=this->getSizers()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getSizers()->clear();
}

bool trader::SizerManager::addSizer(QString name,trader::Sizer *sizer)
{
    bool contains_sizer=this->getSizers()->contains(name);
    if (contains_sizer==true) { return false; }
    if (sizer==nullptr) { return false; }
    this->getSizers()->insert(name,sizer);
    emit this->sizerAdded(name,sizer);
    return true;
}

bool trader::SizerManager::removeSizer(QString name)
{
    bool contains_sizer=this->getSizers()->contains(name);
    if (contains_sizer==false) { return false; }
    trader::Sizer *sizer=this->getSizers()->value(name);
    this->getSizers()->remove(name);
    emit this->sizerRemoved(name,sizer);
    sizer->deleteLater();
    return true;
}

trader::Sizer *trader::SizerManager::getSizer(QString name)
{
    bool contains_sizer=this->getSizers()->contains(name);
    if (contains_sizer==false) { return nullptr; }
    trader::Sizer *sizer=this->getSizers()->value(name);
    return sizer;
}

void trader::SizerManager::clear()
{
    this->setData(QVariantMap());
    this->clearSizers();
}
