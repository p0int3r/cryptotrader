#ifndef TRADER_TRAITS_HPP
#define TRADER_TRAITS_HPP

#include <concepts>
#include <iterator>
#include <type_traits>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/accumulators/statistics/rolling_variance.hpp>
#include <boost/accumulators/statistics/rolling_moment.hpp>
#include <boost/accumulators/statistics/rolling_max.hpp>
#include <boost/accumulators/statistics/rolling_min.hpp>

namespace trader
{
    namespace traits
    {
        using namespace boost::accumulators;

        template<typename T> concept SafeNumerical = std::is_arithmetic<T>::value && std::is_trivially_copyable<T>::value;

        template<typename T> concept SafeContainer = requires(T a,const T b)
        {
            requires std::regular<T>;
            requires std::swappable<T>;
            requires std::destructible<typename T::value_type>;
            requires std::same_as<typename T::reference,typename T::value_type &>;
            requires std::same_as<typename T::const_reference,const typename T::value_type &>;
            requires std::forward_iterator<typename T::iterator>;
            requires std::forward_iterator<typename T::const_iterator>;
            requires std::signed_integral<typename T::difference_type>;
            requires std::same_as<typename T::difference_type,typename std::iterator_traits<typename T::iterator>::difference_type>;
            requires std::same_as<typename T::difference_type,typename std::iterator_traits<typename T::const_iterator>::difference_type>;
        };

        template<typename T>
        using Statistics = stats<
            tag::count,
            tag::mean,
            tag::sum,
            tag::variance,
            tag::max,
            tag::min
        >;

        template<typename T>
        using RollingStatistics = stats<
            tag::rolling_count,
            tag::rolling_mean,
            tag::rolling_sum,
            tag::rolling_variance,
            tag::rolling_min,
            tag::rolling_max
        >;

        template<typename T>
        using Accumulator = accumulator_set<T,Statistics<T>>;

        template<typename T>
        using RollingAccumulator = accumulator_set<T,RollingStatistics<T>>;
    }
}

#endif // TRADER_TRAITS_HPP
