#include "analyser.hpp"


trader::Analyser::Analyser(QObject *parent): QObject(parent)
{
    this->setSentimentAnalysis(new trader::SentimentAnalysis(parent));
    this->setMacroEconomicAnalysis(new trader::MacroEconomicAnalysis(parent));
    this->setMicroEconomicAnalysis(new trader::MicroEconomicAnalysis(parent));
    this->setArtificialIntelligenceAnalysis(new trader::ArtificialIntelligenceAnalysis(parent));
}

trader::Analyser::~Analyser()
{
    delete this->sentimentAnalysis;
    delete this->macroEconomicAnalysis;
    delete this->microEconomicAnalysis;
    delete this->artificialIntelligenceAnalysis;
}

trader::SentimentAnalysis *trader::Analyser::getSentimentAnalysis() const { return this->sentimentAnalysis; }

void trader::Analyser::setSentimentAnalysis(trader::SentimentAnalysis *value)
{
    this->sentimentAnalysis=value;
    emit this->sentimentAnalysisChanged(this->sentimentAnalysis);
}

trader::MacroEconomicAnalysis *trader::Analyser::getMacroEconomicAnalysis() const { return this->macroEconomicAnalysis; }

void trader::Analyser::setMacroEconomicAnalysis(trader::MacroEconomicAnalysis *value)
{
    this->macroEconomicAnalysis=value;
    emit this->macroEconomicAnalysisChanged(this->macroEconomicAnalysis);
}

trader::MicroEconomicAnalysis *trader::Analyser::getMicroEconomicAnalysis() const { return this->microEconomicAnalysis; }

void trader::Analyser::setMicroEconomicAnalysis(trader::MicroEconomicAnalysis *value)
{
    this->microEconomicAnalysis=value;
    emit this->microEconomicAnalysisChanged(this->microEconomicAnalysis);
}

trader::ArtificialIntelligenceAnalysis *trader::Analyser::getArtificialIntelligenceAnalysis() const { return this->artificialIntelligenceAnalysis; }

void trader::Analyser::setArtificialIntelligenceAnalysis(trader::ArtificialIntelligenceAnalysis *value)
{
    this->artificialIntelligenceAnalysis=value;
    emit this->artificialIntelligenceAnalysisChanged(this->artificialIntelligenceAnalysis);
}


trader::data_structures::TimeSeries<std::deque<double>> &trader::Analyser::getTimeSeries() { return this->timeSeries; }

void trader::Analyser::setTimeSeries(const trader::data_structures::TimeSeries<std::deque<double>> &value)
{
    this->timeSeries=value;
    emit this->timeSeriesChanged(this->timeSeries);
}

void trader::Analyser::clear()
{
    this->getSentimentAnalysis()->clear();
    this->getMacroEconomicAnalysis()->clear();
    this->getMicroEconomicAnalysis()->clear();
    this->getArtificialIntelligenceAnalysis()->clear();
}
