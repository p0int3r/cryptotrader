 #include "trader.hpp"



trader::Trader::Trader(QObject *parent): QObject(parent)
{
    this->setAssetManager(new trader::AssetManager(parent));
    this->setAnalyserManager(new trader::AnalyserManager(parent));
    this->setPortfolioManager(new trader::PortfolioManager(parent));
    this->setStrategyManager(new trader::StrategyManager(parent));
    this->setRiskManager(new trader::RiskManager(parent));
    this->setOrderManager(new trader::OrderManager(parent));
    this->setExecutionManager(new trader::ExecutionManager(parent));
    this->setFiltererManager(new trader::FiltererManager(parent));
    this->setSizerManager(new trader::SizerManager(parent));
}

trader::Trader::~Trader()
{
    delete this->assetManager;
    delete this->analyserManager;
    delete this->portfolioManager;
    delete this->strategyManager;
    delete this->riskManager;
    delete this->orderManager;
    delete this->executionManager;
    delete this->filtererManager;
    delete this->sizerManager;
}


trader::AnalyserManager *trader::Trader::getAnalyserManager() const { return this->analyserManager; }

void trader::Trader::setAnalyserManager(trader::AnalyserManager *value)
{
    this->analyserManager=value;
    emit this->analyserManagerChanged(this->analyserManager);
}

trader::PortfolioManager *trader::Trader::getPortfolioManager() const { return this->portfolioManager; }

void trader::Trader::setPortfolioManager(trader::PortfolioManager *value)
{
    this->portfolioManager=value;
    emit this->portfolioManagerChanged(this->portfolioManager);
}

trader::StrategyManager *trader::Trader::getStrategyManager() const { return this->strategyManager; }

void trader::Trader::setStrategyManager(trader::StrategyManager *value)
{
    this->strategyManager=value;
    emit this->strategyManagerChanged(this->strategyManager);
}

trader::RiskManager *trader::Trader::getRiskManager() const { return this->riskManager; }

void trader::Trader::setRiskManager(trader::RiskManager *value)
{
    this->riskManager=value;
    emit this->riskManagerChanged(this->riskManager);
}

trader::OrderManager *trader::Trader::getOrderManager() const { return this->orderManager; }

void trader::Trader::setOrderManager(trader::OrderManager *value)
{
    this->orderManager=value;
    emit this->orderManagerChanged(this->orderManager);
}

trader::ExecutionManager *trader::Trader::getExecutionManager() const { return this->executionManager; }

void trader::Trader::setExecutionManager(trader::ExecutionManager *value)
{
    this->executionManager=value;
    emit this->executionManagerChanged(this->executionManager);
}

trader::AssetManager *trader::Trader::getAssetManager() const { return this->assetManager; }

void trader::Trader::setAssetManager(trader::AssetManager *value)
{
    this->assetManager=value;
    emit this->assetManagerChanged(this->assetManager);
}

trader::FiltererManager *trader::Trader::getFiltererManager() const { return this->filtererManager; }

void trader::Trader::setFiltererManager(trader::FiltererManager *value)
{
    this->filtererManager=value;
    emit this->filtererManagerChanged(this->filtererManager);
}

trader::SizerManager *trader::Trader::getSizerManager() const { return this->sizerManager; }

void trader::Trader::setSizerManager(trader::SizerManager *value)
{
    this->sizerManager=value;
    emit this->sizerManagerChanged(this->sizerManager);
}
