#ifndef TIMESERIES_HPP
#define TIMESERIES_HPP

#include "trader_traits.hpp"

namespace trader
{
    namespace data_structures
    {
        using namespace trader::traits;

        template<trader::traits::SafeContainer Container>
        class TimeSeries
        {
            using ContainerType=Container;
            using ElementType=typename std::remove_reference<decltype(std::declval<ContainerType>().front())>::type;
            using AllocType=decltype(std::declval<Container>().get_allocator());

            private:
                long long int count;
                long long int rollingCount;
                ContainerType data;
                ElementType mean;
                ElementType variance;
                ElementType sum;
                ElementType min;
                ElementType max;
                ElementType rollingMean;
                ElementType rollingVariance;
                ElementType rollingSum;
                ElementType rollingMax;
                ElementType rollingMin;
                Accumulator<ElementType> accumulator=Accumulator<ElementType>();
                RollingAccumulator<ElementType> rollingAccumulator=RollingAccumulator<ElementType>(tag::rolling_window::window_size=500);

            public:
                TimeSeries(int window_size=500)
                {
                    this->reset();
                    this->setRollingWindow(window_size);
                }

                template<typename Iterator>
                TimeSeries(Iterator first,Iterator last,int window_size=500)
                {
                    this->reset();
                    this->setRollingWindow(window_size);
                    std::copy(first,last,std::back_inserter(this->data));
                    std::for_each(first,last,[=](const ElementType &element) { this->update(element); });

                }

                template<typename Iterator>
                TimeSeries(const Iterator &first,const Iterator &last,int window_size=500)
                {
                    this->reset();
                    this->setRollingWindow(window_size);
                    std::copy(first,last,std::back_inserter(this->data));
                    std::for_each(first,last,[=](const ElementType &element) { this->update(element); });
                }

                long long int &getCount() { return this->count; }
                void setCount(const long long int &value) { this->count=value; }
                long long int &getRollingCount() { return this->rollingCount; }
                void setRollingCount(const long long int &value) { this->rollingCount=value; }
                Container &getData() { return this->data; }
                void setData(const Container &value) { this->data=value; }
                ElementType &getMean() { return this->mean; }
                void setMean(const ElementType &value) { this->mean=value; }
                ElementType &getVariance() { return this->variance; }
                void setVariance(const ElementType &value) { this->variance=value; }
                ElementType &getSum() { return this->sum; }
                void setSum(const ElementType &value) { this->sum=value; }
                ElementType &getMin() { return this->min; }
                void setMin(const ElementType &value) { this->min-value; }
                ElementType &getMax() { return this->max; }
                void setMax(const ElementType &value) { this->max=value; }
                ElementType &getRollingMean() { return this->rollingMean; }
                void setRollingMean(const ElementType &value) { this->rollingMean=value; }
                ElementType &getRollingVariance() { return this->rollingVariance; }
                void setRollingVariance(const ElementType &value) { this->rollingVariance=value; }
                ElementType &getRollingSum() { return this->rollingSum; }
                void setRollingSum(const ElementType &value) { this->rollingSum=value; }
                ElementType &getRollingMax() { return this->rollingMax; }
                void setRollingMax(const ElementType &value) { this->rollingMax=value; }
                ElementType &getRollingMin() { return this->rollingMin; }
                void setRollingMin(const ElementType &value) { this->rollingMin=value; }
                void setRollingWindow(int windowSize) { this->rollingAccumulator=RollingAccumulator<ElementType>(tag::rolling_window::window_size=windowSize); }

                void reset()
                {
                    this->count=0;
                    this->rollingCount=0;
                    this->data=ContainerType();
                    this->mean=ElementType();
                    this->variance=ElementType();
                    this->sum=ElementType();
                    this->min=ElementType();
                    this->max=ElementType();
                    this->rollingMean=ElementType();
                    this->rollingVariance=ElementType();
                    this->rollingSum=ElementType();
                    this->rollingMax=ElementType();
                    this->rollingMin=ElementType();
                    this->accumulator=Accumulator<ElementType>();
                    this->rollingAccumulator=RollingAccumulator<ElementType>(tag::rolling_window::window_size=500);
                }

                void place(const ElementType &value)
                {
                    this->data.insert(data.end(),value);
                    this->update(value);
                }

                void update(const ElementType &value)
                {
                    this->accumulator(value);
                    this->rollingAccumulator(value);

                    this->count=trader::traits::count(this->accumulator);
                    this->mean=trader::traits::mean(this->accumulator);
                    this->variance=trader::traits::variance(this->accumulator);
                    this->sum=trader::traits::sum(this->accumulator);
                    this->min=trader::traits::min(this->accumulator);
                    this->max=trader::traits::max(this->accumulator);

                    this->rollingCount=trader::traits::rolling_count(this->rollingAccumulator);
                    this->rollingMean=trader::traits::rolling_mean(this->rollingAccumulator);
                    this->rollingVariance=trader::traits::rolling_variance(this->rollingAccumulator);
                    this->rollingSum=trader::traits::rolling_sum(this->rollingAccumulator);
                    this->rollingMax=trader::traits::rolling_min(this->rollingAccumulator);
                    this->rollingMin=trader::traits::rolling_max(this->rollingAccumulator);
                }
        };
    }
}


#endif // TIMESERIES_HPP
