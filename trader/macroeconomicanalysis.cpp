#include "macroeconomicanalysis.hpp"

trader::MacroEconomicAnalysis::MacroEconomicAnalysis(QObject *parent): QObject(parent) {}

trader::MacroEconomicAnalysis::~MacroEconomicAnalysis() = default;

void trader::MacroEconomicAnalysis::clear() {}
