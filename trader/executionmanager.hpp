#ifndef EXECUTIONMANAGER_HPP
#define EXECUTIONMANAGER_HPP

#include <QObject>
#include <QString>
#include <QVariantMap>
#include <qqml.h>
#include "execution.hpp"
#include "order.hpp"

namespace trader { typedef QHash<QString,trader::Execution *> ExecutionHash; }
Q_DECLARE_METATYPE(trader::ExecutionHash *)

namespace trader
{
    class ExecutionManager: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QVariantMap data READ getData WRITE setData NOTIFY dataChanged)
        Q_PROPERTY(trader::ExecutionHash *executions READ getExecutions WRITE setExecutions NOTIFY executionsChanged)

        private:
            QVariantMap data;
            trader::ExecutionHash *executions=nullptr;

        public:
            explicit ExecutionManager(QObject *parent=nullptr);
            ~ExecutionManager();
            QVariantMap getData() const;
            void setData(const QVariantMap &value);
            trader::ExecutionHash *getExecutions() const;
            void setExecutions(trader::ExecutionHash *value);
            void clearExecutions();
            void clear();

        signals:
            void dataChanged(const QVariantMap &data);
            void executionsChanged(const trader::ExecutionHash *executions);
            void executionAdded(const QString &orderId,const trader::Execution *execution);
            void executionRemoved(const QString &orderId,const trader::Execution *execution);

            void opened(const QString &orderId,const trader::Order *order);
            void updated(const QString &orderId,const trader::Order *order);
            void closed(const QString &orderId,const trader::Order *order);
            void canceled(const QString &orderId,const trader::Order *order);
            void stopped(const QString &orderId,const trader::Order *order);

        public slots:
            bool addExecution(QString orderId,trader::Execution *execution);
            bool removeExecution(QString orderId);
            trader::Execution *getExecution(QString orderId);
    };
}

Q_DECLARE_METATYPE(trader::ExecutionManager *)

#endif // EXECUTIONMANAGER_HPP
