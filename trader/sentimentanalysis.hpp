#ifndef SENTIMENTANALYSIS_HPP
#define SENTIMENTANALYSIS_HPP

#include <QObject>
#include <qqml.h>

// TODO: implement when needed.

namespace trader
{
    class SentimentAnalysis: public QObject
    {
        Q_OBJECT
        QML_ELEMENT

        public:
            explicit SentimentAnalysis(QObject *parent=nullptr);
            ~SentimentAnalysis();
            void clear();

        signals:

        public slots:
    };
}

Q_DECLARE_METATYPE(trader::SentimentAnalysis *)

#endif // SENTIMENTANALYSIS_HPP
