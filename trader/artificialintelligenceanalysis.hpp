#ifndef ARTIFICIALINTELLIGENCEANALYSIS_HPP
#define ARTIFICIALINTELLIGENCEANALYSIS_HPP

#include <QObject>
#include <qqml.h>

// TODO: implement when needed.

namespace trader
{
    class ArtificialIntelligenceAnalysis: public QObject
    {
        Q_OBJECT
        QML_ELEMENT

        private:

        public:
            ArtificialIntelligenceAnalysis(QObject *parent=nullptr);
            ~ArtificialIntelligenceAnalysis();
            void clear();

        signals:

        public slots:
    };
}

Q_DECLARE_METATYPE(trader::ArtificialIntelligenceAnalysis *)

#endif // ARTIFICIALINTELLIGENCEANALYSIS_HPP
