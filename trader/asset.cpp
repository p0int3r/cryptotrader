#include "asset.hpp"

trader::Asset::Asset(QObject *parent): QObject(parent)
{
    this->setMarket(QString());
    this->setAllocatedCapital(0.0);
    this->setCapitalPercentage(0.0);
    this->setTimer(new QTimer());
    this->setCurrentRound(0);
    this->setTradingRounds(0);
    this->connect(this->timer,&QTimer::timeout,this,&trader::Asset::update);
    this->connect(this,&trader::Asset::started,this,[=](){ this->getTimer()->start(this->getFrequencyMilliseconds()); });
}

trader::Asset::~Asset() { delete this->timer; }

double trader::Asset::getAllocatedCapital() const { return this->allocatedCapital; }

void trader::Asset::setAllocatedCapital(double value)
{
    this->allocatedCapital=value;
    emit this->allocatedCapitalChanged(this->allocatedCapital);
}

QString trader::Asset::getMarket() const { return this->market; }

void trader::Asset::setMarket(const QString &value)
{
    this->market=value;
    emit this->marketChanged(this->market);
}

double trader::Asset::getCapitalPercentage() const { return this->capitalPercentage; }

void trader::Asset::setCapitalPercentage(double value)
{
    this->capitalPercentage=value;
    emit this->capitalPercentageChanged(this->capitalPercentage);
}

int trader::Asset::getTradingRounds() const { return this->tradingRounds; }

void trader::Asset::setTradingRounds(int value)
{
    this->tradingRounds=value;
    emit this->tradingRoundsChanged(this->tradingRounds);
}

int trader::Asset::getCurrentRound() const { return this->currentRound; }

void trader::Asset::setCurrentRound(int value)
{
    this->currentRound=value;
    emit this->currentRoundChanged(this->currentRound);
}

QTimer *trader::Asset::getTimer() const { return this->timer; }

void trader::Asset::setTimer(QTimer *value)
{
    this->timer=value;
    emit this->timerChanged(this->timer);
}


int trader::Asset::getFrequencyMilliseconds() const { return this->frequencyMilliseconds; }

void trader::Asset::setFrequencyMilliseconds(int value)
{
    this->frequencyMilliseconds=value;
    emit this->frequencyMillisecondsChanged(this->frequencyMilliseconds);
}
