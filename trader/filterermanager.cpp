#include "filterermanager.hpp"


trader::FiltererManager::FiltererManager(QObject *parent): QObject(parent)
{
    this->setFilterers(new trader::FiltererHash());
    this->setSortedFilterers(new trader::SortedFiltererList());
    this->setData(QVariantMap());
}

trader::FiltererManager::~FiltererManager()
{
    this->clear();
    delete this->filterers;
    delete this->sortedFilterers;
}


QVariantMap trader::FiltererManager::getData() const { return this->data; }

void trader::FiltererManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::FiltererHash *trader::FiltererManager::getFilterers() const { return this->filterers; }

void trader::FiltererManager::setFilterers(trader::FiltererHash *value)
{
    this->filterers=value;
    emit this->filterersChanged(this->filterers);
}

void trader::FiltererManager::clearFilterers()
{
    trader::FiltererHash::iterator iterator;
    for (iterator=this->getFilterers()->begin();iterator!=this->getFilterers()->end();iterator++) { iterator.value()->deleteLater(); }
    this->getFilterers()->clear();
    this->getSortedFilterers()->clear();
}

trader::SortedFiltererList *trader::FiltererManager::getSortedFilterers() const { return this->sortedFilterers; }

void trader::FiltererManager::setSortedFilterers(trader::SortedFiltererList *value)
{
    this->sortedFilterers=value;
    emit this->sortedFilterersChanged(this->sortedFilterers);
}

bool trader::FiltererManager::addFilterer(QString name,trader::Filterer *filterer)
{
    bool contains_filterer=this->getFilterers()->contains(name);
    if (contains_filterer==true) { return false; }
    if (filterer==nullptr) { return false; }
    this->getFilterers()->insert(name,filterer);
    emit this->filtererAdded(name,filterer);
    return true;
}


bool trader::FiltererManager::removeFilterer(QString name)
{
    bool contains_filterer=this->getFilterers()->contains(name);
    if (contains_filterer==false) { return false; }
    trader::Filterer *filterer=this->getFilterers()->value(name);
    this->getFilterers()->remove(name);
    emit this->filtererRemoved(name,filterer);
    filterer->deleteLater();
    return true;
}


trader::Filterer *trader::FiltererManager::getFilterer(QString name)
{
    bool contains_filterer=this->getFilterers()->contains(name);
    if (contains_filterer==false) { return nullptr; }
    trader::Filterer *filterer=this->getFilterers()->value(name);
    return filterer;
}

void trader::FiltererManager::sortByPrecedenceOrder()
{
    std::function<bool(trader::Filterer *,trader::Filterer *)> sortFn;
    sortFn=[=](trader::Filterer *f1,trader::Filterer *f2) -> bool
    {
        int f1_precedence_order=f1->getPrecedenceOrder();
        int f2_precendence_order=f2->getPrecedenceOrder();
        bool result=f1_precedence_order<f2_precendence_order;
        return result;
    };
    std::sort(this->getSortedFilterers()->begin(),this->getSortedFilterers()->end(),sortFn);
}

void trader::FiltererManager::clear()
{
    this->setData(QVariantMap());
    this->clearFilterers();
    this->getSortedFilterers()->clear();
}

