#ifndef CANDLESTICK_HPP
#define CANDLESTICK_HPP


#include "trader_traits.hpp"


namespace trader
{
    namespace data_structures
    {
        template<trader::traits::SafeNumerical T>
        class CandleStick
        {
            private:
                T open;
                T close;
                T high;
                T low;

            public:
                CandleStick() {}

                CandleStick(const T &open,const T &close,const T &high,const T &low)
                {
                    this->open=open;
                    this->close=close;
                    this->high=high;
                    this->low=low;
                }

                CandleStick(const CandleStick &rhs)
                {
                    this->open=rhs.open;
                    this->close=rhs.close;
                    this->high=rhs.high;
                    this->low=rhs.low;
                }

                CandleStick operator+(const CandleStick &rhs)
                {
                    CandleStick candle_stick;
                    candle_stick.open=this->open+rhs.open;
                    candle_stick.close=this->close+rhs.close;
                    candle_stick.high=this->high+rhs.high;
                    candle_stick.low=this->low+rhs.low;
                    return candle_stick;
                }

                CandleStick operator-(const CandleStick &rhs)
                {
                    CandleStick candle_stick;
                    candle_stick.open=this->open-rhs.open;
                    candle_stick.close=this->close-rhs.close;
                    candle_stick.high=this->high-rhs.high;
                    candle_stick.low=this->low-rhs.low;
                    return candle_stick;
                }

                CandleStick operator/(const CandleStick &rhs)
                {
                    CandleStick candle_stick;
                    candle_stick.open=this->open/rhs.open;
                    candle_stick.close=this->close/rhs.close;
                    candle_stick.high=this->high/rhs.high;
                    candle_stick.low=this->low/rhs.low;
                    return candle_stick;
                }

                CandleStick operator*(const CandleStick &rhs)
                {
                    CandleStick candle_stick;
                    candle_stick.open=this->open*rhs.open;
                    candle_stick.close=this->close*rhs.close;
                    candle_stick.high=this->high*rhs.high;
                    candle_stick.low=this->low*rhs.low;
                    return candle_stick;
                }

                T &getOpen() { return this->open; }
                void setOpen(const T &value) { this->open=value; }
                T &getClose() { return this->close; }
                void setClose(const T &value) { this->close=value; }
                T &getHigh() { return this->high; }
                void setHigh(const T &value) { this->high=value; }
                T &getLow() { return this->low; }
                void setLow(const T &value) { this->low=value; }
        };
    }
}

#endif // CANDLESTICK_HPP
