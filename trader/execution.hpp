#ifndef EXECUTION_HPP
#define EXECUTION_HPP

#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <order.hpp>
#include <qqml.h>

namespace trader
{
    class Execution: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QTimer *timer READ getTimer WRITE setTimer NOTIFY timerChanged)
        Q_PROPERTY(trader::Order *order READ getOrder WRITE setOrder NOTIFY orderChanged)
        Q_PROPERTY(QElapsedTimer *elapsedTimer READ getElapsedTimer WRITE setElapsedTimer NOTIFY elapsedTimerChanged)
        Q_PROPERTY(int cancelAfterMilliseconds READ getCancelAfterMilliseconds WRITE setCancelAfterMilliseconds NOTIFY cancelAfterMillisecondsChanged)
        Q_PROPERTY(int frequencyMilliseconds READ getFrequencyMilliseconds WRITE setFrequencyMilliseconds NOTIFY frequencyMillisecondsChanged)

        private:
            QTimer *timer=nullptr;
            trader::Order *order=nullptr;
            QElapsedTimer *elapsedTimer=nullptr;
            int cancelAfterMilliseconds;
            int frequencyMilliseconds;

        public:
            explicit Execution(QObject *parent=nullptr);
            virtual ~Execution();
            QTimer *getTimer() const;
            void setTimer(QTimer *value);
            trader::Order *getOrder() const;
            void setOrder(trader::Order *value);
            QElapsedTimer *getElapsedTimer() const;
            void setElapsedTimer(QElapsedTimer *value);
            int getCancelAfterMilliseconds() const;
            void setCancelAfterMilliseconds(int value);
            int getFrequencyMilliseconds() const;
            void setFrequencyMilliseconds(int value);

        signals:
            void timerChanged(const QTimer *timer);
            void orderChanged(const trader::Order *order);
            void elapsedTimerChanged(const QElapsedTimer *elapsedTimer);
            void cancelAfterMillisecondsChanged(const int &cancelAfterMilliseconds);
            void frequencyMillisecondsChanged(const int &frequencyMilliseconds);
            void placed();
            void opened();
            void closed();
            void updated();
            void canceled();
            void stopped();

        public slots:
            virtual void place() = 0;
            virtual void update() = 0;
            virtual void cancel() = 0;
            virtual void stop() = 0;
    };
}

Q_DECLARE_METATYPE(trader::Execution *)

#endif // EXECUTION_HPP
