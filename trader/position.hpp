#ifndef POSITION_HPP
#define POSITION_HPP

#include <QObject>
#include <QString>
#include <qqml.h>

namespace trader
{
    class Position: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString market READ getMarket WRITE setMarket NOTIFY marketChanged)
        Q_PROPERTY(trader::Position::Action action READ getAction WRITE setAction NOTIFY actionChanged)
        Q_PROPERTY(double quantity READ getQuantity WRITE setQuantity NOTIFY quantityChanged)
        Q_PROPERTY(double price READ getPrice WRITE setPrice NOTIFY priceChanged)
        Q_PROPERTY(double middlePrice READ getMiddlePrice WRITE setMiddlePrice NOTIFY middlePriceChanged)
        Q_PROPERTY(double ask READ getAsk WRITE setAsk NOTIFY askChanged)
        Q_PROPERTY(double bid READ getBid WRITE setBid NOTIFY bidChanged)
        Q_PROPERTY(double commission READ getCommission WRITE setCommission NOTIFY commissionChanged)
        Q_PROPERTY(double realisedPnL READ getRealisedPnL WRITE setRealisedPnL NOTIFY realisedPnLChanged)
        Q_PROPERTY(double unrealisedPnL READ getUnrealisedPnL WRITE setUnrealisedPnL NOTIFY unrealisedPnLChanged)
        Q_PROPERTY(double buys READ getBuys WRITE setBuys NOTIFY buysChanged)
        Q_PROPERTY(double sells READ getSells WRITE setSells NOTIFY sellsChanged)
        Q_PROPERTY(double averageBoughtCost READ getAverageBoughtCost WRITE setAverageBoughtCost NOTIFY averageBoughtCostChanged)
        Q_PROPERTY(double averageSoldCost READ getAverageSoldCost WRITE setAverageSoldCost NOTIFY averageSoldCostChanged)
        Q_PROPERTY(double totalBoughtCost READ getTotalBoughtCost WRITE setTotalBoughtCost NOTIFY totalBoughtCostChanged)
        Q_PROPERTY(double totalSoldCost READ getTotalSoldCost WRITE setTotalSoldCost NOTIFY totalSoldCostChanged)
        Q_PROPERTY(double averagePrice READ getAveragePrice WRITE setAveragePrice NOTIFY averagePriceChanged)
        Q_PROPERTY(double costBasis READ getCostBasis WRITE setCostBasis NOTIFY costBasisChanged)
        Q_PROPERTY(double net READ getNet WRITE setNet NOTIFY netChanged)

        public:

            enum class Action
            {
                BOUGHT,
                SOLD,
                NONE
            }; Q_ENUM(Action)

        private:
            QString market;
            Action action;
            double quantity;
            double price;
            double middlePrice;
            double bid;
            double ask;
            double commission;
            double realisedPnL;
            double unrealisedPnL;
            double buys;
            double sells;
            double averageBoughtCost;
            double averageSoldCost;
            double totalBoughtCost;
            double totalSoldCost;
            double averagePrice;
            double costBasis;
            double net;
            double netTotal;
            double netInclusiveCommission;
            double marketValue;

        public:
            explicit Position(QObject *parent=nullptr);
            explicit Position(QString market,trader::Position::Action action,double quantity,
                double price,double commission,double bid,double ask,QObject *parent=nullptr);
            ~Position();
            QString getMarket() const;
            void setMarket(const QString &value);
            trader::Position::Action getAction() const;
            void setAction(const trader::Position::Action &value);
            double getQuantity() const;
            void setQuantity(double value);
            double getPrice() const;
            void setPrice(double value);
            double getMiddlePrice() const;
            void setMiddlePrice(double value);
            double getBid() const;
            void setBid(double value);
            double getAsk() const;
            void setAsk(double value);
            double getCommission() const;
            void setCommission(double value);
            double getRealisedPnL() const;
            void setRealisedPnL(double value);
            double getUnrealisedPnL() const;
            void setUnrealisedPnL(double value);
            double getBuys() const;
            void setBuys(double value);
            double getSells() const;
            void setSells(double value);
            double getAverageBoughtCost() const;
            void setAverageBoughtCost(double value);
            double getAverageSoldCost() const;
            void setAverageSoldCost(double value);
            double getTotalBoughtCost() const;
            void setTotalBoughtCost(double value);
            double getTotalSoldCost() const;
            void setTotalSoldCost(double value);
            double getAveragePrice() const;
            void setAveragePrice(double value);
            double getCostBasis() const;
            void setCostBasis(double value);
            double getNet() const;
            void setNet(double value);
            double getNetTotal() const;
            void setNetTotal(double value);
            double getNetInclusiveCommission() const;
            void setNetInclusiveCommission(double value);
            double getMarketValue() const;
            void setMarketValue(double value);

        private:
            void initiate();

        signals:
            void marketChanged(const QString &market);
            void actionChanged(const trader::Position::Action &action);
            void quantityChanged(const double &quantity);
            void priceChanged(const double &price);
            void middlePriceChanged(const double &middlePrice);
            void bidChanged(const double &bid);
            void askChanged(const double &ask);
            void commissionChanged(const double &commission);
            void realisedPnLChanged(const double &realisedPnL);
            void unrealisedPnLChanged(const double &unrealisedPnL);
            void buysChanged(const double &buys);
            void sellsChanged(const double &sells);
            void averageBoughtCostChanged(const double &averageBoughtCost);
            void averageSoldCostChanged(const double &averageSoldCost);
            void totalBoughtCostChanged(const double &totalBoughtCost);
            void totalSoldCostChanged(const double &totalSoldCost);
            void averagePriceChanged(const double &averagePrice);
            void costBasisChanged(const double &costBasis);
            void netChanged(const double &net);
            void netTotalChanged(const double &netTotal);
            void netInclusiveCommissionChanged(const double &netInclusiveCommission);
            void marketValueChanged(const double &marketValue);
            void updated();
            void transacted();

        public slots:
            void update(double bid,double ask);
            void transact(trader::Position::Action action,double quantity,double price,double commission);
    };
}

Q_DECLARE_METATYPE(trader::Position *)

#endif // POSITION_HPP
