#ifndef MICROANALYSIS_HPP
#define MICROANALYSIS_HPP

#include <QObject>
#include <qqml.h>

namespace trader
{
    class MicroEconomicAnalysis: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(double bid READ getBid WRITE setBid NOTIFY bidChanged)
        Q_PROPERTY(double ask READ getAsk WRITE setAsk NOTIFY askChanged)
        Q_PROPERTY(double middlePrice READ getMiddlePrice WRITE setMiddlePrice NOTIFY middlePriceChanged)
        Q_PROPERTY(double high READ getHigh WRITE setHigh NOTIFY highChanged)
        Q_PROPERTY(double low READ getLow WRITE setLow NOTIFY lowChanged)
        Q_PROPERTY(double last READ getLast WRITE setLast NOTIFY lastChanged)
        Q_PROPERTY(double previousDay READ getPreviousDay WRITE setPreviousDay NOTIFY previousDayChanged)
        Q_PROPERTY(double volume READ getVolume WRITE setVolume NOTIFY volumeChanged)
        Q_PROPERTY(double baseVolume READ getBaseVolume WRITE setBaseVolume NOTIFY baseVolumeChanged)
        Q_PROPERTY(double depthImbalance READ getDepthImbalance WRITE setDepthImbalance NOTIFY depthImbalanceChanged)
        Q_PROPERTY(double volumeImbalance READ getVolumeImbalance WRITE setVolumeImbalance NOTIFY volumeImbalanceChanged)
        Q_PROPERTY(double spread READ getSpread WRITE setSpread NOTIFY spreadChanged)
        Q_PROPERTY(double spreadPercentage READ getSpreadPercentage WRITE setSpreadPercentage NOTIFY spreadPercentageChanged)
        Q_PROPERTY(double historicalVolatility READ getHistoricalVolatility WRITE setHistoricalVolatility NOTIFY historicalVolatilityChanged)
        Q_PROPERTY(double historicalVolatilityPercentage READ getHistoricalVolatilityPercentage WRITE setHistoricalVolatilityPercentage NOTIFY historicalVolatilityPercentageChanged)
        Q_PROPERTY(double momentum READ getMomentum WRITE setMomentum NOTIFY momentumChanged)
        Q_PROPERTY(double momentumPercentage READ getMomentumPercentage WRITE setMomentumPercentage NOTIFY momentumPercentageChanged)
        Q_PROPERTY(double bidVolume READ getBidVolume WRITE setBidVolume NOTIFY bidVolumeChanged)
        Q_PROPERTY(double askVolume READ getAskVolume WRITE setAskVolume NOTIFY askVolumeChanged)
        Q_PROPERTY(double averageBidVolume READ getAverageBidVolume WRITE setAverageBidVolume NOTIFY averageBidVolumeChanged)
        Q_PROPERTY(double averageAskVolume READ getAverageAskVolume WRITE setAverageAskVolume NOTIFY averageAskVolumeChanged)
        Q_PROPERTY(double baseBidVolume READ getBaseBidVolume WRITE setBaseBidVolume NOTIFY baseBidVolumeChanged)
        Q_PROPERTY(double baseAskVolume READ getBaseAskVolume WRITE setBaseAskVolume NOTIFY baseAskVolumeChanged)
        Q_PROPERTY(double averageBaseBidVolume READ getAverageBaseBidVolume WRITE setAverageBaseBidVolume NOTIFY averageBaseBidVolumeChanged)
        Q_PROPERTY(double averageBaseAskVolume READ getAverageBaseAskVolume WRITE setAverageBaseAskVolume NOTIFY averageBaseAskVolumeChanged)
        Q_PROPERTY(double buyVolume READ getBuyVolume WRITE setBuyVolume NOTIFY buyVolumeChanged)
        Q_PROPERTY(double sellVolume READ getSellVolume WRITE setSellVolume NOTIFY sellVolumeChanged)
        Q_PROPERTY(double averageBuyVolume READ getAverageBuyVolume WRITE setAverageBuyVolume NOTIFY averageBuyVolumeChanged)
        Q_PROPERTY(double averageSellVolume READ getAverageSellVolume WRITE setAverageSellVolume NOTIFY averageSellVolumeChanged)
        Q_PROPERTY(double baseBuyVolume READ getBaseBuyVolume WRITE setBaseBuyVolume NOTIFY baseBuyVolumeChanged)
        Q_PROPERTY(double baseSellVolume READ getBaseSellVolume WRITE setBaseSellVolume NOTIFY baseSellVolumeChanged)
        Q_PROPERTY(double averageBaseBuyVolume READ getAverageBaseBuyVolume WRITE setAverageBaseBuyVolume NOTIFY averageBaseBuyVolumeChanged)
        Q_PROPERTY(double averageBaseSellVolume READ getAverageBaseSellVolume WRITE setAverageBaseSellVolume NOTIFY averageBaseSellVolumeChanged)

        private:
            double  bid;
            double  ask;
            double  middlePrice;
            double  high;
            double  low;
            double  last;
            double  previousDay;
            double  volume;
            double  baseVolume;
            double  depthImbalance;
            double  volumeImbalance;
            double  spread;
            double  spreadPercentage;
            double  historicalVolatility;
            double  historicalVolatilityPercentage;
            double  momentum;
            double  momentumPercentage;
            double  bidVolume;
            double  askVolume;
            double  averageBidVolume;
            double  averageAskVolume;
            double  baseBidVolume;
            double  baseAskVolume;
            double  averageBaseBidVolume;
            double  averageBaseAskVolume;
            double  buyVolume;
            double  sellVolume;
            double  averageBuyVolume;
            double  averageSellVolume;
            double  baseBuyVolume;
            double  baseSellVolume;
            double  averageBaseBuyVolume;
            double  averageBaseSellVolume;

        public:
            explicit MicroEconomicAnalysis(QObject *parent=nullptr);
            ~MicroEconomicAnalysis();
            double getBid() const;
            void setBid(double value);
            double getAsk() const;
            void setAsk(double value);
            double getMiddlePrice() const;
            double getHigh() const;
            void setHigh(double value);
            double getLow() const;
            void setLow(double value);
            double getLast() const;
            void setLast(double value);
            double getPreviousDay() const;
            void setPreviousDay(double value);
            void setMiddlePrice(double value);
            double getVolume() const;
            void setVolume(double value);
            double getBaseVolume() const;
            void setBaseVolume(double value);
            double getDepthImbalance() const;
            void setDepthImbalance(double value);
            double getVolumeImbalance() const;
            void setVolumeImbalance(double value);
            double getSpread() const;
            void setSpread(double value);
            double getSpreadPercentage() const;
            void setSpreadPercentage(double value);
            double getHistoricalVolatility() const;
            void setHistoricalVolatility(double value);
            double getHistoricalVolatilityPercentage() const;
            void setHistoricalVolatilityPercentage(double value);
            double getMomentum() const;
            void setMomentum(double value);
            double getMomentumPercentage() const;
            void setMomentumPercentage(double value);
            double getBidVolume() const;
            void setBidVolume(double value);
            double getAskVolume() const;
            void setAskVolume(double value);
            double getAverageBidVolume() const;
            void setAverageBidVolume(double value);
            double getAverageAskVolume() const;
            void setAverageAskVolume(double value);
            double getBaseBidVolume() const;
            void setBaseBidVolume(double value);
            double getBaseAskVolume() const;
            void setBaseAskVolume(double value);
            double getAverageBaseBidVolume() const;
            void setAverageBaseBidVolume(double value);
            double getAverageBaseAskVolume() const;
            void setAverageBaseAskVolume(double value);
            double getBuyVolume() const;
            void setBuyVolume(double value);
            double getSellVolume() const;
            void setSellVolume(double value);
            double getAverageBuyVolume() const;
            void setAverageBuyVolume(double value);
            double getAverageSellVolume() const;
            void setAverageSellVolume(double value);
            double getBaseBuyVolume() const;
            void setBaseBuyVolume(double value);
            double getBaseSellVolume() const;
            void setBaseSellVolume(double value);
            double getAverageBaseBuyVolume() const;
            void setAverageBaseBuyVolume(double value);
            double getAverageBaseSellVolume() const;
            void setAverageBaseSellVolume(double value);
            void clear();

        signals:
            void bidChanged(const double &bid);
            void askChanged(const double &ask);
            void middlePriceChanged(const double &middlePrice);
            void highChanged(const double &high);
            void lowChanged(const double &low);
            void lastChanged(const double &last);
            void previousDayChanged(const double &last);
            void volumeChanged(const double &volume);
            void baseVolumeChanged(const double &baseVolume);
            void depthImbalanceChanged(const double &depthImbalance);
            void volumeImbalanceChanged(const double &volumeImbalance);
            void spreadChanged(const double &spread);
            void spreadPercentageChanged(const double &spreadPercentage);
            void historicalVolatilityChanged(const double &historicalVolatility);
            void historicalVolatilityPercentageChanged(const double &historicalVolatilityPercentage);
            void momentumChanged(const double &momentum);
            void momentumPercentageChanged(const double &momentumPercentage);
            void bidVolumeChanged(const double &bidVolume);
            void askVolumeChanged(const double &askVolume);
            void averageBidVolumeChanged(const double &averageBidVolume);
            void averageAskVolumeChanged(const double &averageAskVolume);
            void baseBidVolumeChanged(const double &baseBidVolume);
            void baseAskVolumeChanged(const double &baseAskVolume);
            void averageBaseBidVolumeChanged(const double &averageBaseBidVolume);
            void averageBaseAskVolumeChanged(const double &averageBaseAskVolume);
            void buyVolumeChanged(const double &buyVolume);
            void sellVolumeChanged(const double &sellVolume);
            void averageBuyVolumeChanged(const double &averageBuyVolume);
            void averageSellVolumeChanged(const double &averageSellVolume);
            void baseBuyVolumeChanged(const double &baseBuyVolume);
            void baseSellVolumeChanged(const double &baseSellVolume);
            void averageBaseBuyVolumeChanged(const double &averageBaseBuyVolume);
            void averageBaseSellVolumeChanged(const double &averageBaseSellVolume);
    };
}

Q_DECLARE_METATYPE(trader::MicroEconomicAnalysis *)

#endif // MICROANALYSIS_HPP
