#include "assetmanager.hpp"


trader::AssetManager::AssetManager(QObject *parent): QObject(parent)
{
    this->setData(QVariantMap());
    this->setAssets(new trader::AssetHash());
}


trader::AssetManager::~AssetManager()
{
    this->clearAssets();
    delete this->assets;
}

QVariantMap trader::AssetManager::getData() const { return this->data; }

void trader::AssetManager::setData(const QVariantMap &value)
{
    this->data=value;
    emit this->dataChanged(this->data);
}

trader::AssetHash *trader::AssetManager::getAssets() const { return this->assets; }

void trader::AssetManager::setAssets(trader::AssetHash *value)
{
    this->assets=value;
    emit this->assetsChanged(this->assets);
}


void trader::AssetManager::clearAssets()
{
    trader::AssetHash::iterator iterator;
    for (iterator=this->getAssets()->begin();iterator!=this->getAssets()->end();iterator++) {  iterator.value()->deleteLater(); }
    this->getAssets()->clear();
}


bool trader::AssetManager::addAsset(QString market,trader::Asset *asset)
{
    bool contains_asset=this->getAssets()->contains(market);
    if (contains_asset==true) { return false; }
    if (asset==nullptr) { return false; }
    this->getAssets()->insert(market,asset);
    this->connect(asset,&trader::Asset::started,this,[=]() { emit this->assetStarted(market,asset); });
    this->connect(asset,&trader::Asset::updated,this,[=]() { emit this->assetUpdated(market,asset); });
    this->connect(asset,&trader::Asset::stopped,this,[=]() { emit this->assetStopped(market,asset); });
    emit this->assetAdded(market,asset);
    return true;
}


bool trader::AssetManager::removeAsset(QString market)
{
    bool contains_asset=this->getAssets()->contains(market);
    if (contains_asset==false) { return false; }
    trader::Asset *asset=this->getAssets()->value(market);
    this->getAssets()->remove(market);
    emit this->assetRemoved(market,asset);
    asset->deleteLater();
    return true;
}


trader::Asset *trader::AssetManager::getAsset(QString market)
{
    bool contains_asset=this->getAssets()->contains(market);
    if (contains_asset==false) { return nullptr; }
    trader::Asset *asset=this->getAssets()->value(market);
    return asset;
}
