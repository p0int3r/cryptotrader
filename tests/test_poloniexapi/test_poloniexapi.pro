QT += testlib network quick
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_testpoloniexapi.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../poloniexapi/release/ -lpoloniexapi
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../poloniexapi/debug/ -lpoloniexapi
else:unix: LIBS += -L$$OUT_PWD/../../poloniexapi/ -lpoloniexapi

INCLUDEPATH += $$PWD/../../poloniexapi
DEPENDPATH += $$PWD/../../poloniexapi

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../poloniexapi/release/libpoloniexapi.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../poloniexapi/debug/libpoloniexapi.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../poloniexapi/release/poloniexapi.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../poloniexapi/debug/poloniexapi.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../poloniexapi/libpoloniexapi.a
