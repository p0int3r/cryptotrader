#include <QtTest>
#include "poloniexrestapihandler.hpp"
#include <qqml.h>

// add necessary includes here

class TestPoloniexApi: public QObject
{
    Q_OBJECT

    private:
        poloniexapi::PoloniexRestApiHandler *poloniexRestApiHandler=nullptr;

    public:
        TestPoloniexApi();
        ~TestPoloniexApi();

    private slots:
        void test_case1();
        void test_case2();
        void test_case3();
        void test_case4();
        void test_case5();
        void test_case6();
};

TestPoloniexApi::TestPoloniexApi()
{
    this->poloniexRestApiHandler=new poloniexapi::PoloniexRestApiHandler(this);
}

TestPoloniexApi::~TestPoloniexApi()
{
    delete this->poloniexRestApiHandler;
}

void TestPoloniexApi::test_case1()
{
    // Testing returnTicker

    QSignalSpy return_ticker_started_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryTickersStarted);
    QSignalSpy return_ticker_finished_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryTickersFinished);
    QVERIFY(return_ticker_started_spy.isValid());
    QVERIFY(return_ticker_finished_spy.isValid());
    this->poloniexRestApiHandler->queryTickers();
    return_ticker_started_spy.wait();
    QCOMPARE(return_ticker_started_spy.count(),1);
    return_ticker_finished_spy.wait();
    QCOMPARE(return_ticker_finished_spy.count(),1);
    QList<QVariant> arguments=return_ticker_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(0)).toObject();
    QJsonObject::iterator json_object_iterator;

    for (json_object_iterator=data.begin();json_object_iterator!=data.end();json_object_iterator++)
    {
        QJsonObject current_object=json_object_iterator->toObject();
        QVERIFY(current_object.contains("baseVolume"));
        QVERIFY(current_object.contains("high24hr"));
        QVERIFY(current_object.contains("highestBid"));
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("isFrozen"));
        QVERIFY(current_object.contains("last"));
        QVERIFY(current_object.contains("low24hr"));
        QVERIFY(current_object.contains("lowestAsk"));
        QVERIFY(current_object.contains("percentChange"));
        QVERIFY(current_object.contains("quoteVolume"));
    }
}

void TestPoloniexApi::test_case2()
{
    // Testing returnCurrencies

    QSignalSpy return_currencies_started_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryCurrenciesStarted);
    QSignalSpy return_currencies_finished_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryCurrenciesFinished);
    QVERIFY(return_currencies_started_spy.isValid());
    QVERIFY(return_currencies_finished_spy.isValid());
    this->poloniexRestApiHandler->queryCurrencies();
    return_currencies_started_spy.wait();
    QCOMPARE(return_currencies_started_spy.count(),1);
    return_currencies_finished_spy.wait();
    QCOMPARE(return_currencies_finished_spy.count(),1);
    QList<QVariant> arguments=return_currencies_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(0)).toObject();
    QJsonObject::iterator json_object_iterator;

    for (json_object_iterator=data.begin();json_object_iterator!=data.end();json_object_iterator++)
    {
        QJsonObject current_object=json_object_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("name"));
        QVERIFY(current_object.contains("txFee"));
        QVERIFY(current_object.contains("minConf"));
        QVERIFY(current_object.contains("depositAddress"));
        QVERIFY(current_object.contains("disabled"));
        QVERIFY(current_object.contains("delisted"));
        QVERIFY(current_object.contains("frozen"));
    }
}

void TestPoloniexApi::test_case3()
{
    // Testing return24hVolume

    QSignalSpy return_24h_volume_started_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryDailyVolumesStarted);
    QSignalSpy return_24h_volume_finished_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryDailyVolumesFinished);
    QVERIFY(return_24h_volume_started_spy.isValid());
    QVERIFY(return_24h_volume_finished_spy.isValid());
    this->poloniexRestApiHandler->queryDailyVolumes();
    return_24h_volume_started_spy.wait();
    QCOMPARE(return_24h_volume_started_spy.count(),1);
    return_24h_volume_finished_spy.wait();
    QCOMPARE(return_24h_volume_finished_spy.count(),1);
    QList<QVariant> arguments=return_24h_volume_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(0)).toObject();
    QVERIFY(data.isEmpty()==false);
}


void TestPoloniexApi::test_case4()
{
    // Testing returnLoanOrders

    QSignalSpy return_load_orders_started_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryLoanOrdersStarted);
    QSignalSpy return_load_orders_finished_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryLoanOrdersFinished);
    QVERIFY(return_load_orders_started_spy.isValid());
    QVERIFY(return_load_orders_finished_spy.isValid());
    this->poloniexRestApiHandler->queryLoanOrders("BTC");
    return_load_orders_started_spy.wait();
    QCOMPARE(return_load_orders_started_spy.count(),1);
    QList<QVariant> arguments=return_load_orders_started_spy.takeFirst();
    QString received_currency=arguments.at(0).toString();
    QCOMPARE(received_currency,QString("BTC"));
    return_load_orders_finished_spy.wait();
    QCOMPARE(return_load_orders_finished_spy.count(),1);
    arguments=return_load_orders_finished_spy.takeFirst();
    received_currency=arguments.at(0).toString();
    QCOMPARE(received_currency,QString("BTC"));
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("offers"));
    QVERIFY(data.contains("demands"));

    QJsonArray offers_json_array=data.value("offers").toArray();
    QJsonArray::iterator offers_json_array_iterator;

    for (offers_json_array_iterator=offers_json_array.begin();offers_json_array_iterator!=offers_json_array.end();offers_json_array_iterator++)
    {
        QJsonObject current_object=offers_json_array_iterator->toObject();
        QVERIFY(current_object.contains("rate"));
        QVERIFY(current_object.contains("amount"));
        QVERIFY(current_object.contains("rangeMin"));
        QVERIFY(current_object.contains("rangeMax"));
    }

    QJsonArray demands_json_array=data.value("demands").toArray();
    QJsonArray::iterator demands_json_array_iterator;

    for (demands_json_array_iterator=demands_json_array.begin();demands_json_array_iterator!=demands_json_array.end();demands_json_array_iterator++)
    {
        QJsonObject current_object=demands_json_array_iterator->toObject();
        QVERIFY(current_object.contains("rate"));
        QVERIFY(current_object.contains("amount"));
        QVERIFY(current_object.contains("rangeMin"));
        QVERIFY(current_object.contains("rangeMax"));
    }
}


void TestPoloniexApi::test_case5()
{
    // Testing returnOrderBook

    QSignalSpy return_order_book_started_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryOrderBookStarted);
    QSignalSpy return_order_book_finished_spy(this->poloniexRestApiHandler,&poloniexapi::PoloniexRestApiHandler::queryOrderBookFinished);
    QVERIFY(return_order_book_started_spy.isValid());
    QVERIFY(return_order_book_finished_spy.isValid());
    this->poloniexRestApiHandler->queryOrderBook("BTC_ETH",100);
    return_order_book_started_spy.wait();
    QCOMPARE(return_order_book_started_spy.count(),1);
    QList<QVariant> arguments=return_order_book_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    int received_depth=arguments.at(1).toInt();
    QCOMPARE(received_market,QString("BTC_ETH"));
    QCOMPARE(received_depth,100);
    return_order_book_finished_spy.wait();
    QCOMPARE(return_order_book_finished_spy.count(),1);
    arguments=return_order_book_finished_spy.takeFirst();
    received_market=arguments.at(0).toString();
    received_depth=arguments.at(1).toInt();
    QCOMPARE(received_market,QString("BTC_ETH"));
    QCOMPARE(received_depth,100);
    QJsonObject data=QJsonValue::fromVariant(arguments.at(2)).toObject();
    QVERIFY(data.contains("bids"));
    QVERIFY(data.contains("asks"));
    QVERIFY(data.contains("isFrozen"));
    QVERIFY(data.contains("seq"));
}


void TestPoloniexApi::test_case6()
{
    // Testing returnTradeHistory

}


QTEST_MAIN(TestPoloniexApi)

#include "tst_testpoloniexapi.moc"
