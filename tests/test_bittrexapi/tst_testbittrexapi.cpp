#include <QtTest>
#include "v1_bittrexrestapihandler.hpp"
#include "v3_bittrexrestapihandler.hpp"

// add necessary includes here

class TestBittrexApi: public QObject
{
    Q_OBJECT

    private:
        bittrexapi::v1::BittrexRestApiHandler *v1BittrexRestApiHandler=nullptr;
        bittrexapi::v3::BittrexRestApiHandler *v3BittrexRestApiHandler=nullptr;
        QString mockOrderId;
        QString exchangeOrderId;
        QString depositTxId;
        QString depositId;
        QString withdrawalTxId;
        QString withdrawalId;

    public:
        TestBittrexApi();
        ~TestBittrexApi();

    private slots:
        void test_v3_case1();
        void test_v3_case2();
        void test_v3_case3();
        void test_v3_case4();
        void test_v3_case5();
        void test_v3_case6();
        void test_v3_case7();
        void test_v3_case8();
        void test_v3_case9();
        void test_v3_case10();
        void test_v3_case11();
        void test_v3_case12();
        void test_v3_case13();
        void test_v3_case14();
        void test_v3_case15();
        void test_v3_case16();
        void test_v3_case17();
        void test_v3_case18();
        void test_v3_case19();
        void test_v3_case20();
        void test_v3_case21();
        void test_v3_case22();
        void test_v3_case23();
        void test_v3_case24();
        void test_v3_case25();
        void test_v3_case26();
        void test_v3_case27();
        void test_v3_case28();
        void test_v3_case29();
        void test_v3_case30();
        void test_v3_case31();
        void test_v3_case32();
        void test_v3_case33();
        void test_v3_case34();
        void test_v3_case35();
        void test_v3_case36();
        void test_v3_case37();
        void test_v3_case38();
        void test_v3_case39();
        void test_v3_case40();
};

TestBittrexApi::TestBittrexApi()
{
    this->mockOrderId=QString("axi123sikasfifg1239fkf");
    this->exchangeOrderId=QString("3b7e1067-d7fc-4ba8-a41c-61240c750fd5");
    this->depositTxId=QString("d23637924f72dc439205a7fb5fa60453ebc28c77ff0a4586ce0faa4de83b86b9");
    this->depositId=QString("10f4019d-14f6-46c5-9759-2fd52bedaa35");
    this->withdrawalTxId=QString("8d1f60decf6e0a833261877eb11e55c679b694349bd47443f1cabd78b346316f");
    this->withdrawalId=QString("15eda5b8-8ba5-4cad-b7ae-16975c4e5560");
    this->v1BittrexRestApiHandler=new bittrexapi::v1::BittrexRestApiHandler(this);
    this->v3BittrexRestApiHandler=new bittrexapi::v3::BittrexRestApiHandler(this);
    this->v1BittrexRestApiHandler->setPublicKey(QString("6cffbef61b504ed2a7b4f9a103b58284"));
    this->v1BittrexRestApiHandler->setPrivateKey(QString("5c205d80e79346fd923872a1e7aa1328"));
    this->v3BittrexRestApiHandler->setPublicKey(QString("6cffbef61b504ed2a7b4f9a103b58284"));
    this->v3BittrexRestApiHandler->setPrivateKey(QString("5c205d80e79346fd923872a1e7aa1328"));
}

TestBittrexApi::~TestBittrexApi()
{
    delete this->v1BittrexRestApiHandler;
    delete this->v3BittrexRestApiHandler;
}

void TestBittrexApi::test_v3_case1()
{
    // Testing GET /account

    QSignalSpy get_account_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAccountStarted);
    QSignalSpy get_account_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAccountFinished);
    QVERIFY(get_account_started_spy.isValid());
    QVERIFY(get_account_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryAccount();
    get_account_started_spy.wait();
    QCOMPARE(get_account_started_spy.count(),1);
    get_account_finished_spy.wait();
    QCOMPARE(get_account_finished_spy.count(),1);
    QList<QVariant> arguments=get_account_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(0)).toObject();
    QVERIFY(data.contains("accountId"));
}


void TestBittrexApi::test_v3_case2()
{
    // Testing GET /account/volume

    QSignalSpy get_account_volume_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAccountVolumeStarted);
    QSignalSpy get_account_volume_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAccountVolumeFinished);
    QVERIFY(get_account_volume_started_spy.isValid());
    QVERIFY(get_account_volume_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryAccountVolume();
    get_account_volume_started_spy.wait();
    QCOMPARE(get_account_volume_started_spy.count(),1);
    get_account_volume_finished_spy.wait();
    QCOMPARE(get_account_volume_finished_spy.count(),1);
    QList<QVariant> arguments=get_account_volume_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(0)).toObject();
//    QVERIFY(data.contains("updated"));
    QVERIFY(data.contains("volume30days"));
}


void TestBittrexApi::test_v3_case3()
{
    // Testing GET /addresses

    QSignalSpy get_addresses_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAddressesStarted);
    QSignalSpy get_addresses_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAddressesFinished);
    QVERIFY(get_addresses_started_spy.isValid());
    QVERIFY(get_addresses_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryAddresses();
    get_addresses_started_spy.wait();
    QCOMPARE(get_addresses_started_spy.count(),1);
    get_addresses_finished_spy.wait();
    QCOMPARE(get_addresses_finished_spy.count(),1);
    QList<QVariant> arguments=get_addresses_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("cryptoAddress"));
        QVERIFY(current_object.contains("currencySymbol"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
    }
}

void TestBittrexApi::test_v3_case4()
{
    // No need to test this, depends on api permissions.
    // Testing POST /addresses

//    QSignalSpy post_address_provision_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAddressProvisionStarted);
//    QSignalSpy post_address_provision_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAddressProvisionFinished);
//    QVERIFY(post_address_provision_started_spy.isValid());
//    QVERIFY(post_address_provision_finished_spy.isValid());
//    this->v3BittrexRestApiHandler->queryAddressProvision(QString("ADA"));
//    post_address_provision_started_spy.wait();
//    QCOMPARE(post_address_provision_started_spy.count(),1);
//    QList<QVariant> arguments=post_address_provision_started_spy.takeFirst();
//    QString received_currency=arguments.at(0).toString();
//    QCOMPARE(received_currency,QString("ADA"));
//    post_address_provision_finished_spy.wait();
//    QCOMPARE(post_address_provision_finished_spy.count(),1);
//    arguments=post_address_provision_finished_spy.takeFirst();
//    QJsonObject data=QJsonValue::fromVariant(arguments.at(0)).toObject();
//    QVERIFY(data.contains("status"));
//    QVERIFY(data.contains("cryptoAddress"));
//    QVERIFY(data.contains("currencySymbol"));
//    QVERIFY(data.contains("cryptoAddressTag"));
}


void TestBittrexApi::test_v3_case5()
{
    // Testing GET /addresses/{currencySymbol}

    QSignalSpy get_address_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAddressStarted);
    QSignalSpy get_address_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryAddressFinished);
    QVERIFY(get_address_started_spy.isValid());
    QVERIFY(get_address_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryAddress(QString("BTC"));
    get_address_started_spy.wait();
    QCOMPARE(get_address_started_spy.count(),1);
    QList<QVariant> arguments=get_address_started_spy.takeFirst();
    QString received_currency=arguments.at(0).toString();
    QCOMPARE(received_currency,QString("BTC"));
    get_address_finished_spy.wait();
    QCOMPARE(get_address_finished_spy.count(),1);
    arguments=get_address_finished_spy.takeFirst();
    received_currency=QJsonValue::fromVariant(arguments.at(0)).toString();
    QCOMPARE(received_currency,QString("BTC"));
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("cryptoAddress"));
    QVERIFY(data.contains("currencySymbol"));
    //    QVERIFY(data.contains("cryptoAddressTag"));
}


void TestBittrexApi::test_v3_case6()
{
    // Testing GET /balances

    QSignalSpy get_balances_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryBalancesStarted);
    QSignalSpy get_balances_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryBalancesFinished);
    QVERIFY(get_balances_started_spy.isValid());
    QVERIFY(get_balances_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryBalances();
    get_balances_started_spy.wait();
    QCOMPARE(get_balances_started_spy.count(),1);
    get_balances_finished_spy.wait();
    QCOMPARE(get_balances_finished_spy.count(),1);
    QList<QVariant> arguments=get_balances_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("total"));
        QVERIFY(current_object.contains("available"));
        QVERIFY(current_object.contains("updatedAt"));
    }
}


void TestBittrexApi::test_v3_case7()
{
    // Testing GET /balances/{currencySymbol}

    QSignalSpy get_balance_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryBalanceStarted);
    QSignalSpy get_balance_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryBalanceFinished);
    QVERIFY(get_balance_started_spy.isValid());
    QVERIFY(get_balance_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryBalance(QString("BTC"));
    get_balance_started_spy.wait();
    QCOMPARE(get_balance_started_spy.count(),1);
    QList<QVariant> arguments=get_balance_started_spy.takeFirst();
    QString received_currency=arguments.at(0).toString();
    QCOMPARE(received_currency,QString("BTC"));
    get_balance_finished_spy.wait();
    QCOMPARE(get_balance_finished_spy.count(),1);
    arguments=get_balance_finished_spy.takeFirst();
    received_currency=QJsonValue::fromVariant(arguments.at(0)).toString();
    QCOMPARE(received_currency,QString("BTC"));
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("currencySymbol"));
    QVERIFY(data.contains("total"));
    QVERIFY(data.contains("available"));
    QVERIFY(data.contains("updatedAt"));
}


void TestBittrexApi::test_v3_case8()
{
    // Testing GET /currencies

    QSignalSpy get_currencies_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryCurrenciesStarted);
    QSignalSpy get_currencies_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryCurrenciesFinished);
    QVERIFY(get_currencies_started_spy.isValid());
    QVERIFY(get_currencies_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryCurrencies();
    get_currencies_started_spy.wait();
    QCOMPARE(get_currencies_started_spy.count(),1);
    get_currencies_finished_spy.wait();
    QCOMPARE(get_currencies_finished_spy.count(),1);
    QList<QVariant> arguments=get_currencies_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("symbol"));
        QVERIFY(current_object.contains("name"));
        QVERIFY(current_object.contains("coinType"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("minConfirmations"));
        QVERIFY(current_object.contains("notice"));
        QVERIFY(current_object.contains("txFee"));
//        QVERIFY(current_object.contains("logoUrl"));
        QVERIFY(current_object.contains("prohibitedIn"));
    }
}


void TestBittrexApi::test_v3_case9()
{
    // Testing GET /currencies/{symbol}

    QSignalSpy get_currency_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryCurrencyStarted);
    QSignalSpy get_currency_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryCurrencyFinished);
    QVERIFY(get_currency_started_spy.isValid());
    QVERIFY(get_currency_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryCurrency(QString("BTC"));
    get_currency_started_spy.wait();
    QCOMPARE(get_currency_started_spy.count(),1);
    QList<QVariant> arguments=get_currency_started_spy.takeFirst();
    QString received_currency=arguments.at(0).toString();
    QCOMPARE(received_currency,QString("BTC"));
    get_currency_finished_spy.wait();
    QCOMPARE(get_currency_finished_spy.count(),1);
    arguments=get_currency_finished_spy.takeFirst();
    received_currency=QJsonValue::fromVariant(arguments.at(0)).toString();
    QCOMPARE(received_currency,QString("BTC"));
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("symbol"));
    QVERIFY(data.contains("name"));
    QVERIFY(data.contains("coinType"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("minConfirmations"));
    QVERIFY(data.contains("notice"));
    QVERIFY(data.contains("txFee"));
    QVERIFY(data.contains("logoUrl"));
    QVERIFY(data.contains("prohibitedIn"));
}

void TestBittrexApi::test_v3_case10()
{
    // Testing GET /deposits/open

    QSignalSpy get_deposits_open_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOpenDepositsStarted);
    QSignalSpy get_deposits_open_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOpenDepositsFinished);
    QVERIFY(get_deposits_open_started_spy.isValid());
    QVERIFY(get_deposits_open_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryOpenDeposits();
    get_deposits_open_started_spy.wait();
    QCOMPARE(get_deposits_open_started_spy.count(),1);
    get_deposits_open_finished_spy.wait();
    QCOMPARE(get_deposits_open_finished_spy.count(),1);
    QList<QVariant> arguments=get_deposits_open_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("cryptoAddress"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
        QVERIFY(current_object.contains("txId"));
        QVERIFY(current_object.contains("confirmations"));
        QVERIFY(current_object.contains("updatedAt"));
        QVERIFY(current_object.contains("completedAt"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("source"));
    }
}


void TestBittrexApi::test_v3_case11()
{
    // Testing GET /deposits/closed

    QSignalSpy get_deposits_closed_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryClosedDepositsStarted);
    QSignalSpy get_deposits_closed_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryClosedDepositsFinished);
    QVERIFY(get_deposits_closed_started_spy.isValid());
    QVERIFY(get_deposits_closed_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryClosedDeposits();
    get_deposits_closed_started_spy.wait();
    QCOMPARE(get_deposits_closed_started_spy.count(),1);
    get_deposits_closed_finished_spy.wait();
    QCOMPARE(get_deposits_closed_finished_spy.count(),1);
    QList<QVariant> arguments=get_deposits_closed_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("cryptoAddress"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
        QVERIFY(current_object.contains("txId"));
        QVERIFY(current_object.contains("confirmations"));
        QVERIFY(current_object.contains("updatedAt"));
        QVERIFY(current_object.contains("completedAt"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("source"));
    }
}


void TestBittrexApi::test_v3_case12()
{
    // Testing GET /deposits/ByTxId/{txId}

    QSignalSpy get_deposit_by_txid_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryDepositsByTxIdStarted);
    QSignalSpy get_deposit_by_txid_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryDepositsByTxIdFinished);
    QVERIFY(get_deposit_by_txid_started_spy.isValid());
    QVERIFY(get_deposit_by_txid_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryDepositsByTxId(this->depositTxId);
    get_deposit_by_txid_started_spy.wait();
    QCOMPARE(get_deposit_by_txid_started_spy.count(),1);
    QList<QVariant> arguments=get_deposit_by_txid_started_spy.takeFirst();
    QString received_txid=arguments.at(0).toString();
    QCOMPARE(received_txid,this->depositTxId);
    get_deposit_by_txid_finished_spy.wait();
    QCOMPARE(get_deposit_by_txid_finished_spy.count(),1);
    arguments=get_deposit_by_txid_finished_spy.takeFirst();
    received_txid=arguments.at(0).toString();
    QCOMPARE(received_txid,this->depositTxId);
    QJsonArray data=QJsonValue::fromVariant(arguments.at(1)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("cryptoAddress"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
        QVERIFY(current_object.contains("txId"));
        QVERIFY(current_object.contains("confirmations"));
        QVERIFY(current_object.contains("updatedAt"));
        QVERIFY(current_object.contains("completedAt"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("source"));
    }
}


void TestBittrexApi::test_v3_case13()
{
    // Testing GET /deposits/{depositId}

    QSignalSpy get_deposit_by_id_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryDepositByIdStarted);
    QSignalSpy get_deposit_by_id_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryDepositByIdFinished);
    QVERIFY(get_deposit_by_id_started_spy.isValid());
    QVERIFY(get_deposit_by_id_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryDepositById(this->depositId);
    get_deposit_by_id_started_spy.wait();
    QCOMPARE(get_deposit_by_id_started_spy.count(),1);
    QList<QVariant> arguments=get_deposit_by_id_started_spy.takeFirst();
    QString received_id=arguments.at(0).toString();
    QCOMPARE(received_id,this->depositId);
    get_deposit_by_id_finished_spy.wait();
    QCOMPARE(get_deposit_by_id_finished_spy.count(),1);
    arguments=get_deposit_by_id_finished_spy.takeFirst();
    received_id=arguments.at(0).toString();
    QCOMPARE(received_id,this->depositId);
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("id"));
    QVERIFY(data.contains("currencySymbol"));
    QVERIFY(data.contains("quantity"));
    QVERIFY(data.contains("cryptoAddress"));
//    QVERIFY(current_object.contains("cryptoAddressTag"));
    QVERIFY(data.contains("txId"));
    QVERIFY(data.contains("confirmations"));
    QVERIFY(data.contains("updatedAt"));
    QVERIFY(data.contains("completedAt"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("source"));
}


void TestBittrexApi::test_v3_case14()
{
    // Testing GET /markets

    QSignalSpy get_markets_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketsStarted);
    QSignalSpy get_markets_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketsFinished);
    QVERIFY(get_markets_started_spy.isValid());
    QVERIFY(get_markets_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarkets();
    get_markets_started_spy.wait();
    QCOMPARE(get_markets_started_spy.count(),1);
    get_markets_finished_spy.wait();
    QCOMPARE(get_markets_finished_spy.count(),1);
    QList<QVariant> arguments=get_markets_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("symbol"));
        QVERIFY(current_object.contains("baseCurrencySymbol"));
        QVERIFY(current_object.contains("quoteCurrencySymbol"));
        QVERIFY(current_object.contains("minTradeSize"));
        QVERIFY(current_object.contains("precision"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("createdAt"));
//        QVERIFY(current_object.contains("notice"));
        QVERIFY(current_object.contains("prohibitedIn"));
    }
}


void TestBittrexApi::test_v3_case15()
{
    // Testing GET /markets/summaries

    QSignalSpy get_markets_summaries_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummariesStarted);
    QSignalSpy get_markets_summaries_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummariesFinished);
    QVERIFY(get_markets_summaries_started_spy.isValid());
    QVERIFY(get_markets_summaries_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketSummaries();
    get_markets_summaries_started_spy.wait();
    QCOMPARE(get_markets_summaries_started_spy.count(),1);
    get_markets_summaries_finished_spy.wait();
    QCOMPARE(get_markets_summaries_finished_spy.count(),1);
    QList<QVariant> arguments=get_markets_summaries_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("symbol"));
        QVERIFY(current_object.contains("high"));
        QVERIFY(current_object.contains("low"));
        QVERIFY(current_object.contains("volume"));
//        QVERIFY(current_object.contains("baseVolume"));
        QVERIFY(current_object.contains("quoteVolume"));
//        QVERIFY(current_object.contains("percentChange"));
        QVERIFY(current_object.contains("updatedAt"));
    }
}


void TestBittrexApi::test_v3_case16()
{
    // Testing GET /markets/tickers

    QSignalSpy get_markets_tickers_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickersStarted);
    QSignalSpy get_markets_tickers_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickersFinished);
    QVERIFY(get_markets_tickers_started_spy.isValid());
    QVERIFY(get_markets_tickers_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketTickers();
    get_markets_tickers_started_spy.wait();
    QCOMPARE(get_markets_tickers_started_spy.count(),1);
    get_markets_tickers_finished_spy.wait();
    QCOMPARE(get_markets_tickers_finished_spy.count(),1);
    QList<QVariant> arguments=get_markets_tickers_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("symbol"));
        QVERIFY(current_object.contains("lastTradeRate"));
        QVERIFY(current_object.contains("bidRate"));
        QVERIFY(current_object.contains("askRate"));
    }
}

void TestBittrexApi::test_v3_case17()
{
    // Testing GET /markets/{marketSymbol}/ticker

    QSignalSpy get_markets_ticker_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickerStarted);
    QSignalSpy get_markets_ticker_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickerFinished);
    QVERIFY(get_markets_ticker_started_spy.isValid());
    QVERIFY(get_markets_ticker_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketTicker(QString("BTC-USD"));
    get_markets_ticker_started_spy.wait();
    QCOMPARE(get_markets_ticker_started_spy.count(),1);
    QList<QVariant> arguments=get_markets_ticker_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    QCOMPARE(received_market,QString("BTC-USD"));
    get_markets_ticker_finished_spy.wait();
    QCOMPARE(get_markets_ticker_finished_spy.count(),1);
    arguments=get_markets_ticker_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("symbol"));
    QVERIFY(data.contains("lastTradeRate"));
    QVERIFY(data.contains("bidRate"));
    QVERIFY(data.contains("askRate"));
}


void TestBittrexApi::test_v3_case18()
{
    // Testing GET /markets/{marketSymbol}

    QSignalSpy get_market_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketStarted);
    QSignalSpy get_market_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketFinished);
    QVERIFY(get_market_started_spy.isValid());
    QVERIFY(get_market_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarket(QString("BTC-USD"));
    get_market_started_spy.wait();
    QCOMPARE(get_market_started_spy.count(),1);
    QList<QVariant> arguments=get_market_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    QCOMPARE(received_market,QString("BTC-USD"));
    get_market_finished_spy.wait();
    arguments=get_market_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("symbol"));
    QVERIFY(data.contains("baseCurrencySymbol"));
    QVERIFY(data.contains("quoteCurrencySymbol"));
    QVERIFY(data.contains("minTradeSize"));
    QVERIFY(data.contains("precision"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("createdAt"));
//    QVERIFY(data.contains("notice"));
    QVERIFY(data.contains("prohibitedIn"));
}

void TestBittrexApi::test_v3_case19()
{
    // Testing GET /markets/{marketSymbol}/summary

    QSignalSpy get_market_summary_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummaryStarted);
    QSignalSpy get_market_summary_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummaryFinished);
    QVERIFY(get_market_summary_started_spy.isValid());
    QVERIFY(get_market_summary_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketSummary(QString("BTC-USD"));
    get_market_summary_started_spy.wait();
    QCOMPARE(get_market_summary_started_spy.count(),1);
    QList<QVariant> arguments=get_market_summary_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    QCOMPARE(received_market,QString("BTC-USD"));
    get_market_summary_finished_spy.wait();
    arguments=get_market_summary_finished_spy.takeFirst();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("symbol"));
    QVERIFY(data.contains("high"));
    QVERIFY(data.contains("low"));
    QVERIFY(data.contains("volume"));
//    QVERIFY(data.contains("baseVolume"));
    QVERIFY(data.contains("quoteVolume"));
//    QVERIFY(data.contains("percentChange"));
    QVERIFY(data.contains("updatedAt"));
}


void TestBittrexApi::test_v3_case20()
{
    // Testing GET /markets/{marketSymbol}/orderbook

    QSignalSpy get_market_order_book_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketOrderBookStarted);
    QSignalSpy get_market_order_book_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketOrderBookFinished);
    QVERIFY(get_market_order_book_started_spy.isValid());
    QVERIFY(get_market_order_book_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketOrderBook(QString("BTC-USD"),25);
    get_market_order_book_started_spy.wait();
    QCOMPARE(get_market_order_book_started_spy.count(),1);
    QList<QVariant> arguments=get_market_order_book_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    int order_book_depth=arguments.at(1).toInt();
    QCOMPARE(received_market,QString("BTC-USD"));
    QCOMPARE(order_book_depth,25);
    get_market_order_book_finished_spy.wait();
    arguments=get_market_order_book_finished_spy.takeFirst();
    received_market=arguments.at(0).toString();
    order_book_depth=arguments.at(1).toInt();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(2)).toObject();
    QVERIFY(data.contains("bid"));
    QVERIFY(data.contains("ask"));
}


void TestBittrexApi::test_v3_case21()
{
    // Testing GET /markets/{marketSymbol}/trades

    QSignalSpy get_market_trades_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTradesStarted);
    QSignalSpy get_market_trades_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTradesFinished);
    QVERIFY(get_market_trades_started_spy.isValid());
    QVERIFY(get_market_trades_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketTrades(QString("BTC-USD"));
    get_market_trades_started_spy.wait();
    QCOMPARE(get_market_trades_started_spy.count(),1);
    QList<QVariant> arguments=get_market_trades_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    QCOMPARE(received_market,QString("BTC-USD"));
    get_market_trades_finished_spy.wait();
    arguments=get_market_trades_finished_spy.takeFirst();
    received_market=arguments.at(0).toString();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(1)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("executedAt"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("rate"));
        QVERIFY(current_object.contains("takerSide"));
    }
}

void TestBittrexApi::test_v3_case22()
{
    // Testing GET /markets/{marketSymbol}/candles/{candleInterval}/recent

    QSignalSpy get_market_candles_recent_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketRecentCandlesStarted);
    QSignalSpy get_market_candles_recent_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketRecentCandlesFinished);
    QVERIFY(get_market_candles_recent_started_spy.isValid());
    QVERIFY(get_market_candles_recent_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketRecentCandles(QString("BTC-USD"),QString("MINUTE_1"));
    get_market_candles_recent_started_spy.wait();
    QCOMPARE(get_market_candles_recent_started_spy.count(),1);
    QList<QVariant> arguments=get_market_candles_recent_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    QString candle_interval=arguments.at(1).toString();
    QCOMPARE(received_market,QString("BTC-USD"));
    QCOMPARE(candle_interval,QString("MINUTE_1"));
    get_market_candles_recent_finished_spy.wait();
    arguments=get_market_candles_recent_finished_spy.takeFirst();
    received_market=arguments.at(0).toString();
    candle_interval=arguments.at(1).toString();
    QCOMPARE(received_market,QString("BTC-USD"));
    QCOMPARE(candle_interval,QString("MINUTE_1"));
    QJsonArray data=QJsonValue::fromVariant(arguments.at(2)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("startsAt"));
        QVERIFY(current_object.contains("open"));
        QVERIFY(current_object.contains("high"));
        QVERIFY(current_object.contains("low"));
        QVERIFY(current_object.contains("close"));
        QVERIFY(current_object.contains("volume"));
//        QVERIFY(current_object.contains("baseVolume"));
        QVERIFY(current_object.contains("quoteVolume"));
    }
}



void TestBittrexApi::test_v3_case23()
{
    // Testing GET /markets/{marketSymbol}/candles/{candleInterval}/historical/{year}/{month}/{day}

    QSignalSpy get_market_candles_historical_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketHistoricalCandlesStarted);
    QSignalSpy get_market_candles_historical_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketHistoricalCandlesFinished);
    QVERIFY(get_market_candles_historical_started_spy.isValid());
    QVERIFY(get_market_candles_historical_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryMarketHistoricalCandles(QString("BTC-USD"),QString("MINUTE_1"),2019,1,1);
    get_market_candles_historical_started_spy.wait();
    QCOMPARE(get_market_candles_historical_started_spy.count(),1);
    QList<QVariant> arguments=get_market_candles_historical_started_spy.takeFirst();
    QString received_market=arguments.at(0).toString();
    QString candle_interval=arguments.at(1).toString();
    int selected_year=arguments.at(2).toInt();
    int selected_month=arguments.at(3).toInt();
    int selected_day=arguments.at(4).toInt();
    QCOMPARE(received_market,QString("BTC-USD"));
    QCOMPARE(candle_interval,QString("MINUTE_1"));
    QCOMPARE(selected_year,2019);
    QCOMPARE(selected_month,1);
    QCOMPARE(selected_day,1);
    get_market_candles_historical_finished_spy.wait();
    QCOMPARE(get_market_candles_historical_finished_spy.count(),1);
    arguments=get_market_candles_historical_finished_spy.takeFirst();
    received_market=arguments.at(0).toString();
    candle_interval=arguments.at(1).toString();
    selected_year=arguments.at(2).toInt();
    selected_month=arguments.at(3).toInt();
    selected_day=arguments.at(4).toInt();
    QCOMPARE(received_market,QString("BTC-USD"));
    QCOMPARE(candle_interval,QString("MINUTE_1"));
    QCOMPARE(selected_year,2019);
    QCOMPARE(selected_month,1);
    QCOMPARE(selected_day,1);
    QJsonArray data=QJsonValue::fromVariant(arguments.at(4)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("startsAt"));
        QVERIFY(current_object.contains("open"));
        QVERIFY(current_object.contains("high"));
        QVERIFY(current_object.contains("low"));
        QVERIFY(current_object.contains("close"));
        QVERIFY(current_object.contains("volume"));
//        QVERIFY(current_object.contains("baseVolume"));
        QVERIFY(current_object.contains("quoteVolume"));
    }
}


void TestBittrexApi::test_v3_case24()
{
    // Testing GET /orders/closed

    QSignalSpy get_orders_closed_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryClosedOrdersStarted);
    QSignalSpy get_orders_closed_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryClosedOrdersFinished);
    QVERIFY(get_orders_closed_started_spy.isValid());
    QVERIFY(get_orders_closed_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryClosedOrders();
    get_orders_closed_started_spy.wait();
    QCOMPARE(get_orders_closed_started_spy.count(),1);
    get_orders_closed_finished_spy.wait();
    QCOMPARE(get_orders_closed_finished_spy.count(),1);
    QList<QVariant> arguments=get_orders_closed_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("marketSymbol"));
        QVERIFY(current_object.contains("direction"));
        QVERIFY(current_object.contains("type"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("limit"));
//        QVERIFY(current_object.contains("ceiling"));
        QVERIFY(current_object.contains("timeInForce"));
//        QVERIFY(current_object.contains("clientOrderId"));
        QVERIFY(current_object.contains("fillQuantity"));
        QVERIFY(current_object.contains("commission"));
        QVERIFY(current_object.contains("proceeds"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("createdAt"));
        QVERIFY(current_object.contains("updatedAt"));
        QVERIFY(current_object.contains("closedAt"));
//        QVERIFY(current_object.contains("orderToCancel"));
    }
}


void TestBittrexApi::test_v3_case25()
{
    // Testing GET /orders/open

    QSignalSpy get_orders_open_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOpenOrdersStarted);
    QSignalSpy get_orders_open_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOpenOrdersFinished);
    QVERIFY(get_orders_open_started_spy.isValid());
    QVERIFY(get_orders_open_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryOpenOrders();
    get_orders_open_started_spy.wait();
    QCOMPARE(get_orders_open_started_spy.count(),1);
    get_orders_open_finished_spy.wait();
    QCOMPARE(get_orders_open_finished_spy.count(),1);
    QList<QVariant> arguments=get_orders_open_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("marketSymbol"));
        QVERIFY(current_object.contains("direction"));
        QVERIFY(current_object.contains("type"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("limit"));
//        QVERIFY(current_object.contains("ceiling"));
        QVERIFY(current_object.contains("timeInForce"));
//        QVERIFY(current_object.contains("clientOrderId"));
        QVERIFY(current_object.contains("fillQuantity"));
        QVERIFY(current_object.contains("commission"));
        QVERIFY(current_object.contains("proceeds"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("createdAt"));
        QVERIFY(current_object.contains("updatedAt"));
        QVERIFY(current_object.contains("closedAt"));
//        QVERIFY(current_object.contains("orderToCancel"));
    }
}


void TestBittrexApi::test_v3_case26()
{
    // Testing GET /orders/{orderId}

    QSignalSpy get_order_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOrderStarted);
    QSignalSpy get_order_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOrderFinished);
    QVERIFY(get_order_started_spy.isValid());
    QVERIFY(get_order_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryOrder(this->mockOrderId,this->exchangeOrderId);
    get_order_started_spy.wait();
    QCOMPARE(get_order_started_spy.count(),1);
    QList<QVariant> arguments=get_order_started_spy.takeFirst();
    QString order_id=arguments.at(0).toString();
    QString exchange_order_id=arguments.at(1).toString();
    QCOMPARE(order_id,this->mockOrderId);
    QCOMPARE(exchange_order_id,this->exchangeOrderId);
    get_order_finished_spy.wait();
    QCOMPARE(get_order_finished_spy.count(),1);
    arguments=get_order_finished_spy.takeFirst();
    order_id=arguments.at(0).toString();
    exchange_order_id=arguments.at(1).toString();
    QCOMPARE(order_id,this->mockOrderId);
    QCOMPARE(exchange_order_id,this->exchangeOrderId);
    QJsonObject data=QJsonValue::fromVariant(arguments.at(2)).toObject();
    QVERIFY(data.contains("id"));
    QVERIFY(data.contains("marketSymbol"));
    QVERIFY(data.contains("direction"));
    QVERIFY(data.contains("type"));
    QVERIFY(data.contains("quantity"));
    QVERIFY(data.contains("limit"));
//    QVERIFY(data.contains("ceiling"));
    QVERIFY(data.contains("timeInForce"));
//    QVERIFY(data.contains("clientOrderId"));
    QVERIFY(data.contains("fillQuantity"));
    QVERIFY(data.contains("commission"));
    QVERIFY(data.contains("proceeds"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("createdAt"));
    QVERIFY(data.contains("updatedAt"));
    QVERIFY(data.contains("closedAt"));
//    QVERIFY(data.contains("orderToCancel"));
}


void TestBittrexApi::test_v3_case27()
{
    // Testing POST /orders

    QSignalSpy post_order_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::placeOrderStarted);
    QSignalSpy post_order_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::placeOrderFinished);
    QVERIFY(post_order_started_spy.isValid());
    QVERIFY(post_order_finished_spy.isValid());
    QJsonObject order_object=QJsonObject();
    order_object.insert("marketSymbol","BTC-USD");
    order_object.insert("direction","BUY");
    order_object.insert("type","LIMIT");
    order_object.insert("quantity",0.00345656);
    order_object.insert("limit",9100.188);
    order_object.insert("timeInForce","GOOD_TIL_CANCELLED");
    this->v3BittrexRestApiHandler->placeOrder(this->mockOrderId,order_object);
    post_order_started_spy.wait();
    QCOMPARE(post_order_started_spy.count(),1);
    QList<QVariant> arguments=post_order_started_spy.takeFirst();
    QString order_id=arguments.at(0).toString();
    QJsonObject received_order_object=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QCOMPARE(order_id,this->mockOrderId);
    QCOMPARE(order_object,received_order_object);
    post_order_finished_spy.wait();
    QCOMPARE(post_order_finished_spy.count(),1);
    arguments=post_order_finished_spy.takeFirst();
    order_id=arguments.at(0).toString();
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("id"));
    QVERIFY(data.contains("marketSymbol"));
    QVERIFY(data.contains("direction"));
    QVERIFY(data.contains("type"));
    QVERIFY(data.contains("quantity"));
//    QVERIFY(data.contains("ceiling"));
    QVERIFY(data.contains("limit"));
    QVERIFY(data.contains("timeInForce"));
//    QVERIFY(data.contains("clientOrderId"));
    QVERIFY(data.contains("fillQuantity"));
    QVERIFY(data.contains("commission"));
    QVERIFY(data.contains("proceeds"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("createdAt"));
    QVERIFY(data.contains("updatedAt"));
//    QVERIFY(data.contains("closedAt"));
//    QVERIFY(data.contains("orderToCancel"));
    this->exchangeOrderId=data.value("id").toString();
}

void TestBittrexApi::test_v3_case28()
{
    // Testing DELETE /orders/{orderId}

    QSignalSpy delete_order_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::cancelOrderStarted);
    QSignalSpy delete_order_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::cancelOrderFinished);
    QVERIFY(delete_order_started_spy.isValid());
    QVERIFY(delete_order_finished_spy.isValid());
    this->v3BittrexRestApiHandler->cancelOrder(this->mockOrderId,this->exchangeOrderId);
    delete_order_started_spy.wait();
    QCOMPARE(delete_order_started_spy.count(),1);
    QList<QVariant> arguments=delete_order_started_spy.takeFirst();
    QString order_id=arguments.at(0).toString();
    QString exchange_order_id=arguments.at(1).toString();
    QCOMPARE(order_id,this->mockOrderId);
    QCOMPARE(exchange_order_id,this->exchangeOrderId);
    delete_order_finished_spy.wait();
    QCOMPARE(delete_order_finished_spy.count(),1);
    arguments=delete_order_finished_spy.takeFirst();
    order_id=arguments.at(0).toString();
    exchange_order_id=arguments.at(1).toString();
    QCOMPARE(order_id,this->mockOrderId);
    QCOMPARE(exchange_order_id,this->exchangeOrderId);
    QJsonObject data=QJsonValue::fromVariant(arguments.at(2)).toObject();
    QVERIFY(data.contains("id"));
    QVERIFY(data.contains("marketSymbol"));
    QVERIFY(data.contains("direction"));
    QVERIFY(data.contains("type"));
    QVERIFY(data.contains("quantity"));
//    QVERIFY(data.contains("ceiling"));
    QVERIFY(data.contains("limit"));
    QVERIFY(data.contains("timeInForce"));
//    QVERIFY(data.contains("clientOrderId"));
    QVERIFY(data.contains("fillQuantity"));
    QVERIFY(data.contains("commission"));
    QVERIFY(data.contains("proceeds"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("createdAt"));
    QVERIFY(data.contains("updatedAt"));
    QVERIFY(data.contains("closedAt"));
//    QVERIFY(data.contains("orderToCancel"));
}


void TestBittrexApi::test_v3_case29()
{
    // Testing GET /withdrawals/open

    QSignalSpy get_withdrawals_open_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOpenWithdrawalsStarted);
    QSignalSpy get_withdrawals_open_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOpenWithdrawalsFinished);
    QVERIFY(get_withdrawals_open_started_spy.isValid());
    QVERIFY(get_withdrawals_open_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryOpenWithdrawals();
    get_withdrawals_open_started_spy.wait();
    QCOMPARE(get_withdrawals_open_started_spy.count(),1);
    get_withdrawals_open_finished_spy.wait();
    QCOMPARE(get_withdrawals_open_finished_spy.count(),1);
    QList<QVariant> arguments=get_withdrawals_open_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("cryptoAddress"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
        QVERIFY(current_object.contains("txCost"));
//        QVERIFY(current_object.contains("txId"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("createdAt"));
        QVERIFY(current_object.contains("completedAt"));
    }
}



void TestBittrexApi::test_v3_case30()
{
    // Testing GET /withdrawals/closed

    QSignalSpy get_withdrawals_closed_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryClosedWithdrawalsStarted);
    QSignalSpy get_withdrawals_closed_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryClosedWithdrawalsFinished);
    QVERIFY(get_withdrawals_closed_started_spy.isValid());
    QVERIFY(get_withdrawals_closed_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryClosedWithdrawals();
    get_withdrawals_closed_started_spy.wait();
    QCOMPARE(get_withdrawals_closed_started_spy.count(),1);
    get_withdrawals_closed_finished_spy.wait();
    QCOMPARE(get_withdrawals_closed_finished_spy.count(),1);
    QList<QVariant> arguments=get_withdrawals_closed_finished_spy.takeFirst();
    QJsonArray data=QJsonValue::fromVariant(arguments.at(0)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("cryptoAddress"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
        QVERIFY(current_object.contains("txCost"));
//        QVERIFY(current_object.contains("txId"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("createdAt"));
        QVERIFY(current_object.contains("completedAt"));
    }
}


void TestBittrexApi::test_v3_case31()
{
    // Testing GET /withdrawals/ByTxId/{txId}

    QSignalSpy get_withdrawals_by_txid_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryWithdrawalsByTxIdStarted);
    QSignalSpy get_withdrawals_by_txid_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryWithdrawalsByTxIdFinished);
    QVERIFY(get_withdrawals_by_txid_started_spy.isValid());
    QVERIFY(get_withdrawals_by_txid_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryWithdrawalsByTxId(this->withdrawalTxId);
    get_withdrawals_by_txid_started_spy.wait();
    QCOMPARE(get_withdrawals_by_txid_started_spy.count(),1);
    QList<QVariant> arguments=get_withdrawals_by_txid_started_spy.takeFirst();
    QString withdrawal_txid=arguments.at(0).toString();
    QCOMPARE(withdrawal_txid,this->withdrawalTxId);
    get_withdrawals_by_txid_finished_spy.wait();
    QCOMPARE(get_withdrawals_by_txid_finished_spy.count(),1);
    arguments=get_withdrawals_by_txid_finished_spy.takeFirst();
    withdrawal_txid=arguments.at(0).toString();
    QCOMPARE(withdrawal_txid,this->withdrawalTxId);
    QJsonArray data=QJsonValue::fromVariant(arguments.at(1)).toArray();
    QJsonArray::iterator json_array_iterator;

    for (json_array_iterator=data.begin();json_array_iterator!=data.end();json_array_iterator++)
    {
        QJsonObject current_object=json_array_iterator->toObject();
        QVERIFY(current_object.contains("id"));
        QVERIFY(current_object.contains("currencySymbol"));
        QVERIFY(current_object.contains("quantity"));
        QVERIFY(current_object.contains("cryptoAddress"));
//        QVERIFY(current_object.contains("cryptoAddressTag"));
        QVERIFY(current_object.contains("txCost"));
//        QVERIFY(current_object.contains("txId"));
        QVERIFY(current_object.contains("status"));
        QVERIFY(current_object.contains("createdAt"));
        QVERIFY(current_object.contains("completedAt"));
    }
}


void TestBittrexApi::test_v3_case32()
{
    // Testing GET /withdrawals/{withdrawalId}

    QSignalSpy get_withdrawal_by_id_started_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryWithdrawalByIdStarted);
    QSignalSpy get_withdrawal_by_id_finished_spy(this->v3BittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryWithdrawalByIdFinished);
    QVERIFY(get_withdrawal_by_id_started_spy.isValid());
    QVERIFY(get_withdrawal_by_id_finished_spy.isValid());
    this->v3BittrexRestApiHandler->queryWithdrawalById(this->withdrawalId);
    get_withdrawal_by_id_started_spy.wait();
    QCOMPARE(get_withdrawal_by_id_started_spy.count(),1);
    QList<QVariant> arguments=get_withdrawal_by_id_started_spy.takeFirst();
    QString withdrawal_id=arguments.at(0).toString();
    QCOMPARE(withdrawal_id,this->withdrawalId);
    get_withdrawal_by_id_finished_spy.wait();
    QCOMPARE(get_withdrawal_by_id_finished_spy.count(),1);
    arguments=get_withdrawal_by_id_finished_spy.takeFirst();
    withdrawal_id=arguments.at(0).toString();
    QCOMPARE(withdrawal_id,this->withdrawalId);
    QJsonObject data=QJsonValue::fromVariant(arguments.at(1)).toObject();
    QVERIFY(data.contains("id"));
    QVERIFY(data.contains("currencySymbol"));
    QVERIFY(data.contains("quantity"));
    QVERIFY(data.contains("cryptoAddress"));
//    QVERIFY(data.contains("cryptoAddressTag"));
    QVERIFY(data.contains("txCost"));
//    QVERIFY(data.contains("txId"));
    QVERIFY(data.contains("status"));
    QVERIFY(data.contains("createdAt"));
    QVERIFY(data.contains("completedAt"));
}


void TestBittrexApi::test_v3_case33()
{
    // Testing DELETE /withdrawals/{withdrawalId}
    // TODO: implement later.
}


void TestBittrexApi::test_v3_case34()
{
    // Testing POST /withdrawals
    // TODO: implement later.
}


void TestBittrexApi::test_v3_case35()
{
    // Testing GET /withdrawals/whitelistAddresses
    // TODO: implement later.

}


void TestBittrexApi::test_v3_case36()
{
    // Testing GET /conditional-orders/{conditionalOrderId}
    // TODO: implement later.
}

void TestBittrexApi::test_v3_case37()
{
    // Testing DELETE /conditional-orders/{conditionalOrderId}
    // TODO: implement later.

}


void TestBittrexApi::test_v3_case38()
{
    // Testing GET /conditional-orders/closed
    // TODO: implement later.

}

void TestBittrexApi::test_v3_case39()
{
    // Testing GET /conditional-orders/open
    // TODO: implement later.

}


void TestBittrexApi::test_v3_case40()
{
    // Testing POST /conditional-orders
    // TODO: implement later.
}

QTEST_MAIN(TestBittrexApi)

#include "tst_testbittrexapi.moc"
