QT += testlib network quick
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_testbittrexapi.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../bittrexapi/release/ -lbittrexapi
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../bittrexapi/debug/ -lbittrexapi
else:unix: LIBS += -L$$OUT_PWD/../../bittrexapi/ -lbittrexapi

INCLUDEPATH += $$PWD/../../bittrexapi
DEPENDPATH += $$PWD/../../bittrexapi

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../bittrexapi/release/libbittrexapi.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../bittrexapi/debug/libbittrexapi.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../bittrexapi/release/bittrexapi.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../bittrexapi/debug/bittrexapi.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../bittrexapi/libbittrexapi.a
