#include <QDateTime>
#include "v1_bittrexrestapihandler.hpp"



bittrexapi::v1::BittrexRestApiHandler::BittrexRestApiHandler(QObject *parent): QObject(parent)
{
    this->setPublicKey(QString()); this->setPrivateKey(QString());
    this->setScheme("https"); this->setHost("api.bittrex.com");
    this->setRestClient(new QNetworkAccessManager(parent));
    this->connect(this->restClient,&QNetworkAccessManager::finished,this,&bittrexapi::v1::BittrexRestApiHandler::finalizeReply);
}


bittrexapi::v1::BittrexRestApiHandler::~BittrexRestApiHandler() { delete this->restClient; }

QString bittrexapi::v1::BittrexRestApiHandler::getScheme() const { return this->scheme; }

void bittrexapi::v1::BittrexRestApiHandler::setScheme(const QString &value)
{
    this->scheme=value;
    emit this->schemeChanged(this->scheme);
}

QString bittrexapi::v1::BittrexRestApiHandler::getHost() const { return this->host; }

void bittrexapi::v1::BittrexRestApiHandler::setHost(const QString &value)
{
    this->host=value;
    emit this->hostChanged(this->host);
}

QString bittrexapi::v1::BittrexRestApiHandler::getPublicKey() const { return this->publicKey; }

void bittrexapi::v1::BittrexRestApiHandler::setPublicKey(const QString &value)
{
    this->publicKey=value;
    emit this->publicKeyChanged(this->publicKey);
}

QString bittrexapi::v1::BittrexRestApiHandler::getPrivateKey() const { return this->privateKey; }

void bittrexapi::v1::BittrexRestApiHandler::setPrivateKey(const QString &value)
{
    this->privateKey=value;
    emit this->privateKeyChanged(this->privateKey);
}


QNetworkAccessManager *bittrexapi::v1::BittrexRestApiHandler::getRestClient() const { return this->restClient; }

void bittrexapi::v1::BittrexRestApiHandler::setRestClient(QNetworkAccessManager *value)
{
    this->restClient=value;
    emit this->restClientChanged(this->restClient);
}


void bittrexapi::v1::BittrexRestApiHandler::queryMarkets()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getmarkets");
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getmarkets");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketsStarted();
}


void bittrexapi::v1::BittrexRestApiHandler::queryCurrencies()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getcurrencies");
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getcurrencies");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryCurrenciesStarted();
}


void bittrexapi::v1::BittrexRestApiHandler::queryTicker(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getticker");
    bittrexUrl.setQuery("market="+market);
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getticker");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryTickerStarted(market);
}


void bittrexapi::v1::BittrexRestApiHandler::queryMarketSummaries()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getmarketsummaries");
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getmarketsummaries");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketSummariesStarted();
}


void bittrexapi::v1::BittrexRestApiHandler::queryMarketSummary(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getmarketsummary");
    bittrexUrl.setQuery("market="+market);
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getmarketsummary");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketSummaryStarted(market);
}


void bittrexapi::v1::BittrexRestApiHandler::queryOrderBook(const QString &market,const QString &type)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getorderbook");
    bittrexUrl.setQuery("market="+market+"&type="+type);
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getorderbook");
    reply->setProperty("market",market);
    reply->setProperty("type",type);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOrderBookStarted(market,type);
}


void bittrexapi::v1::BittrexRestApiHandler::queryMarketHistory(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/public/getmarkethistory");
    bittrexUrl.setQuery("market="+market);
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getmarkethistory");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketHistoryStarted(market);
}


void bittrexapi::v1::BittrexRestApiHandler::placeLimitBuy(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/market/buylimit");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&market="+market;
    query+="&quantity="+QString::number(quantity)+"&rate="+QString::number(rate);
    query+="&timeInForce="+timeInForce+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","buylimit");
    reply->setProperty("orderId",orderId);
    reply->setProperty("market",market);
    reply->setProperty("quantity",quantity);
    reply->setProperty("rate",rate);
    reply->setProperty("timeInForce",timeInForce);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->placeLimitBuyStarted(orderId,market,quantity,rate,timeInForce);
}


void bittrexapi::v1::BittrexRestApiHandler::placeLimitSell(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/market/selllimit");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&market="+market;
    query+="&quantity="+QString::number(quantity)+"&rate="+QString::number(rate);
    query+="&timeInForce="+timeInForce+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","selllimit");
    reply->setProperty("orderId",orderId);
    reply->setProperty("market",market);
    reply->setProperty("quantity",quantity);
    reply->setProperty("rate",rate);
    reply->setProperty("timeInForce",timeInForce);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->placeLimitSellStarted(orderId,market,quantity,rate,timeInForce);
}


void bittrexapi::v1::BittrexRestApiHandler::placeCancel(const QString &orderId,const QString &uuid)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/market/cancel");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&uuid="+uuid+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","cancel");
    reply->setProperty("orderId",orderId);
    reply->setProperty("uuid",uuid);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->placeCancelStarted(orderId,uuid);
}


void bittrexapi::v1::BittrexRestApiHandler::queryOpenOrders(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/market/getopenorders");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&market="+market+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getopenorders");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOpenOrdersStarted(market);
}


void bittrexapi::v1::BittrexRestApiHandler::queryBalances()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getbalances");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getbalances");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryBalancesStarted();
}


void bittrexapi::v1::BittrexRestApiHandler::queryBalance(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getbalance");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&currency="+currency+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getbalance");
    reply->setProperty("currency",currency);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryBalanceStarted(currency);

}



void bittrexapi::v1::BittrexRestApiHandler::queryDepositAddress(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getdepositaddress");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&currency="+currency+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getdepositaddress");
    reply->setProperty("currency",currency);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryDepositAddressStarted(currency);
}



void bittrexapi::v1::BittrexRestApiHandler::queryWithdrawal(const QString &currency,const double &quantity,const QString &address)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/withdraw");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&currency="+currency;
    query+="&quantity="+QString::number(quantity)+"&address="+address+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","withdraw");
    reply->setProperty("currency",currency);
    reply->setProperty("quantity",quantity);
    reply->setProperty("address",address);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryWithdrawalStarted(currency,quantity,address);
}


void bittrexapi::v1::BittrexRestApiHandler::queryOrder(const QString &orderId,const QString &uuid)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getorder");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&uuid="+uuid+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getorder");
    reply->setProperty("orderId",orderId);
    reply->setProperty("uuid",uuid);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOrderStarted(orderId,uuid);
}


void bittrexapi::v1::BittrexRestApiHandler::queryOrderHistory(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getorderhistory");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&market="+market+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getorderhistory");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOrderHistoryStarted(market);
}


void bittrexapi::v1::BittrexRestApiHandler::queryWithdrawalHistory(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getwithdrawalhistory");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&currency="+currency+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getwithdrawalhistory");
    reply->setProperty("currency",currency);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryWithdrawalHistoryStarted(currency);
}


void bittrexapi::v1::BittrexRestApiHandler::queryDepositHistory(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath("/api/v1.1/account/getdeposithistory");
    QString nonce=QString::number(QDateTime::currentSecsSinceEpoch());
    QString query="apikey="+this->getPublicKey()+"&currency="+currency+"&nonce="+nonce;
    bittrexUrl.setQuery(query);
    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    cypher.addData(bittrexUrl.toEncoded());
    QByteArray signature=cypher.result().toHex();
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("apisign"),signature);
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","getdeposithistory");
    reply->setProperty("currency",currency);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryDepositHistoryStarted(currency);
}


void bittrexapi::v1::BittrexRestApiHandler::processReply(QNetworkReply *reply)
{
    qint64 bytesAvailable=reply->bytesAvailable();
    QByteArray currentResult=reply->property("result").toByteArray()+reply->read(bytesAvailable);
    reply->setProperty("result",currentResult);
}


void bittrexapi::v1::BittrexRestApiHandler::finalizeReply(QNetworkReply *reply)
{
    QByteArray output=reply->property("result").toByteArray();
    QJsonDocument document=QJsonDocument::fromJson(output);
    QJsonObject data=document.object();
    QString apiCall=reply->property("apiCall").toString();

    if (apiCall=="getmarkets")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        emit this->queryMarketsFinished(success,message,result);
    }
    else if (apiCall=="getcurrencies")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        emit this->queryCurrenciesFinished(success,message,result);
    }
    else if (apiCall=="getticker")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString market=reply->property("market").toString();
        emit this->queryTickerFinished(market,success,message,result);
    }
    else if (apiCall=="getmarketsummaries")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        emit this->queryMarketSummariesFinished(success,message,result);
    }
    else if (apiCall=="getmarketsummary")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString market=reply->property("market").toString();
        emit this->queryMarketSummaryFinished(market,success,message,result);
    }
    else if (apiCall=="getorderbook")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonValue result=data.value("result");
        QString market=reply->property("market").toString();
        QString type=reply->property("type").toString();
        emit this->queryOrderBookFinished(market,type,success,message,result);
    }
    else if (apiCall=="getmarkethistory")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString market=reply->property("market").toString();
        emit this->queryMarketHistoryFinished(market,success,message,result);
    }
    else if (apiCall=="buylimit")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString orderId=reply->property("orderId").toString();
        QString market=reply->property("market").toString();
        double quantity=reply->property("quantity").toDouble();
        double rate=reply->property("rate").toDouble();
        QString timeInForce=reply->property("timeInForce").toString();
        emit this->placeLimitBuyFinished(orderId,market,quantity,rate,timeInForce,success,message,result);
    }
    else if (apiCall=="selllimit")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString orderId=reply->property("orderId").toString();
        QString market=reply->property("market").toString();
        double quantity=reply->property("quantity").toDouble();
        double rate=reply->property("rate").toDouble();
        QString timeInForce=reply->property("timeInForce").toString();
        emit this->placeLimitSellFinished(orderId,market,quantity,rate,timeInForce,success,message,result);
    }
    else if (apiCall=="cancel")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString orderId=reply->property("orderId").toString();
        QString uuid=reply->property("uuid").toString();
        this->placeCancelFinished(orderId,uuid,success,message,result);
    }
    else if (apiCall=="getopenorders")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString market=reply->property("market").toString();
        emit this->queryOpenOrdersFinished(market,success,message,result);
    }
    else if (apiCall=="getbalances")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        emit this->queryBalancesFinished(success,message,result);
    }
    else if (apiCall=="getbalance")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString currency=reply->property("currency").toString();
        emit this->queryBalanceFinished(currency,success,message,result);
    }
    else if (apiCall=="getdepositaddress")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString currency=reply->property("currency").toString();
        emit this->queryDepositAddressFinished(currency,success,message,result);
    }
    else if (apiCall=="withdraw")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString currency=reply->property("currency").toString();
        double quantity=reply->property("quantity").toDouble();
        QString address=reply->property("address").toString();
        emit this->queryWithdrawalFinished(currency,quantity,address,success,message,result);
    }
    else if (apiCall=="getorder")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonObject result=data.value("result").toObject();
        QString orderId=reply->property("orderId").toString();
        QString uuid=reply->property("uuid").toString();
        emit this->queryOrderFinished(orderId,uuid,success,message,result);
    }
    else if (apiCall=="getorderhistory")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString market=reply->property("market").toString();
        emit this->queryOrderHistoryFinished(market,success,message,result);
    }
    else if (apiCall=="getwithdrawalhistory")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString currency=reply->property("currency").toString();
        emit this->queryWithdrawalHistoryFinished(currency,success,message,result);
    }
    else if (apiCall=="getdeposithistory")
    {
        bool success=data.value("success").toBool();
        QString message=data.value("message").toString();
        QJsonArray result=data.value("result").toArray();
        QString currency=reply->property("currency").toString();
        emit this->queryDepositHistoryFinished(currency,success,message,result);
    } else {}
    reply->deleteLater();
}
