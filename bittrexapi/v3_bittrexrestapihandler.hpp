#ifndef V3_BITTREXRESTAPIHANDLER_HPP
#define V3_BITTREXRESTAPIHANDLER_HPP

#include <QObject>
#include <QObject>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariantMap>
#include <QJsonObject>
#include <QMessageAuthenticationCode>
#include <QNetworkAccessManager>
#include <QCryptographicHash>
#include <qqml.h>

namespace bittrexapi::v3
{
    class BittrexRestApiHandler: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString scheme READ getScheme WRITE setScheme NOTIFY schemeChanged)
        Q_PROPERTY(QString host READ getHost WRITE setHost NOTIFY hostChanged)
        Q_PROPERTY(QString publicKey READ getPublicKey WRITE setPublicKey NOTIFY publicKeyChanged)
        Q_PROPERTY(QString privateKey READ getPrivateKey WRITE setPrivateKey NOTIFY privateKeyChanged)
        Q_PROPERTY(QNetworkAccessManager *restClient READ getRestClient WRITE setRestClient NOTIFY restClientChanged)

        private:
            QString scheme;
            QString host;
            QString publicKey;
            QString privateKey;
            QNetworkAccessManager *restClient=nullptr;

        public:
            explicit BittrexRestApiHandler(QObject *parent=nullptr);
            ~BittrexRestApiHandler();
            QString getScheme() const;
            void setScheme(const QString &value);
            QString getHost() const;
            void setHost(const QString &value);
            QString getPublicKey() const;
            void setPublicKey(const QString &value);
            QString getPrivateKey() const;
            void setPrivateKey(const QString &value);
            QNetworkAccessManager *getRestClient() const;
            void setRestClient(QNetworkAccessManager *value);

        signals:
            void schemeChanged(const QString &scheme);
            void hostChanged(const QString &host);
            void publicKeyChanged(const QString &publicKey);
            void privateKeyChanged(const QString &privateKey);
            void restClientChanged(const QNetworkAccessManager *restClient);

            void queryAccountStarted();
            void queryAccountFinished(const QJsonObject &data);
            void queryAccountVolumeStarted();
            void queryAccountVolumeFinished(const QJsonObject &data);
            void queryAddressesStarted();
            void queryAddressesFinished(const QJsonArray &data);
            void queryAddressProvisionStarted(const QString &currency);
            void queryAddressProvisionFinished(const QString &currency,const QJsonObject &data);
            void queryAddressStarted(const QString &currency);
            void queryAddressFinished(const QString &currency,const QJsonObject &data);
            void queryBalancesStarted();
            void queryBalancesFinished(const QJsonArray &data);
            void queryBalanceStarted(const QString &currency);
            void queryBalanceFinished(const QString &currency,const QJsonObject &data);
            void queryCurrenciesStarted();
            void queryCurrenciesFinished(const QJsonArray &data);
            void queryCurrencyStarted(const QString &currency);
            void queryCurrencyFinished(const QString &currency,const QJsonObject &data);
            void queryOpenConditionalOrdersStarted();
            void queryOpenConditionalOrdersFinished(const QJsonArray &data);
            void queryClosedConditionalOrdersStarted();
            void queryClosedConditionalOrdersFinished(const QJsonArray &data);
            void queryConditionalOrderStarted(const QString &orderId,const QString &conditionalOrderId);
            void queryConditionalOrderFinished(const QString &orderId,const QString &conditionalorderId,const QJsonObject &data);
            void placeConditionalOrderStarted(const QString &orderId,const QJsonObject &data);
            void placeConditionalOrderFinished(const QString &orderId,const QJsonObject &data);
            void cancelConditionalOrderStarted(const QString &orderId,const QString &conditionalOrderId);
            void cancelConditionalOrderFinished(const QString &orderId,const QString &conditionalOrderId,const QJsonObject &data);
            void queryOpenDepositsStarted();
            void queryOpenDepositsFinished(const QJsonArray &data);
            void queryClosedDepositsStarted();
            void queryClosedDepositsFinished(const QJsonArray &data);
            void queryDepositsByTxIdStarted(const QString &txId);
            void queryDepositsByTxIdFinished(const QString &txId,const QJsonArray &data);
            void queryDepositByIdStarted(const QString &depositId);
            void queryDepositByIdFinished(const QString &depositId,const QJsonObject &data);
            void queryMarketsStarted();
            void queryMarketsFinished(const QJsonArray &data);
            void queryMarketSummariesStarted();
            void queryMarketSummariesFinished(const QJsonArray &data);
            void queryMarketTickersStarted();
            void queryMarketTickersFinished(const QJsonArray &data);
            void queryMarketStarted(const QString &market);
            void queryMarketFinished(const QString &market,const QJsonObject &data);
            void queryMarketSummaryStarted(const QString &market);
            void queryMarketSummaryFinished(const QString &market,const QJsonObject &data);
            void queryMarketOrderBookStarted(const QString &market,const int &depth);
            void queryMarketOrderBookFinished(const QString &market,const int &depth,const QJsonObject &data);
            void queryMarketTradesStarted(const QString &market);
            void queryMarketTradesFinished(const QString &market,const QJsonArray &data);
            void queryMarketTickerStarted(const QString &market);
            void queryMarketTickerFinished(const QString &market,const QJsonObject &data);
            void queryMarketRecentCandlesStarted(const QString &market,const QString &candleInterval);
            void queryMarketRecentCandlesFinished(const QString &market,const QString &candleInterval,const QJsonArray &data);
            void queryMarketHistoricalCandlesStarted(const QString &market,const QString &candleInterval,const int &year,const int &month,const int &day);
            void queryMarketHistoricalCandlesFinished(const QString &market,const QString &candleInterval,const int &year,const int &month,const int &day,const QJsonArray &data);
            void queryOpenOrdersStarted();
            void queryOpenOrdersFinished(const QJsonArray &data);
            void queryClosedOrdersStarted();
            void queryClosedOrdersFinished(const QJsonArray &data);
            void queryOrderStarted(const QString &orderId,const QString &exchangeOrderId);
            void queryOrderFinished(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &data);
            void cancelOrderStarted(const QString &orderId,const QString &exchangeOrderId);
            void cancelOrderFinished(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &data);
            void placeOrderStarted(const QString &orderId,const QJsonObject &data);
            void placeOrderFinished(const QString &orderId,const QJsonObject &data);
            void queryPingStarted();
            void queryPingFinished(const QJsonObject &data);
            void queryOpenWithdrawalsStarted();
            void queryOpenWithdrawalsFinished(const QJsonArray &data);
            void queryClosedWithdrawalsStarted();
            void queryClosedWithdrawalsFinished(const QJsonArray &data);
            void queryWithdrawalsByTxIdStarted(const QString &txId);
            void queryWithdrawalsByTxIdFinished(const QString &txId,const QJsonArray &data);
            void queryWithdrawalByIdStarted(const QString &withdrawalId);
            void queryWithdrawalByIdFinished(const QString &withdrawalId,const QJsonObject &data);

        private slots:
            void finalizeReply(QNetworkReply *reply);
            void processReply(QNetworkReply *reply);

        public slots:
            void queryAccount();
            void queryAccountVolume();
            void queryAddresses();
            void queryAddressProvision(const QString &currency);
            void queryAddress(const QString &currency);
            void queryBalances();
            void queryBalance(const QString &currency);
            void queryCurrencies();
            void queryCurrency(const QString &currency);
            void queryOpenConditionalOrders();
            void queryClosedConditionalOrders();
            void queryConditionalOrder(const QString &orderId,const QString &conditionalOrderId);
            void cancelConditionalOrder(const QString &orderId,const QString &conditionalOrderId);
            void placeConditionalOrder(const QString &orderId,const QJsonObject &conditionalOrder);
            void queryOpenDeposits();
            void queryClosedDeposits();
            void queryDepositsByTxId(const QString &txId);
            void queryDepositById(const QString &depositId);
            void queryMarkets();
            void queryMarketSummaries();
            void queryMarketTickers();
            void queryMarket(const QString &market);
            void queryMarketSummary(const QString &market);
            void queryMarketOrderBook(const QString &market,const int &depth);
            void queryMarketTrades(const QString &market);
            void queryMarketTicker(const QString &market);
            void queryMarketRecentCandles(const QString &market,const QString &candleInterval);
            void queryMarketHistoricalCandles(const QString &market,const QString &candleInterval,const int &year,const int &month,const int &day);
            void queryOpenOrders();
            void queryClosedOrders();
            void queryOrder(const QString &orderId,const QString &exchangeOrderId);
            void cancelOrder(const QString &orderId,const QString &exchangeOrderId);
            void placeOrder(const QString &orderId,const QJsonObject &data);
            void queryPing();
            void queryOpenWithdrawals();
            void queryClosedWithdrawals();
            void queryWithdrawalsByTxId(const QString &txId);
            void queryWithdrawalById(const QString &withdrawalId);
    };
}

Q_DECLARE_METATYPE(bittrexapi::v3::BittrexRestApiHandler *)

#endif // V3_BITTREXRESTAPIHANDLER_HPP
