#ifndef V1_BITTREXRESTAPIHANDLER_HPP
#define V1_BITTREXRESTAPIHANDLER_HPP

#include <QObject>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariantMap>
#include <QJsonObject>
#include <QMessageAuthenticationCode>
#include <QNetworkAccessManager>
#include <QCryptographicHash>
#include <qqml.h>


namespace bittrexapi::v1
{
    class BittrexRestApiHandler: public QObject
    {

        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString scheme READ getScheme WRITE setScheme NOTIFY schemeChanged)
        Q_PROPERTY(QString host READ getHost WRITE setHost NOTIFY hostChanged)
        Q_PROPERTY(QString publicKey READ getPublicKey WRITE setPublicKey NOTIFY publicKeyChanged)
        Q_PROPERTY(QString privateKey READ getPrivateKey WRITE setPrivateKey NOTIFY privateKeyChanged)
        Q_PROPERTY(QNetworkAccessManager *restClient READ getRestClient WRITE setRestClient NOTIFY restClientChanged)

        private:
            QString scheme;
            QString host;
            QString publicKey;
            QString privateKey;
            QNetworkAccessManager *restClient=nullptr;

        public:
            explicit BittrexRestApiHandler(QObject *parent=nullptr);
            ~BittrexRestApiHandler();
            QString getScheme() const;
            void setScheme(const QString &value);
            QString getHost() const;
            void setHost(const QString &value);
            QString getPublicKey() const;
            void setPublicKey(const QString &value);
            QString getPrivateKey() const;
            void setPrivateKey(const QString &value);
            QNetworkAccessManager *getRestClient() const;
            void setRestClient(QNetworkAccessManager *value);

        signals:
            void schemeChanged(const QString &scheme);
            void hostChanged(const QString &host);
            void publicKeyChanged(const QString &publicKey);
            void privateKeyChanged(const QString &privateKey);
            void restClientChanged(const QNetworkAccessManager *restClient);

            void queryMarketsStarted();
            void queryMarketsFinished(const bool &success,const QString &message,const QJsonArray &result);
            void queryCurrenciesStarted();
            void queryCurrenciesFinished(const bool &success,const QString &message,const QJsonArray &result);
            void queryTickerStarted(const QString &market);
            void queryTickerFinished(const QString &market,const bool &success,const QString &message,const QJsonObject &result);
            void queryMarketSummariesStarted();
            void queryMarketSummariesFinished(const bool &success,const QString &message,const QJsonArray &result);
            void queryMarketSummaryStarted(const QString &market);
            void queryMarketSummaryFinished(const QString &market,const bool &success,const QString &message,const QJsonArray &result);
            void queryOrderBookStarted(const QString &market,const QString &type);
            void queryOrderBookFinished(const QString &market,const QString &type,const bool &success,const QString &message,const QJsonValue &result);
            void queryMarketHistoryStarted(const QString &market);
            void queryMarketHistoryFinished(const QString &market,const bool &success,const QString &messsage,const QJsonArray &result);
            void placeLimitBuyStarted(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce);
            void placeLimitBuyFinished(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce,const bool &success,const QString &message,const QJsonObject &result);
            void placeLimitSellStarted(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce);
            void placeLimitSellFinished(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce,const bool &success,const QString &message,const QJsonObject &result);
            void placeCancelStarted(const QString &orderId,const QString &uuid);
            void placeCancelFinished(const QString &orderId,const QString &uuid,const bool &success,const QString &message,const QJsonObject &result);
            void queryOpenOrdersStarted(const QString &market);
            void queryOpenOrdersFinished(const QString &market,const bool &success,const QString &message,const QJsonArray &result);
            void queryBalancesStarted();
            void queryBalancesFinished(const bool &success,const QString &message,const QJsonArray &result);
            void queryBalanceStarted(const QString &currency);
            void queryBalanceFinished(const QString &currency,const bool &success,const QString &message,const QJsonObject &result);
            void queryDepositAddressStarted(const QString &currency);
            void queryDepositAddressFinished(const QString &currency,const bool &success,const QString &message,const QJsonArray &result);
            void queryWithdrawalStarted(const QString &currency,const double &quantity,const QString &address);
            void queryWithdrawalFinished(const QString &currency,const double &quantity,const QString &address,const bool &success,const QString &message,const QJsonObject &result);
            void queryOrderStarted(const QString &orderId,const QString &uuid);
            void queryOrderFinished(const QString &orderId,const QString &uuid,const bool &success,const QString &message,const QJsonObject &result);
            void queryOrderHistoryStarted(const QString &market);
            void queryOrderHistoryFinished(const QString &market,const bool &success,const QString &message,const QJsonArray &result);
            void queryWithdrawalHistoryStarted(const QString &currency);
            void queryWithdrawalHistoryFinished(const QString &currency,const bool &success,const QString &message,const QJsonArray &result);
            void queryDepositHistoryStarted(const QString &currency);
            void queryDepositHistoryFinished(const QString &currency,const bool &success,const QString &message,const QJsonArray &result);


        private slots:
            void finalizeReply(QNetworkReply *reply);
            void processReply(QNetworkReply *reply);

        public slots:
            void queryMarkets();
            void queryCurrencies();
            void queryTicker(const QString &market);
            void queryMarketSummaries();
            void queryMarketSummary(const QString &market);
            void queryOrderBook(const QString &market,const QString &type);
            void queryMarketHistory(const QString &market);
            void placeLimitBuy(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce);
            void placeLimitSell(const QString &orderId,const QString &market,const double &quantity,const double &rate,const QString &timeInForce);
            void placeCancel(const QString &orderId,const QString &uuid);
            void queryOpenOrders(const QString &market);
            void queryBalances();
            void queryBalance(const QString &currency);
            void queryDepositAddress(const QString &currency);
            void queryWithdrawal(const QString &currency,const double &quantity,const QString &address);
            void queryOrder(const QString &orderId,const QString &uuid);
            void queryOrderHistory(const QString &market);
            void queryWithdrawalHistory(const QString &currency);
            void queryDepositHistory(const QString &currency);
    };
}

Q_DECLARE_METATYPE(bittrexapi::v1::BittrexRestApiHandler *)

#endif // V1_BITTREXRESTAPIHANDLER_HPP
