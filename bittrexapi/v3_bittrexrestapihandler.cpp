#include "v3_bittrexrestapihandler.hpp"
#include <QDateTime>


bittrexapi::v3::BittrexRestApiHandler::BittrexRestApiHandler(QObject *parent): QObject(parent)
{
    this->setPublicKey(QString()); this->setPrivateKey(QString());
    this->setScheme(QString("https")); this->setHost(QString("api.bittrex.com"));
    this->setRestClient(new QNetworkAccessManager(parent));
    this->connect(this->restClient,&QNetworkAccessManager::finished,this,&bittrexapi::v3::BittrexRestApiHandler::finalizeReply);
}


bittrexapi::v3::BittrexRestApiHandler::~BittrexRestApiHandler() { delete this->restClient; }

QString bittrexapi::v3::BittrexRestApiHandler::getScheme() const { return this->scheme; }

void bittrexapi::v3::BittrexRestApiHandler::setScheme(const QString &value)
{
    this->scheme=value;
    emit this->schemeChanged(this->scheme);
}

QString bittrexapi::v3::BittrexRestApiHandler::getHost() const { return this->host; }

void bittrexapi::v3::BittrexRestApiHandler::setHost(const QString &value)
{
    this->host=value;
    emit this->hostChanged(this->host);
}

QString bittrexapi::v3::BittrexRestApiHandler::getPublicKey() const { return this->publicKey; }

void bittrexapi::v3::BittrexRestApiHandler::setPublicKey(const QString &value)
{
    this->publicKey=value;
    emit this->publicKeyChanged(this->publicKey);
}

QString bittrexapi::v3::BittrexRestApiHandler::getPrivateKey() const { return this->privateKey; }

void bittrexapi::v3::BittrexRestApiHandler::setPrivateKey(const QString &value)
{
    this->privateKey=value;
    emit this->privateKeyChanged(this->privateKey);
}

QNetworkAccessManager *bittrexapi::v3::BittrexRestApiHandler::getRestClient() const { return this->restClient; }

void bittrexapi::v3::BittrexRestApiHandler::setRestClient(QNetworkAccessManager *value)
{
    this->restClient=value;
    emit this->restClientChanged(this->restClient);
}


void bittrexapi::v3::BittrexRestApiHandler::queryAccount()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/account"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","account");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryAccountStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryAccountVolume()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/account/volume"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","accountVolume");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryAccountVolumeStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryAddresses()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/addresses"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","addresses");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryAddressesStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryAddressProvision(const QString &currency)
{
    QByteArray result; QNetworkRequest request;
    QUrl bittrexUrl; QJsonObject content;

    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/addresses"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    content.insert("currencySymbol",currency);
    QJsonDocument doc(content);
    hash.addData(doc.toJson());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("POST")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->post(request,doc.toJson());
    reply->setProperty("apiCall","addressProvision");
    reply->setProperty("currency",currency);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryAddressProvisionStarted(currency);
}

void bittrexapi::v3::BittrexRestApiHandler::queryAddress(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/addresses/")+currency);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","address");
    reply->setProperty("result",result);
    reply->setProperty("currency",currency.toLatin1());
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryAddressStarted(currency);
}

void bittrexapi::v3::BittrexRestApiHandler::queryBalances()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/balances"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","balances");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryBalancesStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryBalance(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/balances/")+currency);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","balance");
    reply->setProperty("result",result);
    reply->setProperty("currency",currency.toLatin1());
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryBalanceStarted(currency);
}


void bittrexapi::v3::BittrexRestApiHandler::queryOpenConditionalOrders()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/conditional-orders/open"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","openConditionalOrders");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOpenConditionalOrdersStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryClosedConditionalOrders()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/conditional-orders/closed"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","closedConditionalOrders");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryClosedConditionalOrdersStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryConditionalOrder(const QString &orderId,const QString &conditionalOrderId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/conditional-orders/")+conditionalOrderId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","conditionalOrder");
    reply->setProperty("result",result);
    reply->setProperty("conditionalOrderId",conditionalOrderId);
    reply->setProperty("orderId",orderId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryConditionalOrderStarted(orderId,conditionalOrderId);
}


void bittrexapi::v3::BittrexRestApiHandler::cancelConditionalOrder(const QString &orderId,const QString &conditionalOrderId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/conditional-orders/")+conditionalOrderId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("DELETE")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->deleteResource(request);
    reply->setProperty("apiCall","cancelConditionalOrder");
    reply->setProperty("result",result);
    reply->setProperty("conditionalOrderId",conditionalOrderId);
    reply->setProperty("orderId",orderId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->cancelConditionalOrderStarted(orderId,conditionalOrderId);
}


void bittrexapi::v3::BittrexRestApiHandler::placeConditionalOrder(const QString &orderId,const QJsonObject &data)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/conditional-orders"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    QJsonDocument doc(data);
    hash.addData(doc.toJson());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("POST")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->post(request,doc.toJson());
    reply->setProperty("apiCall","placeConditionalOrder");
    reply->setProperty("orderId",orderId);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->placeConditionalOrderStarted(orderId,data);
}


void bittrexapi::v3::BittrexRestApiHandler::queryCurrencies()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/currencies"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","currencies");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryCurrenciesStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryCurrency(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/currencies/")+currency);
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","currency");
    reply->setProperty("result",result);
    reply->setProperty("currency",currency.toLatin1());
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryCurrencyStarted(currency);
}

void bittrexapi::v3::BittrexRestApiHandler::queryOpenDeposits()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/deposits/open"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","openDeposits");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOpenDepositsStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryClosedDeposits()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/deposits/closed"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","closedDeposits");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryClosedDepositsStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryDepositsByTxId(const QString &txId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/deposits/ByTxId/")+txId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","depositsByTxId");
    reply->setProperty("result",result);
    reply->setProperty("txId",txId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryDepositsByTxIdStarted(txId);
}


void bittrexapi::v3::BittrexRestApiHandler::queryDepositById(const QString &depositId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/deposits/")+depositId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","depositById");
    reply->setProperty("result",result);
    reply->setProperty("depositId",depositId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryDepositByIdStarted(depositId);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarkets()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","markets");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketsStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketSummaries()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/summaries"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketSummaries");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketSummariesStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketTickers()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/tickers"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketTickers");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketTickersStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarket(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market);
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","market");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketStarted(market);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketSummary(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market+QString("/summary"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketSummary");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketSummaryStarted(market);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketOrderBook(const QString &market,const int &depth)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market+QString("/orderbook"));
    bittrexUrl.setQuery("depth="+QString::number(depth));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketOrderBook");
    reply->setProperty("market",market);
    reply->setProperty("depth",depth);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketOrderBookStarted(market,depth);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketTrades(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market+QString("/trades"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketTrades");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketTradesStarted(market);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketTicker(const QString &market)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market+QString("/ticker"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketTicker");
    reply->setProperty("market",market);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketTickerStarted(market);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketRecentCandles(const QString &market,const QString &candleInterval)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market+QString("/candles/")+candleInterval+QString("/recent"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketRecentCandles");
    reply->setProperty("market",market);
    reply->setProperty("candleInterval",candleInterval);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketRecentCandlesStarted(market,candleInterval);
}

void bittrexapi::v3::BittrexRestApiHandler::queryMarketHistoricalCandles(const QString &market,const QString &candleInterval,const int &year,const int &month,const int &day)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/markets/")+market+QString("/candles/")+candleInterval+QString("/historical/")
        +QString::number(year)+QString("/")+QString::number(month)+QString("/")+QString::number(day));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","marketHistoricalCandles");
    reply->setProperty("market",market);
    reply->setProperty("candleInterval",candleInterval);
    reply->setProperty("year",year);
    reply->setProperty("month",month);
    reply->setProperty("day",day);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryMarketHistoricalCandlesStarted(market,candleInterval,year,month,day);
}

void bittrexapi::v3::BittrexRestApiHandler::queryOpenOrders()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/orders/open"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","openOrders");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOpenOrdersStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryClosedOrders()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/orders/closed"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","closedOrders");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryClosedOrdersStarted();
}

void bittrexapi::v3::BittrexRestApiHandler::queryOrder(const QString &orderId,const QString &exchangeOrderId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/orders/")+exchangeOrderId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","order");
    reply->setProperty("orderId",orderId);
    reply->setProperty("exchangeOrderId",exchangeOrderId);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOrderStarted(orderId,exchangeOrderId);
}

void bittrexapi::v3::BittrexRestApiHandler::cancelOrder(const QString &orderId,const QString &exchangeOrderId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/orders/")+exchangeOrderId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("DELETE")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->sendCustomRequest(request,"DELETE");
    reply->setProperty("apiCall","cancelOrder");
    reply->setProperty("result",result);
    reply->setProperty("exchangeOrderId",exchangeOrderId);
    reply->setProperty("orderId",orderId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->cancelOrderStarted(orderId,exchangeOrderId);
}

void bittrexapi::v3::BittrexRestApiHandler::placeOrder(const QString &orderId,const QJsonObject &data)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/orders"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    QJsonDocument doc(data);
    hash.addData(doc.toJson());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("POST")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->post(request,doc.toJson());
    reply->setProperty("apiCall","placeOrder");
    reply->setProperty("orderId",orderId);
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->placeOrderStarted(orderId,data);
}

void bittrexapi::v3::BittrexRestApiHandler::queryPing()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/ping"));
    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","ping");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryPingStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryOpenWithdrawals()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/withdrawals/open"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","openWithdrawals");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOpenWithdrawalsStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryClosedWithdrawals()
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/withdrawals/closed"));

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","closedWithdrawals");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryClosedWithdrawalsStarted();
}


void bittrexapi::v3::BittrexRestApiHandler::queryWithdrawalsByTxId(const QString &txId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/withdrawals/ByTxId/")+txId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","withdrawalsByTxId");
    reply->setProperty("result",result);
    reply->setProperty("txId",txId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryWithdrawalsByTxIdStarted(txId);
}


void bittrexapi::v3::BittrexRestApiHandler::queryWithdrawalById(const QString &withdrawalId)
{
    QByteArray result; QNetworkRequest request; QUrl bittrexUrl;
    bittrexUrl.setScheme(this->getScheme());
    bittrexUrl.setHost(this->getHost());
    bittrexUrl.setPath(QString("/v3/withdrawals/")+withdrawalId);

    QString public_key=this->getPublicKey();
    QString api_timestamp=QString::number(QDateTime::currentMSecsSinceEpoch());

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(QString("").toLatin1());
    QString api_content_hash=hash.result().toHex();

    QMessageAuthenticationCode cypher(QCryptographicHash::Sha512);
    cypher.setKey(this->getPrivateKey().toLatin1());
    QString signature=api_timestamp+bittrexUrl.toEncoded()+QString("GET")+api_content_hash;
    cypher.addData(signature.toLatin1());
    QString api_signature=cypher.result().toHex();

    request.setUrl(bittrexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("Api-Key",public_key.toLatin1());
    request.setRawHeader("Api-Timestamp",api_timestamp.toLatin1());
    request.setRawHeader("Api-Content-Hash",api_content_hash.toLatin1());
    request.setRawHeader("Api-Signature",api_signature.toLatin1());

    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","withdrawalById");
    reply->setProperty("result",result);
    reply->setProperty("withdrawalId",withdrawalId);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryWithdrawalByIdStarted(withdrawalId);
}


void bittrexapi::v3::BittrexRestApiHandler::processReply(QNetworkReply *reply)
{
    qint64 bytesAvailable=reply->bytesAvailable();
    QByteArray current_result=reply->property("result").toByteArray()+reply->read(bytesAvailable);
    reply->setProperty("result",current_result);
}


void bittrexapi::v3::BittrexRestApiHandler::finalizeReply(QNetworkReply *reply)
{
    QByteArray output=reply->property("result").toByteArray();
    QJsonDocument document=QJsonDocument::fromJson(output);
    QString apiCall=reply->property("apiCall").toString();

    if (apiCall==QString("account"))
    {
        QJsonObject data=document.object();
        emit this->queryAccountFinished(data);
    }
    else if (apiCall==QString("accountVolume"))
    {
        QJsonObject data=document.object();
        emit this->queryAccountVolumeFinished(data);
    }
    else if (apiCall==QString("addresses"))
    {
        QJsonArray data=document.array();
        emit this->queryAddressesFinished(data);
    }
    else if (apiCall==QString("addressProvision"))
    {
        QJsonObject data=document.object();
        QString currency=reply->property("currency").toString();
        emit this->queryAddressProvisionFinished(currency,data);
    }
    else if (apiCall==QString("address"))
    {
        QJsonObject data=document.object();
        QString currency=reply->property("currency").toString();
        emit this->queryAddressFinished(currency,data);
    }
    else if (apiCall==QString("balances"))
    {
        QJsonArray data=document.array();
        emit this->queryBalancesFinished(data);
    }
    else if (apiCall==QString("balance"))
    {
        QJsonObject data=document.object();
        QString currency=reply->property("currency").toString();
        emit this->queryBalanceFinished(currency,data);
    }
    else if (apiCall==QString("currencies"))
    {
        QJsonArray data=document.array();
        emit this->queryCurrenciesFinished(data);
    }
    else if (apiCall==QString("currency"))
    {
        QJsonObject data=document.object();
        QString currency=reply->property("currency").toString();
        emit this->queryCurrencyFinished(currency,data);
    }
    else if (apiCall==QString("openConditionalOrders"))
    {
        QJsonArray data=document.array();
        emit this->queryOpenConditionalOrdersFinished(data);
    }
    else if (apiCall==QString("closedConditionalOrders"))
    {
        QJsonArray data=document.array();
        emit this->queryClosedConditionalOrdersFinished(data);
    }
    else if (apiCall==QString("conditionalOrder"))
    {
        QJsonObject data=document.object();
        QString orderId=reply->property("orderId").toString();
        QString conditionalOrderId=reply->property("conditionalOrderId").toString();
        emit this->queryConditionalOrderFinished(orderId,conditionalOrderId,data);
    }
    else if (apiCall==QString("cancelConditionalOrder"))
    {
        QJsonObject data=document.object();
        QString orderId=reply->property("orderId").toString();
        QString conditionalOrderId=reply->property("conditionalOrderId").toString();
        emit this->cancelConditionalOrderFinished(orderId,conditionalOrderId,data);
    }
    else if (apiCall==QString("placeConditionalOrder"))
    {
        QJsonObject data=document.object();
        QString orderId=reply->property("orderId").toString();
        emit this->placeConditionalOrderFinished(orderId,data);
    }
    else if (apiCall==QString("openDeposits"))
    {
        QJsonArray data=document.array();
        emit this->queryOpenDepositsFinished(data);
    }
    else if (apiCall==QString("closedDeposits"))
    {
        QJsonArray data=document.array();
        emit this->queryClosedDepositsFinished(data);
    }
    else if (apiCall==QString("depositsByTxId"))
    {
        QJsonArray data=document.array();
        QString txId=reply->property("txId").toString();
        emit this->queryDepositsByTxIdFinished(txId,data);
    }
    else if (apiCall==QString("depositById"))
    {
        QJsonObject data=document.object();
        QString depositId=reply->property("depositId").toString();
        emit this->queryDepositByIdFinished(depositId,data);
    }
    else if (apiCall==QString("markets"))
    {
        QJsonArray data=document.array();
        emit this->queryMarketsFinished(data);
    }
    else if (apiCall==QString("marketSummaries"))
    {
        QJsonArray data=document.array();
        emit this->queryMarketSummariesFinished(data);
    }
    else if (apiCall==QString("marketTickers"))
    {
        QJsonArray data=document.array();
        emit this->queryMarketTickersFinished(data);
    }
    else if (apiCall==QString("market"))
    {
        QJsonObject data=document.object();
        QString market=reply->property("market").toString();
        emit this->queryMarketFinished(market,data);
    }
    else if (apiCall==QString("marketSummary"))
    {
        QJsonObject data=document.object();
        QString market=reply->property("market").toString();
        emit this->queryMarketSummaryFinished(market,data);
    }
    else if (apiCall==QString("marketOrderBook"))
    {
        QJsonObject data=document.object();
        QString market=reply->property("market").toString();
        int depth=reply->property("depth").toInt();
        emit this->queryMarketOrderBookFinished(market,depth,data);
    }
    else if (apiCall==QString("marketTrades"))
    {
        QJsonArray data=document.array();
        QString market=reply->property("market").toString();
        emit this->queryMarketTradesFinished(market,data);
    }
    else if (apiCall==QString("marketTicker"))
    {
        QJsonObject data=document.object();
        QString market=reply->property("market").toString();
        emit this->queryMarketTickerFinished(market,data);
    }
    else if (apiCall==QString("marketRecentCandles"))
    {
        QJsonArray data=document.array();
        QString market=reply->property("market").toString();
        QString candleInterval=reply->property("candleInterval").toString();
        emit this->queryMarketRecentCandlesFinished(market,candleInterval,data);
    }
    else if (apiCall==QString("marketHistoricalCandles"))
    {
        QJsonArray data=document.array();
        QString market=reply->property("market").toString();
        QString candleInterval=reply->property("candleInterval").toString();
        int year=reply->property("year").toInt();
        int month=reply->property("month").toInt();
        int day=reply->property("day").toInt();
        emit this->queryMarketHistoricalCandlesFinished(market,candleInterval,year,month,day,data);
    }
    else if (apiCall==QString("openOrders"))
    {
        QJsonArray data=document.array();
        emit this->queryOpenOrdersFinished(data);
    }
    else if (apiCall==QString("closedOrders"))
    {
        QJsonArray data=document.array();
        emit this->queryClosedOrdersFinished(data);
    }
    else if (apiCall==QString("order"))
    {
        QJsonObject data=document.object();
        QString orderId=reply->property("orderId").toString();
        QString exchangeOrderId=reply->property("exchangeOrderId").toString();
        emit this->queryOrderFinished(orderId,exchangeOrderId,data);
    }
    else if (apiCall==QString("cancelOrder"))
    {
        QJsonObject data=document.object();
        QString orderId=reply->property("orderId").toString();
        QString exchangeOrderId=reply->property("exchangeOrderId").toString();
        emit this->cancelOrderFinished(orderId,exchangeOrderId,data);
    }
    else if (apiCall==QString("placeOrder"))
    {
        QJsonObject data=document.object();
        QString orderId=reply->property("orderId").toString();
        emit this->placeOrderFinished(orderId,data);
    }
    else if (apiCall==QString("ping"))
    {
        QJsonObject data=document.object();
        emit this->queryPingFinished(data);
    }
    else if (apiCall==QString("openWithdrawals"))
    {
        QJsonArray data=document.array();
        emit this->queryOpenWithdrawalsFinished(data);
    }
    else if (apiCall==QString("closedWithdrawals"))
    {
        QJsonArray data=document.array();
        emit this->queryClosedWithdrawalsFinished(data);
    }
    else if (apiCall==QString("withdrawalsByTxId"))
    {
        QJsonArray data=document.array();
        QString txId=reply->property("txId").toString();
        emit this->queryWithdrawalsByTxIdFinished(txId,data);
    }
    else if (apiCall==QString("withdrawalById"))
    {
        QJsonObject data=document.object();
        QString withdrawalId=reply->property("withdrawalId").toString();
        emit this->queryWithdrawalByIdFinished(withdrawalId,data);
    } else {}

    reply->deleteLater();
}
