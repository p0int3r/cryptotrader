#ifndef SINGLETON_HPP
#define SINGLETON_HPP


#include <cassert>

namespace pattern
{
    template <class T>
    class Singleton
    {
        private:
            inline static T *instance=nullptr;

        public:
            static T *getInstance();

        protected:
            Singleton();
            ~Singleton();

        private:
            Singleton(const Singleton &);
            Singleton &operator=(const Singleton &);
    };


    template <class T>
    T *Singleton<T>::getInstance()
    {
        if (!Singleton::instance) { Singleton::instance=new T(); }
        assert(Singleton::instance!=nullptr);
        return Singleton::instance;
    }
}


#endif // SINGLETON_HPP
