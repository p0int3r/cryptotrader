#ifndef POLONIEXRESTAPIHANDLER_HPP
#define POLONIEXRESTAPIHANDLER_HPP

#include <QObject>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariantMap>
#include <QJsonObject>
#include <QMessageAuthenticationCode>
#include <QNetworkAccessManager>
#include <QCryptographicHash>
#include <qqml.h>

namespace poloniexapi
{
    class PoloniexRestApiHandler: public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(QString scheme READ getScheme WRITE setScheme NOTIFY schemeChanged)
        Q_PROPERTY(QString host READ getHost WRITE setHost NOTIFY hostChanged)
        Q_PROPERTY(QString publicKey READ getPublicKey WRITE setPublicKey NOTIFY publicKeyChanged)
        Q_PROPERTY(QString privateKey READ getPrivateKey WRITE setPrivateKey NOTIFY privateKeyChanged)
        Q_PROPERTY(QNetworkAccessManager *restClient READ getRestClient WRITE setRestClient NOTIFY restClientChanged)

        private:
            QString scheme;
            QString host;
            QString publicKey;
            QString privateKey;
            QNetworkAccessManager *restClient=nullptr;

        public:
            explicit PoloniexRestApiHandler(QObject *parent=nullptr);
            ~PoloniexRestApiHandler();
            QString getScheme() const;
            void setScheme(const QString &value);
            QString getHost() const;
            void setHost(const QString &value);
            QString getPublicKey() const;
            void setPublicKey(const QString &value);
            QString getPrivateKey() const;
            void setPrivateKey(const QString &value);
            QNetworkAccessManager *getRestClient() const;
            void setRestClient(QNetworkAccessManager *value);

        signals:
            void schemeChanged(const QString &scheme);
            void hostChanged(const QString &host);
            void publicKeyChanged(const QString &publicKey);
            void privateKeyChanged(const QString &privateKey);
            void restClientChanged(const QNetworkAccessManager *restClient);

            void queryTickersStarted();
            void queryTickersFinished(const QJsonObject &data);
            void queryCurrenciesStarted();
            void queryCurrenciesFinished(const QJsonObject &data);
            void queryDailyVolumesStarted();
            void queryDailyVolumesFinished(const QJsonObject &data);
            void queryLoanOrdersStarted(const QString &currency);
            void queryLoanOrdersFinished(const QString &currency,const QJsonObject &data);
            void queryOrderBookStarted(const QString &market,const int &depth);
            void queryOrderBookFinished(const QString &market,const int &depth,const QJsonObject &data);
            void queryTradeHistoryStarted(const QString &market,const QDateTime &start,const QDateTime &end,const QJsonArray &data);
            void queryChartDataStarted(const QString &market,const int &period,const QDateTime &start,const QDateTime &end);
            void queryChartDataFinished(const QString &market,const int &period,const QDateTime &start,const QDateTime &end,const QJsonArray &data);

        private slots:
            void finalizeReply(QNetworkReply *reply);
            void processReply(QNetworkReply *reply);

        public slots:
            void queryTickers();
            void queryCurrencies();
            void queryDailyVolumes();
            void queryLoanOrders(const QString &currency);
            void queryOrderBook(const QString &market,const int &depth);
            void queryTradeHistory(const QString &market,const QDateTime &start=QDateTime(),const QDateTime &end=QDateTime());
//            void queryChartData(const QString &market,const int &period,const QDateTime &start,const QDateTime &end);
    };
}

Q_DECLARE_METATYPE(poloniexapi::PoloniexRestApiHandler *)

#endif // POLONIEXRESTAPIHANDLER_HPP
