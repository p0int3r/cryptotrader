#include "poloniexrestapihandler.hpp"

poloniexapi::PoloniexRestApiHandler::PoloniexRestApiHandler(QObject *parent): QObject(parent)
{
    this->setPublicKey(QString()); this->setPrivateKey(QString());
    this->setScheme(QString("https")); this->setHost(QString("poloniex.com"));
    this->setRestClient(new QNetworkAccessManager(parent));
    this->connect(this->restClient,&QNetworkAccessManager::finished,this,&poloniexapi::PoloniexRestApiHandler::finalizeReply);
}

poloniexapi::PoloniexRestApiHandler::~PoloniexRestApiHandler() { delete this->restClient; }

QString poloniexapi::PoloniexRestApiHandler::getScheme() const { return this->scheme; }

void poloniexapi::PoloniexRestApiHandler::setScheme(const QString &value)
{
    this->scheme=value;
    emit this->schemeChanged(this->scheme);
}

QString poloniexapi::PoloniexRestApiHandler::getHost() const { return this->host; }

void poloniexapi::PoloniexRestApiHandler::setHost(const QString &value)
{
    this->host=value;
    emit this->hostChanged(this->host);
}

QString poloniexapi::PoloniexRestApiHandler::getPublicKey() const { return this->publicKey; }

void poloniexapi::PoloniexRestApiHandler::setPublicKey(const QString &value)
{
    this->publicKey=value;
    emit this->publicKeyChanged(this->publicKey);
}

QString poloniexapi::PoloniexRestApiHandler::getPrivateKey() const { return this->privateKey; }

void poloniexapi::PoloniexRestApiHandler::setPrivateKey(const QString &value)
{
    this->privateKey=value;
    emit this->privateKeyChanged(this->privateKey);
}

QNetworkAccessManager *poloniexapi::PoloniexRestApiHandler::getRestClient() const { return this->restClient; }

void poloniexapi::PoloniexRestApiHandler::setRestClient(QNetworkAccessManager *value)
{
    this->restClient=value;
    emit this->restClientChanged(this->restClient);
}


void poloniexapi::PoloniexRestApiHandler::queryTickers()
{
    QByteArray result; QNetworkRequest request; QUrl poloniexUrl;
    poloniexUrl.setScheme(this->getScheme());
    poloniexUrl.setHost(this->getHost());
    poloniexUrl.setPath(QString("/public"));
    poloniexUrl.setQuery("command=returnTicker");
    request.setUrl(poloniexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","tickers");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryTickersStarted();
}

void poloniexapi::PoloniexRestApiHandler::queryCurrencies()
{
    QByteArray result; QNetworkRequest request; QUrl poloniexUrl;
    poloniexUrl.setScheme(this->getScheme());
    poloniexUrl.setHost(this->getHost());
    poloniexUrl.setPath(QString("/public"));
    poloniexUrl.setQuery("command=returnCurrencies");
    request.setUrl(poloniexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","currencies");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryCurrenciesStarted();
}


void poloniexapi::PoloniexRestApiHandler::queryDailyVolumes()
{
    QByteArray result; QNetworkRequest request; QUrl poloniexUrl;
    poloniexUrl.setScheme(this->getScheme());
    poloniexUrl.setHost(this->getHost());
    poloniexUrl.setPath(QString("/public"));
    poloniexUrl.setQuery("command=return24hVolume");
    request.setUrl(poloniexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","dailyVolumes");
    reply->setProperty("result",result);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryDailyVolumesStarted();
}


void poloniexapi::PoloniexRestApiHandler::queryLoanOrders(const QString &currency)
{
    QByteArray result; QNetworkRequest request; QUrl poloniexUrl;
    poloniexUrl.setScheme(this->getScheme());
    poloniexUrl.setHost(this->getHost());
    poloniexUrl.setPath(QString("/public"));
    poloniexUrl.setQuery("command=returnLoanOrders&currency="+currency);
    request.setUrl(poloniexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","loanOrders");
    reply->setProperty("result",result);
    reply->setProperty("currency",currency);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryLoanOrdersStarted(currency);
}

void poloniexapi::PoloniexRestApiHandler::queryOrderBook(const QString &market,const int &depth)
{
    QByteArray result; QNetworkRequest request; QUrl poloniexUrl;
    poloniexUrl.setScheme(this->getScheme());
    poloniexUrl.setHost(this->getHost());
    poloniexUrl.setPath(QString("/public"));
    poloniexUrl.setQuery("command=returnOrderBook&currencyPair="+market+"&depth="+QString::number(depth));
    request.setUrl(poloniexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QNetworkReply *reply=this->getRestClient()->get(request);
    reply->setProperty("apiCall","orderBook");
    reply->setProperty("result",result);
    reply->setProperty("market",market);
    reply->setProperty("depth",depth);
    this->connect(reply,&QNetworkReply::readyRead,this,[=]() { this->processReply(reply); });
    emit this->queryOrderBookStarted(market,depth);
}

void poloniexapi::PoloniexRestApiHandler::queryTradeHistory(const QString &market,const QDateTime &start,const QDateTime &end)
{
    QByteArray result; QNetworkRequest request; QUrl poloniexUrl;
    poloniexUrl.setScheme(this->getScheme());
    poloniexUrl.setHost(this->getHost());
    poloniexUrl.setPath(QString("/"));
    QString query_string=QString("command=returnTradeHistory&currencyPair=")+market;

    if (start.isNull()==false && end.isNull()==false)
    {
        query_string+=QString("&start=")+start.currentMSecsSinceEpoch();
        query_string+=QString("&end=")+end.currentMSecsSinceEpoch();
    }

    poloniexUrl.setQuery(query_string);
    request.setUrl(poloniexUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");

}


void poloniexapi::PoloniexRestApiHandler::processReply(QNetworkReply *reply)
{
    qint64 bytesAvailable=reply->bytesAvailable();
    QByteArray current_result=reply->property("result").toByteArray()+reply->read(bytesAvailable);
    reply->setProperty("result",current_result);
}


void poloniexapi::PoloniexRestApiHandler::finalizeReply(QNetworkReply *reply)
{
    QByteArray output=reply->property("result").toByteArray();
    QJsonDocument document=QJsonDocument::fromJson(output);
    QString apiCall=reply->property("apiCall").toString();

    if (apiCall==QString("tickers"))
    {
        QJsonObject data=document.object();
        emit this->queryTickersFinished(data);
    }
    else if (apiCall==QString("currencies"))
    {
        QJsonObject data=document.object();
        emit this->queryCurrenciesFinished(data);
    }
    else if (apiCall==QString("dailyVolumes"))
    {
        QJsonObject data=document.object();
        emit this->queryDailyVolumesFinished(data);
    }
    else if (apiCall==QString("loanOrders"))
    {
        QJsonObject data=document.object();
        QString currency=reply->property("currency").toString();
        emit this->queryLoanOrdersFinished(currency,data);
    }
    else if (apiCall==QString("orderBook"))
    {
        QJsonObject data=document.object();
        QString market=reply->property("market").toString();
        int depth=reply->property("depth").toInt();
        emit this->queryOrderBookFinished(market,depth,data);
    }

    reply->deleteLater();
}
