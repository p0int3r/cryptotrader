cmake_minimum_required(VERSION 3.17)

project(poloniexapi)

set(CMAKE_CXX_STANDARD 20)

set (CMAKE_AUTOMOC ON)
set (CMAKE_AUTOUIC ON)
set (CMAKE_AUTORCC ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
find_package(Qt5 5.15.1 COMPONENTS Core Network Qml REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-pragmas -Wno-deprecated -Wno-deprecated-copy")
include_directories(${BOOST_INCLUDE_DIR})

add_library(poloniexapi poloniexrestapihandler.hpp poloniexrestapihandler.cpp)

target_link_libraries(poloniexapi Qt5::Core Qt5::Qml Qt5::Network)